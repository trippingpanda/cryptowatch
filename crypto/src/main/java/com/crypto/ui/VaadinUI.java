package com.crypto.ui;

import com.crypto.backend.db.enitity.Bitcoin;
import com.crypto.backend.db.enitity.DogeCoin;
import com.crypto.backend.db.enitity.Ethereum;
import com.crypto.backend.db.enitity.Litecoin;
import com.crypto.backend.db.Currency;
import com.crypto.backend.db.repository.BitcoinRepository;
import com.crypto.backend.db.repository.DogecoinRepository;
import com.crypto.backend.db.repository.EtherRepository;
import com.crypto.backend.db.repository.LitecoinRepository;
import com.crypto.backend.service.CurrencyService;
import com.vaadin.annotations.Theme;
import com.vaadin.event.selection.SingleSelectionEvent;
import com.vaadin.event.selection.SingleSelectionListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@SpringUI
@Theme("valo")
public class VaadinUI extends UI {

	@Autowired
	private BitcoinRepository repo;

	@Autowired
	private EtherRepository etherRepository;

	@Autowired
	private DogecoinRepository dogecoinRepository;

	@Autowired
	private LitecoinRepository litecoinRepository;

	@Autowired
	private CurrencyService currencyService;

	final Grid<Bitcoin> bitcoinGrid = new Grid<>(Bitcoin.class);

	final Grid<Litecoin> ltcGrid = new Grid<>(Litecoin.class);

	final Grid<Ethereum> ethereumGrid = new Grid<>(Ethereum.class);

	final Grid<DogeCoin> dogeCoinGrid = new Grid<>(DogeCoin.class);

	@Override
	protected void init(VaadinRequest request) {
		List<Currency> currencyList = new ArrayList<>();
		currencyList.add(Currency.BITCOIN);
		currencyList.add(Currency.DOGECOIN);
		currencyList.add(Currency.ETHEREUM);
		currencyList.add(Currency.LITECOIN);

		ComboBox<Currency> select =
				new ComboBox<>("Select a coin");
		select.setItems(currencyList);
		select.setItemCaptionGenerator(Currency::getValue);

		select.setSizeFull();
		select.setSelectedItem(Currency.BITCOIN);

		HorizontalLayout actions = new HorizontalLayout(select);
		VerticalLayout mainLayout = new VerticalLayout(actions, bitcoinGrid);
		setContent(mainLayout);

		bitcoinGrid.setSizeFull();
		bitcoinGrid.setColumns( "date", "txVolume","txCount","marketCap","price","exchangeVolume","generatedCoins","fees");

		ethereumGrid.setSizeFull();
		ethereumGrid.setColumns( "date", "txVolume","txCount","marketCap","price","exchangeVolume","generatedCoins","fees");

		dogeCoinGrid.setSizeFull();
		dogeCoinGrid.setColumns( "date", "txVolume","txCount","marketCap","price","exchangeVolume","generatedCoins","fees");

		ltcGrid.setSizeFull();
		ltcGrid.setColumns( "date", "txVolume","txCount","marketCap","price","exchangeVolume","generatedCoins","fees");

		listCustomers();

		select.addSelectionListener(new SingleSelectionListener<Currency>() {
			@Override
			public void selectionChange(SingleSelectionEvent<Currency> event) {
				Optional<Currency> selectedItem = event.getSelectedItem();
				if(selectedItem.isPresent()){
					listCustomers(selectedItem.get(),actions,mainLayout);
				}
			}
		});
	}

	private void listCustomers(Currency currency,HorizontalLayout horizontalLayout,VerticalLayout verticalLayout) {
		switch (currency){
			case BITCOIN:
				listCustomers();
				verticalLayout.removeAllComponents();
				verticalLayout.addComponents(horizontalLayout,bitcoinGrid);
				break;
			case ETHEREUM:
				populateEtherToGrid();
				verticalLayout.removeAllComponents();
				verticalLayout.addComponents(horizontalLayout,ethereumGrid);
				break;
			case DOGECOIN:
				populateDogeToGrid();
				verticalLayout.removeAllComponents();
				verticalLayout.addComponents(horizontalLayout,dogeCoinGrid);
				break;
			case LITECOIN:
				populateLiteToGrid();
				verticalLayout.removeAllComponents();
				verticalLayout.addComponents(horizontalLayout,ltcGrid);
				break;
		}
	}

	private void populateLiteToGrid() {
		List<Litecoin> all = litecoinRepository.findAll();

		all.forEach(x -> {
			System.out.println(x.toString());
		});

		Litecoin litecoinCurrentData = currencyService.getLitecoinCurrentData();
		if(litecoinCurrentData != null){
			all.add(litecoinCurrentData);
		}
		ltcGrid.setItems(all);
	}

	private void populateEtherToGrid() {
		List<Ethereum> all = etherRepository.findAll();
		all.forEach(x -> {
			System.out.println(x.toString());
		});
		Ethereum ethCurrentData = currencyService.getETHCurrentData();
		if(ethCurrentData != null){
			all.add(ethCurrentData);
		}
		ethereumGrid.setItems(all);
	}

	private void populateDogeToGrid() {
		List<DogeCoin> all = dogecoinRepository.findAll();
		all.forEach(x -> {
			System.out.println(x.toString());
		});

		DogeCoin dogecoinCurrentData = currencyService.getDogecoinCurrentData();
		if(dogecoinCurrentData !=null){
			all.add(dogecoinCurrentData);
		}
		dogeCoinGrid.setItems(all);

	}

	void listCustomers() {
		List<Bitcoin> all = repo.findAll();
		all.forEach(x -> {
			System.out.println(x.toString());
		});
		Bitcoin bitcoinCurrentData = currencyService.getBitcoinCurrentData();
		if(bitcoinCurrentData != null){
			all.add(bitcoinCurrentData);
		}
		bitcoinGrid.setItems(all);
	}
}
