package com.crypto.backend.service;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AlphavantageApi {

	@GET("query?function=DIGITAL_CURRENCY_INTRADAY&symbol=BTC&market=USD&apikey=AWLH1WVVSLPUVPCK")
	Call<Object> getIntraDayDataBTC();

	@GET("query?function=DIGITAL_CURRENCY_INTRADAY&symbol=LTC&market=USD&apikey=AWLH1WVVSLPUVPCK")
	Call<Object> getIntraDayDataLTC();

	@GET("query?function=DIGITAL_CURRENCY_INTRADAY&symbol=ETH&market=USD&apikey=AWLH1WVVSLPUVPCK")
	Call<Object> getIntraDayDataETH();

	@GET("query?function=DIGITAL_CURRENCY_INTRADAY&symbol=DOGE&market=USD&apikey=AWLH1WVVSLPUVPCK")
	Call<Object> getIntraDayDataDOGE();
}
