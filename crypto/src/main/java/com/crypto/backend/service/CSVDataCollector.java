package com.crypto.backend.service;

import com.crypto.backend.db.enitity.Bitcoin;
import com.crypto.backend.db.enitity.DogeCoin;
import com.crypto.backend.db.enitity.Ethereum;
import com.crypto.backend.db.enitity.Litecoin;
import com.crypto.backend.db.repository.BitcoinRepository;
import com.crypto.backend.db.repository.DogecoinRepository;
import com.crypto.backend.db.repository.EtherRepository;
import com.crypto.backend.db.repository.LitecoinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.Scanner;

@Component
public class CSVDataCollector {

	@Autowired
	private BitcoinRepository bitcoinRepository;

	@Autowired
	private EtherRepository etherRepository;

	@Autowired
	private DogecoinRepository dogecoinRepository;

	@Autowired
	private LitecoinRepository litecoinRepository;

	@Autowired
	private CurrencyService currencyService;

	@PostConstruct
	public void collectFromCSV(){
		loadBtc();
		loadEth();
		loadLtc();
		loadDoge();

		//loading up the cache at boot time.
		currencyService.getBitcoinCurrentData();
		currencyService.getDogecoinCurrentData();
		currencyService.getETHCurrentData();
		currencyService.getLitecoinCurrentData();

	}

	private void loadBtc() {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream("btc.csv");
		Scanner sc = new Scanner(inputStream);
		if(!StringUtils.isEmpty(sc.nextLine())){
			while (sc.hasNextLine()){
				String[] dataSplit = sc.nextLine().split(",");

				Bitcoin btcCurrency = Bitcoin.builder()
						.date(dataSplit[0])
						.txVolume(dataSplit[1])
						.txCount(dataSplit[2])
						.marketCap(dataSplit[3])
						.price(dataSplit[4])
						.exchangeVolume(dataSplit[5])
						.generatedCoins(dataSplit[6])
						.fees(dataSplit[7])
						.build();
				bitcoinRepository.save(btcCurrency);
			}
		}
	}

	private void loadLtc() {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream("ltc.csv");
		Scanner sc = new Scanner(inputStream);
		if(!StringUtils.isEmpty(sc.nextLine())){
			while (sc.hasNextLine()){
				String[] dataSplit = sc.nextLine().split(",");

				Litecoin ltcCurrency = Litecoin.builder()
						.date(dataSplit[0])
						.txVolume(dataSplit[1])
						.txCount(dataSplit[2])
						.marketCap(dataSplit[3])
						.price(dataSplit[4])
						.exchangeVolume(dataSplit[5])
						.generatedCoins(dataSplit[6])
						.fees(dataSplit[7])
						.build();
				litecoinRepository.save( ltcCurrency);
			}
		}
	}

	private void loadEth() {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream("eth.csv");
		Scanner sc = new Scanner(inputStream);
		if(!StringUtils.isEmpty(sc.nextLine())){
			while (sc.hasNextLine()){
				String[] dataSplit = sc.nextLine().split(",");

				Ethereum etherCurrency = Ethereum.builder()
						.date(dataSplit[0])
						.txVolume(dataSplit[1])
						.txCount(dataSplit[2])
						.marketCap(dataSplit[3])
						.price(dataSplit[4])
						.exchangeVolume(dataSplit[5])
						.generatedCoins(dataSplit[6])
						.fees(dataSplit[7])
						.build();
				etherRepository.save((Ethereum) etherCurrency);
			}
		}
	}

	private void loadDoge() {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream("doge.csv");
		Scanner sc = new Scanner(inputStream);
		if(!StringUtils.isEmpty(sc.nextLine())){
			while (sc.hasNextLine()){
				String[] dataSplit = sc.nextLine().split(",");

				DogeCoin dogeCurrency = DogeCoin.builder()
						.date(dataSplit[0])
						.txVolume(dataSplit[1])
						.txCount(dataSplit[2])
						.marketCap(dataSplit[3])
						.price(dataSplit[4])
						.exchangeVolume(dataSplit[5])
						.generatedCoins(dataSplit[6])
						.fees(dataSplit[7])
						.build();
				dogecoinRepository.save( dogeCurrency);
			}
		}
	}
}
