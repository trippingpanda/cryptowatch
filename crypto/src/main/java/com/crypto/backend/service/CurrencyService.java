package com.crypto.backend.service;

import com.crypto.backend.db.Currency;
import com.crypto.backend.db.enitity.Bitcoin;
import com.crypto.backend.db.enitity.DogeCoin;
import com.crypto.backend.db.enitity.Ethereum;
import com.crypto.backend.db.enitity.Litecoin;
import com.crypto.backend.helper.GuavaCacheHelper;
import com.crypto.backend.helper.RestHelper;
import com.crypto.backend.pojo.IntradayData;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

@Component
public class CurrencyService {

	LoadingCache<Currency,IntradayData> intradayDataLoadingCache;

	@Autowired
	private RestHelper restHelper;

	@Autowired
	private GuavaCacheHelper guavaCacheHelper;

	@PostConstruct
	public void postConstruct() {
		intradayDataLoadingCache = CacheBuilder.newBuilder()
				.maximumSize(10)
				.refreshAfterWrite(90, TimeUnit.SECONDS)
				.expireAfterWrite(12, TimeUnit.HOURS)
				.build(new CacheLoader<Currency, IntradayData>() {
					@Override
					public IntradayData load(Currency key) throws Exception {
						return restHelper.getIntradayData(key);
					}

					@Override
					public ListenableFuture<IntradayData> reload(Currency key, IntradayData oldValue) throws Exception {
						return Futures.immediateFuture(guavaCacheHelper.updateCurrencyDataAsync(key, oldValue, intradayDataLoadingCache));
					}
				});
	}

	public Bitcoin getBitcoinCurrentData() {
		try {
			IntradayData intradayData = intradayDataLoadingCache.get(Currency.BITCOIN);
			return Bitcoin.builder().price(intradayData.getPrice())
					.txVolume(intradayData.getVolume())
					.marketCap(intradayData.getMarketCap())
					.date(intradayData.getDate())
					.build();
		} catch (Exception e) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
			String format = simpleDateFormat.format(new Date(Instant.now().toEpochMilli()));
			return Bitcoin.builder().date(format)
					.txVolume("NA")
					.marketCap("NA")
					.price("NA")
					.build();
		}
	}

	public Ethereum getETHCurrentData() {
		try {
			IntradayData intradayData = intradayDataLoadingCache.get(Currency.ETHEREUM);
			return Ethereum.builder().price(intradayData.getPrice())
					.txVolume(intradayData.getVolume())
					.marketCap(intradayData.getMarketCap())
					.date(intradayData.getDate())
					.build();
		} catch (Exception e) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
			String format = simpleDateFormat.format(new Date(Instant.now().toEpochMilli()));
			return Ethereum.builder().date(format)
					.txVolume("NA")
					.marketCap("NA")
					.price("NA")
					.build();
		}
	}

	public Litecoin getLitecoinCurrentData() {
		try {
			IntradayData intradayData = intradayDataLoadingCache.get(Currency.LITECOIN);
			return Litecoin.builder().price(intradayData.getPrice())
					.txVolume(intradayData.getVolume())
					.marketCap(intradayData.getMarketCap())
					.date(intradayData.getDate())
					.build();
		} catch (Exception e) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
			String format = simpleDateFormat.format(new Date(Instant.now().toEpochMilli()));
			return Litecoin.builder().date(format)
					.txVolume("NA")
					.marketCap("NA")
					.price("NA")
					.build();
		}
	}

	public DogeCoin getDogecoinCurrentData() {
		try {
			IntradayData intradayData = intradayDataLoadingCache.get(Currency.DOGECOIN);
			return DogeCoin.builder().price(intradayData.getPrice())
					.txVolume(intradayData.getVolume())
					.marketCap(intradayData.getMarketCap())
					.date(intradayData.getDate())
					.build();
		} catch (Exception e) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
			String format = simpleDateFormat.format(new Date(Instant.now().toEpochMilli()));
			return DogeCoin.builder().date(format)
					.txVolume("NA")
					.marketCap("NA")
					.price("NA")
					.build();
		}
	}
}
