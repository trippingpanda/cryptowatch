package com.crypto.backend.service;

import com.crypto.backend.pojo.IntradayData;
import okhttp3.ConnectionPool;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.stereotype.Component;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

@Component
public class AlphavantageService {

	private AlphavantageApi alphavantageApi;

	private String alphavantageUrl="https://www.alphavantage.co/";

	@PostConstruct
	private void initialise() {
//		int CONNECT_TIMEOUT =  5000;
//		int READ_TIMEOUT =  5000;
		int POOL_SIZE = 50;
		ConnectionPool pool = new ConnectionPool(POOL_SIZE, 5, TimeUnit.MINUTES);
		OkHttpClient httpClient = new OkHttpClient.Builder()
				.addNetworkInterceptor(new Interceptor() {
					@Override
					public okhttp3.Response intercept(Chain chain) throws IOException {
						Request request = chain
								.request()
								.newBuilder()
								.addHeader("Content-Type", "application/json")
								.build();
						return chain.proceed(request);
					}
				})
				.connectionPool(pool)
//				.connectTimeout(CONNECT_TIMEOUT, TimeUnit.MILLISECONDS)
//				.readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS)
				.build();

		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(alphavantageUrl)
				.addConverterFactory(JacksonConverterFactory.create())
				.client(httpClient)
				.build();

		this.alphavantageApi = retrofit.create(AlphavantageApi.class);
	}

	public IntradayData getIntradayDataBTC(){
		Call<Object> intraDayDataBTC = alphavantageApi.getIntraDayDataBTC();
		IntradayData responseMap = getIntradayData(intraDayDataBTC);
		if (responseMap != null) return responseMap;
		System.out.println("asd");
		return null;
	}

	public IntradayData getIntradayDataLTC(){
		Call<Object> intraDayDataLTC = alphavantageApi.getIntraDayDataLTC();
		IntradayData responseMap = getIntradayData(intraDayDataLTC);
		if (responseMap != null) return responseMap;
		System.out.println("asd");
		return null;
	}

	public IntradayData getIntradayDataETH(){
		Call<Object> intraDayDataETH = alphavantageApi.getIntraDayDataETH();
		IntradayData responseMap = getIntradayData(intraDayDataETH);
		if (responseMap != null) return responseMap;
		System.out.println("asd");
		return null;
	}


	public IntradayData getIntradayDataDOGE(){
		Call<Object> intraDayDataDOGE = alphavantageApi.getIntraDayDataDOGE();
		IntradayData responseMap = getIntradayData(intraDayDataDOGE);
		if (responseMap != null) return responseMap;
		System.out.println("asd");
		return null;
	}
	private IntradayData getIntradayData(Call<Object> intraDayDataLTC) {
		try {
			Response<Object> execute = intraDayDataLTC.execute();
			if(execute.isSuccessful()){
				HashMap<String,HashMap> body = (HashMap<String, HashMap>) execute.body();
				HashMap map = body.get("Time Series (Digital Currency Intraday)");
				String next = String.valueOf(map.keySet().iterator().next());
				HashMap responseMap = (HashMap) map.get(next);
				return IntradayData.builder().price(String.valueOf(responseMap.get("1a. price (USD)")))
						.volume(String.valueOf(responseMap.get("2. volume")))
						.marketCap(String.valueOf(responseMap.get("3. market cap (USD)")))
						.date(next.split(" ")[0])
						.build();

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
