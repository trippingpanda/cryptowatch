package com.crypto.backend.helper;

import com.crypto.backend.db.Currency;
import com.crypto.backend.pojo.IntradayData;
import com.crypto.backend.service.AlphavantageService;
import com.google.common.cache.LoadingCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;

@Component
public class RestHelper {

	@Autowired
	private AlphavantageService alphavantageService;

	public IntradayData getIntradayData(Currency currency) {
		switch (currency){
			case BITCOIN:
				return alphavantageService.getIntradayDataBTC();
			case ETHEREUM:
				return alphavantageService.getIntradayDataETH();
			case LITECOIN:
				return alphavantageService.getIntradayDataLTC();
			case DOGECOIN:
				return alphavantageService.getIntradayDataDOGE();
		}
		return null;
	}

	@Async
	public void updateCurrencyData(Currency currency, LoadingCache<Currency, IntradayData> intradayDataLoadingCache) {
		switch (currency){
			case BITCOIN:
				intradayDataLoadingCache.put(currency,alphavantageService.getIntradayDataBTC());
				break;
			case ETHEREUM:
				intradayDataLoadingCache.put(currency,alphavantageService.getIntradayDataETH());
				break;
			case LITECOIN:
				intradayDataLoadingCache.put(currency,alphavantageService.getIntradayDataLTC());
				break;
			case DOGECOIN:
				intradayDataLoadingCache.put(currency,alphavantageService.getIntradayDataDOGE());
				break;
		}
	}
}
