package com.crypto.backend.helper;

import com.crypto.backend.db.Currency;
import com.crypto.backend.pojo.IntradayData;
import com.google.common.cache.LoadingCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GuavaCacheHelper {
	@Autowired
	private RestHelper restHelper;

	public IntradayData updateCurrencyDataAsync(Currency key, IntradayData oldValue, LoadingCache<Currency, IntradayData> intradayDataLoadingCache) {
		restHelper.updateCurrencyData(key,intradayDataLoadingCache);
		return oldValue;
	}
}
