package com.crypto.backend.controller;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Builder
@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataResponse {
	private String date;

	private String txVolume;

	private String txCount;

	private String marketCap;

	private String price;

	private String exchangeVolume;

	private String generatedCoins;

	private String fees;

}
