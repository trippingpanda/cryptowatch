package com.crypto.backend.controller;

import com.crypto.backend.db.Currency;
import com.crypto.backend.db.enitity.Bitcoin;
import com.crypto.backend.db.enitity.DogeCoin;
import com.crypto.backend.db.enitity.Ethereum;
import com.crypto.backend.db.enitity.Litecoin;
import com.crypto.backend.db.repository.BitcoinRepository;
import com.crypto.backend.db.repository.DogecoinRepository;
import com.crypto.backend.db.repository.EtherRepository;
import com.crypto.backend.db.repository.LitecoinRepository;
import com.crypto.backend.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DataService {

	@Autowired
	private BitcoinRepository bitcoinRepository;

	@Autowired
	private EtherRepository etherRepository;

	@Autowired
	private LitecoinRepository litecoinRepository;

	@Autowired
	private DogecoinRepository dogecoinRepository;

	@Autowired
	private CurrencyService currencyService;

	public List<DataResponse> getData(Currency currency) {
		switch (currency){
			case BITCOIN:
				return getBitcoinResponse();
			case ETHEREUM:
				return getETHresponse();
			case DOGECOIN:
				return getDogeCointResponse();
			case LITECOIN:
				return getLitecointResponse();
		}
		return null;
	}

	private List<DataResponse> getLitecointResponse() {
		List<Litecoin> all = litecoinRepository.findAll();
		Litecoin litecoinCurrentData = currencyService.getLitecoinCurrentData();
		if(litecoinCurrentData != null){
			all.add(litecoinCurrentData);
		}


		return all.stream().map(x -> DataResponse.builder().price(x.getPrice())
				.marketCap(x.getMarketCap())
				.txVolume(x.getTxVolume())
				.date(x.getDate())
				.exchangeVolume(x.getExchangeVolume())
				.fees(x.getFees())
				.generatedCoins(x.getGeneratedCoins())
				.txCount(x.getTxCount())
				.build()).collect(Collectors.toList());
	}

	private List<DataResponse> getDogeCointResponse() {
		List<DogeCoin> all = dogecoinRepository.findAll();
		DogeCoin dogecoinCurrentData = currencyService.getDogecoinCurrentData();
		if(dogecoinCurrentData != null){
			all.add(dogecoinCurrentData);
		}


		return all.stream().map(x -> DataResponse.builder().price(x.getPrice())
				.marketCap(x.getMarketCap())
				.txVolume(x.getTxVolume())
				.date(x.getDate())
				.exchangeVolume(x.getExchangeVolume())
				.fees(x.getFees())
				.generatedCoins(x.getGeneratedCoins())
				.txCount(x.getTxCount())
				.build()).collect(Collectors.toList());
	}

	private List<DataResponse> getETHresponse() {
		List<Ethereum> all = etherRepository.findAll();
		Ethereum  ethCurrentData= currencyService.getETHCurrentData();
		if(ethCurrentData != null){
			all.add(ethCurrentData);
		}


		return all.stream().map(x -> DataResponse.builder().price(x.getPrice())
				.marketCap(x.getMarketCap())
				.txVolume(x.getTxVolume())
				.date(x.getDate())
				.exchangeVolume(x.getExchangeVolume())
				.fees(x.getFees())
				.generatedCoins(x.getGeneratedCoins())
				.txCount(x.getTxCount())
				.build()).collect(Collectors.toList());
	}

	private List<DataResponse> getBitcoinResponse() {
		List<Bitcoin> all = bitcoinRepository.findAll();
		Bitcoin bitcoinCurrentData = currencyService.getBitcoinCurrentData();
		if(bitcoinCurrentData != null){
			all.add(bitcoinCurrentData);
		}


		return all.stream().map(x -> DataResponse.builder().price(x.getPrice())
				.marketCap(x.getMarketCap())
				.txVolume(x.getTxVolume())
				.date(x.getDate())
				.exchangeVolume(x.getExchangeVolume())
				.fees(x.getFees())
				.generatedCoins(x.getGeneratedCoins())
				.txCount(x.getTxCount())
				.build()).collect(Collectors.toList());
	}


}
