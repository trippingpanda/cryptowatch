package com.crypto.backend.controller;

import com.crypto.backend.db.Currency;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/test")
@Slf4j
public class CurrencyDataController {


	@Autowired
	private DataService dataService;

	@RequestMapping(
			value = "/{currency}",
			method = RequestMethod.GET
	)
	@ResponseBody
	public List<DataResponse> test(@PathVariable("currency") String input) {
		Currency currency = Currency.get(input);

		return dataService.getData(currency);
	}
}
