package com.crypto.backend.db;

import jdk.nashorn.internal.objects.annotations.Getter;

public enum Currency {
	BITCOIN("BTC"),
	ETHEREUM("ETH"),
	DOGECOIN("DOGE"),
	LITECOIN("LTC");

	private String value ;

	@Getter
	public String getValue(){
		return value;
	}

	Currency(String value){
		this.value=value;
	}

	public static Currency get(String val) {
		for (Currency currency : Currency.values()) {
			if (currency.value.equals(val)) return currency;
		}
		return null;
	}
}
