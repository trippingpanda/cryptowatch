package com.crypto.backend.db.repository;

import com.crypto.backend.db.enitity.Ethereum;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtherRepository extends JpaRepository<Ethereum,String> {
}
