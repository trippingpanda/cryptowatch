package com.crypto.backend.db.repository;

import com.crypto.backend.db.enitity.DogeCoin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DogecoinRepository extends JpaRepository<DogeCoin,String> {
}
