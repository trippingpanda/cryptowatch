package com.crypto.backend.db.repository;


import com.crypto.backend.db.enitity.Bitcoin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BitcoinRepository extends JpaRepository<Bitcoin,String> {
}
