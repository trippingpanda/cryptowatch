package com.crypto.backend.db.enitity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;


@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Litecoin {
	@Id
	private String date;

	private String txVolume;

	private String txCount;

	private String marketCap;

	private String price;

	private String exchangeVolume;

	private String generatedCoins;

	private String fees;
}
