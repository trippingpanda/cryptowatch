package com.crypto.backend.db.repository;

import com.crypto.backend.db.enitity.Litecoin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LitecoinRepository extends JpaRepository<Litecoin,String> {
}
