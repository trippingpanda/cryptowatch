// Generated from ForGrammar.g4 by ANTLR 4.5.3

package com.swiggy.alchemist.antlr;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ForGrammarLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		KEY=1, EQ=2, OR=3, TEXT=4, WS=5;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"KEY", "INCENTIVE_GROUP", "ZONE_ID", "CITY_ID", "EQ", "OR", "TEXT", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, "'='"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "KEY", "EQ", "OR", "TEXT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public ForGrammarLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "ForGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\7f\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\3\2\3\2\3\2\5\2"+
		"\27\n\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\67\n\3"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4A\n\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5"+
		"\3\5\5\5K\n\5\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7Y\n\7"+
		"\3\b\6\b\\\n\b\r\b\16\b]\3\t\6\ta\n\t\r\t\16\tb\3\t\3\t\2\2\n\3\3\5\2"+
		"\7\2\t\2\13\4\r\5\17\6\21\7\3\2\4\b\2$$\'(*^aac}\177\177\5\2\13\f\17\17"+
		"\"\"l\2\3\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\3"+
		"\26\3\2\2\2\5\66\3\2\2\2\7@\3\2\2\2\tJ\3\2\2\2\13L\3\2\2\2\rX\3\2\2\2"+
		"\17[\3\2\2\2\21`\3\2\2\2\23\27\5\5\3\2\24\27\5\7\4\2\25\27\5\t\5\2\26"+
		"\23\3\2\2\2\26\24\3\2\2\2\26\25\3\2\2\2\27\4\3\2\2\2\30\31\7k\2\2\31\32"+
		"\7p\2\2\32\33\7e\2\2\33\34\7g\2\2\34\35\7p\2\2\35\36\7v\2\2\36\37\7k\2"+
		"\2\37 \7x\2\2 !\7g\2\2!\"\7a\2\2\"#\7i\2\2#$\7t\2\2$%\7q\2\2%&\7w\2\2"+
		"&\67\7r\2\2\'(\7K\2\2()\7P\2\2)*\7E\2\2*+\7G\2\2+,\7P\2\2,-\7V\2\2-.\7"+
		"K\2\2./\7X\2\2/\60\7G\2\2\60\61\7a\2\2\61\62\7I\2\2\62\63\7T\2\2\63\64"+
		"\7Q\2\2\64\65\7W\2\2\65\67\7R\2\2\66\30\3\2\2\2\66\'\3\2\2\2\67\6\3\2"+
		"\2\289\7|\2\29:\7q\2\2:;\7p\2\2;A\7g\2\2<=\7\\\2\2=>\7Q\2\2>?\7P\2\2?"+
		"A\7G\2\2@8\3\2\2\2@<\3\2\2\2A\b\3\2\2\2BC\7e\2\2CD\7k\2\2DE\7v\2\2EK\7"+
		"{\2\2FG\7E\2\2GH\7K\2\2HI\7V\2\2IK\7[\2\2JB\3\2\2\2JF\3\2\2\2K\n\3\2\2"+
		"\2LM\7?\2\2M\f\3\2\2\2NO\7Q\2\2OY\7T\2\2PQ\7q\2\2QY\7t\2\2RS\7C\2\2ST"+
		"\7P\2\2TY\7F\2\2UV\7c\2\2VW\7p\2\2WY\7f\2\2XN\3\2\2\2XP\3\2\2\2XR\3\2"+
		"\2\2XU\3\2\2\2Y\16\3\2\2\2Z\\\t\2\2\2[Z\3\2\2\2\\]\3\2\2\2][\3\2\2\2]"+
		"^\3\2\2\2^\20\3\2\2\2_a\t\3\2\2`_\3\2\2\2ab\3\2\2\2b`\3\2\2\2bc\3\2\2"+
		"\2cd\3\2\2\2de\b\t\2\2e\22\3\2\2\2\n\2\26\66@JX]b\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}