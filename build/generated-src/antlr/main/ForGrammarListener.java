// Generated from ForGrammar.g4 by ANTLR 4.5.3

package com.swiggy.alchemist.antlr;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ForGrammarParser}.
 */
public interface ForGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ForGrammarParser#ruleDomain}.
	 * @param ctx the parse tree
	 */
	void enterRuleDomain(ForGrammarParser.RuleDomainContext ctx);
	/**
	 * Exit a parse tree produced by {@link ForGrammarParser#ruleDomain}.
	 * @param ctx the parse tree
	 */
	void exitRuleDomain(ForGrammarParser.RuleDomainContext ctx);
	/**
	 * Enter a parse tree produced by {@link ForGrammarParser#pair}.
	 * @param ctx the parse tree
	 */
	void enterPair(ForGrammarParser.PairContext ctx);
	/**
	 * Exit a parse tree produced by {@link ForGrammarParser#pair}.
	 * @param ctx the parse tree
	 */
	void exitPair(ForGrammarParser.PairContext ctx);
}