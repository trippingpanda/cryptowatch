// Generated from Evaluator.g4 by ANTLR 4.5.3

package com.swiggy.alchemist.antlr;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class EvaluatorLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		INT=1, DOUBLE=2, POW=3, NL=4, WS=5, AND=6, OR=7, MAX=8, MIN=9, ID=10, 
		EQ=11, GT=12, LT=13, PLUS=14, EQUAL=15, MINUS=16, MULT=17, DIV=18, LPAR=19, 
		RPAR=20;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"INT", "DOUBLE", "POW", "NL", "WS", "AND", "OR", "MAX", "MIN", "ID", "EQ", 
		"GT", "LT", "PLUS", "EQUAL", "MINUS", "MULT", "DIV", "LPAR", "RPAR"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, "'^'", "'\n'", null, null, null, null, null, null, null, 
		"'>'", "'<'", "'+'", null, "'-'", "'*'", "'/'", "'('", "')'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "INT", "DOUBLE", "POW", "NL", "WS", "AND", "OR", "MAX", "MIN", "ID", 
		"EQ", "GT", "LT", "PLUS", "EQUAL", "MINUS", "MULT", "DIV", "LPAR", "RPAR"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public EvaluatorLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Evaluator.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\26\177\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\3\2\6\2-\n\2\r\2\16\2.\3\3\6\3\62\n\3\r"+
		"\3\16\3\63\3\3\3\3\6\38\n\3\r\3\16\39\3\4\3\4\3\5\3\5\3\6\6\6A\n\6\r\6"+
		"\16\6B\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\5\7M\n\7\3\b\3\b\3\b\3\b\5\bS\n"+
		"\b\3\t\3\t\3\t\3\t\3\t\3\t\5\t[\n\t\3\n\3\n\3\n\3\n\3\n\3\n\5\nc\n\n\3"+
		"\13\3\13\7\13g\n\13\f\13\16\13j\13\13\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3"+
		"\17\3\20\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\2\2\26"+
		"\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20"+
		"\37\21!\22#\23%\24\'\25)\26\3\2\6\3\2\62;\5\2\13\13\17\17\"\"\5\2C\\a"+
		"ac|\6\2\62;C\\aac|\u0087\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2"+
		"\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25"+
		"\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2"+
		"\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\3,\3\2\2"+
		"\2\5\61\3\2\2\2\7;\3\2\2\2\t=\3\2\2\2\13@\3\2\2\2\rL\3\2\2\2\17R\3\2\2"+
		"\2\21Z\3\2\2\2\23b\3\2\2\2\25d\3\2\2\2\27k\3\2\2\2\31m\3\2\2\2\33o\3\2"+
		"\2\2\35q\3\2\2\2\37s\3\2\2\2!u\3\2\2\2#w\3\2\2\2%y\3\2\2\2\'{\3\2\2\2"+
		")}\3\2\2\2+-\t\2\2\2,+\3\2\2\2-.\3\2\2\2.,\3\2\2\2./\3\2\2\2/\4\3\2\2"+
		"\2\60\62\t\2\2\2\61\60\3\2\2\2\62\63\3\2\2\2\63\61\3\2\2\2\63\64\3\2\2"+
		"\2\64\65\3\2\2\2\65\67\7\60\2\2\668\t\2\2\2\67\66\3\2\2\289\3\2\2\29\67"+
		"\3\2\2\29:\3\2\2\2:\6\3\2\2\2;<\7`\2\2<\b\3\2\2\2=>\7\f\2\2>\n\3\2\2\2"+
		"?A\t\3\2\2@?\3\2\2\2AB\3\2\2\2B@\3\2\2\2BC\3\2\2\2CD\3\2\2\2DE\b\6\2\2"+
		"E\f\3\2\2\2FG\7C\2\2GH\7P\2\2HM\7F\2\2IJ\7c\2\2JK\7p\2\2KM\7f\2\2LF\3"+
		"\2\2\2LI\3\2\2\2M\16\3\2\2\2NO\7Q\2\2OS\7T\2\2PQ\7q\2\2QS\7t\2\2RN\3\2"+
		"\2\2RP\3\2\2\2S\20\3\2\2\2TU\7O\2\2UV\7C\2\2V[\7Z\2\2WX\7o\2\2XY\7c\2"+
		"\2Y[\7z\2\2ZT\3\2\2\2ZW\3\2\2\2[\22\3\2\2\2\\]\7O\2\2]^\7K\2\2^c\7P\2"+
		"\2_`\7o\2\2`a\7k\2\2ac\7p\2\2b\\\3\2\2\2b_\3\2\2\2c\24\3\2\2\2dh\t\4\2"+
		"\2eg\t\5\2\2fe\3\2\2\2gj\3\2\2\2hf\3\2\2\2hi\3\2\2\2i\26\3\2\2\2jh\3\2"+
		"\2\2kl\7?\2\2l\30\3\2\2\2mn\7@\2\2n\32\3\2\2\2op\7>\2\2p\34\3\2\2\2qr"+
		"\7-\2\2r\36\3\2\2\2st\7?\2\2t \3\2\2\2uv\7/\2\2v\"\3\2\2\2wx\7,\2\2x$"+
		"\3\2\2\2yz\7\61\2\2z&\3\2\2\2{|\7*\2\2|(\3\2\2\2}~\7+\2\2~*\3\2\2\2\f"+
		"\2.\639BLRZbh\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}