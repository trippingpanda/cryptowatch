// Generated from ForGrammar.g4 by ANTLR 4.5.3

package com.swiggy.alchemist.antlr;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ForGrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ForGrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ForGrammarParser#ruleDomain}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRuleDomain(ForGrammarParser.RuleDomainContext ctx);
	/**
	 * Visit a parse tree produced by {@link ForGrammarParser#pair}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPair(ForGrammarParser.PairContext ctx);
}