// Generated from Evaluator.g4 by ANTLR 4.5.3

package com.swiggy.alchemist.antlr;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link EvaluatorParser}.
 */
public interface EvaluatorListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code CummulativeWithParen}
	 * labeled alternative in {@link EvaluatorParser#cummulative}.
	 * @param ctx the parse tree
	 */
	void enterCummulativeWithParen(EvaluatorParser.CummulativeWithParenContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CummulativeWithParen}
	 * labeled alternative in {@link EvaluatorParser#cummulative}.
	 * @param ctx the parse tree
	 */
	void exitCummulativeWithParen(EvaluatorParser.CummulativeWithParenContext ctx);
	/**
	 * Enter a parse tree produced by the {@code OrCondition}
	 * labeled alternative in {@link EvaluatorParser#cummulative}.
	 * @param ctx the parse tree
	 */
	void enterOrCondition(EvaluatorParser.OrConditionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code OrCondition}
	 * labeled alternative in {@link EvaluatorParser#cummulative}.
	 * @param ctx the parse tree
	 */
	void exitOrCondition(EvaluatorParser.OrConditionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ToEquation}
	 * labeled alternative in {@link EvaluatorParser#cummulative}.
	 * @param ctx the parse tree
	 */
	void enterToEquation(EvaluatorParser.ToEquationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ToEquation}
	 * labeled alternative in {@link EvaluatorParser#cummulative}.
	 * @param ctx the parse tree
	 */
	void exitToEquation(EvaluatorParser.ToEquationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AndCondition}
	 * labeled alternative in {@link EvaluatorParser#cummulative}.
	 * @param ctx the parse tree
	 */
	void enterAndCondition(EvaluatorParser.AndConditionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AndCondition}
	 * labeled alternative in {@link EvaluatorParser#cummulative}.
	 * @param ctx the parse tree
	 */
	void exitAndCondition(EvaluatorParser.AndConditionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code EqualTo}
	 * labeled alternative in {@link EvaluatorParser#equation}.
	 * @param ctx the parse tree
	 */
	void enterEqualTo(EvaluatorParser.EqualToContext ctx);
	/**
	 * Exit a parse tree produced by the {@code EqualTo}
	 * labeled alternative in {@link EvaluatorParser#equation}.
	 * @param ctx the parse tree
	 */
	void exitEqualTo(EvaluatorParser.EqualToContext ctx);
	/**
	 * Enter a parse tree produced by the {@code GreaterThan}
	 * labeled alternative in {@link EvaluatorParser#equation}.
	 * @param ctx the parse tree
	 */
	void enterGreaterThan(EvaluatorParser.GreaterThanContext ctx);
	/**
	 * Exit a parse tree produced by the {@code GreaterThan}
	 * labeled alternative in {@link EvaluatorParser#equation}.
	 * @param ctx the parse tree
	 */
	void exitGreaterThan(EvaluatorParser.GreaterThanContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LessThan}
	 * labeled alternative in {@link EvaluatorParser#equation}.
	 * @param ctx the parse tree
	 */
	void enterLessThan(EvaluatorParser.LessThanContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LessThan}
	 * labeled alternative in {@link EvaluatorParser#equation}.
	 * @param ctx the parse tree
	 */
	void exitLessThan(EvaluatorParser.LessThanContext ctx);
	/**
	 * Enter a parse tree produced by the {@code EquationWithParen}
	 * labeled alternative in {@link EvaluatorParser#equation}.
	 * @param ctx the parse tree
	 */
	void enterEquationWithParen(EvaluatorParser.EquationWithParenContext ctx);
	/**
	 * Exit a parse tree produced by the {@code EquationWithParen}
	 * labeled alternative in {@link EvaluatorParser#equation}.
	 * @param ctx the parse tree
	 */
	void exitEquationWithParen(EvaluatorParser.EquationWithParenContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MaxExpression}
	 * labeled alternative in {@link EvaluatorParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMaxExpression(EvaluatorParser.MaxExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MaxExpression}
	 * labeled alternative in {@link EvaluatorParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMaxExpression(EvaluatorParser.MaxExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code MinExpression}
	 * labeled alternative in {@link EvaluatorParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMinExpression(EvaluatorParser.MinExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code MinExpression}
	 * labeled alternative in {@link EvaluatorParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMinExpression(EvaluatorParser.MinExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Calculate}
	 * labeled alternative in {@link EvaluatorParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCalculate(EvaluatorParser.CalculateContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Calculate}
	 * labeled alternative in {@link EvaluatorParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCalculate(EvaluatorParser.CalculateContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ToMultOrDiv}
	 * labeled alternative in {@link EvaluatorParser#plusOrMinus}.
	 * @param ctx the parse tree
	 */
	void enterToMultOrDiv(EvaluatorParser.ToMultOrDivContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ToMultOrDiv}
	 * labeled alternative in {@link EvaluatorParser#plusOrMinus}.
	 * @param ctx the parse tree
	 */
	void exitToMultOrDiv(EvaluatorParser.ToMultOrDivContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Plus}
	 * labeled alternative in {@link EvaluatorParser#plusOrMinus}.
	 * @param ctx the parse tree
	 */
	void enterPlus(EvaluatorParser.PlusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Plus}
	 * labeled alternative in {@link EvaluatorParser#plusOrMinus}.
	 * @param ctx the parse tree
	 */
	void exitPlus(EvaluatorParser.PlusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Minus}
	 * labeled alternative in {@link EvaluatorParser#plusOrMinus}.
	 * @param ctx the parse tree
	 */
	void enterMinus(EvaluatorParser.MinusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Minus}
	 * labeled alternative in {@link EvaluatorParser#plusOrMinus}.
	 * @param ctx the parse tree
	 */
	void exitMinus(EvaluatorParser.MinusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Multiplication}
	 * labeled alternative in {@link EvaluatorParser#multOrDiv}.
	 * @param ctx the parse tree
	 */
	void enterMultiplication(EvaluatorParser.MultiplicationContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Multiplication}
	 * labeled alternative in {@link EvaluatorParser#multOrDiv}.
	 * @param ctx the parse tree
	 */
	void exitMultiplication(EvaluatorParser.MultiplicationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Division}
	 * labeled alternative in {@link EvaluatorParser#multOrDiv}.
	 * @param ctx the parse tree
	 */
	void enterDivision(EvaluatorParser.DivisionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Division}
	 * labeled alternative in {@link EvaluatorParser#multOrDiv}.
	 * @param ctx the parse tree
	 */
	void exitDivision(EvaluatorParser.DivisionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ToPow}
	 * labeled alternative in {@link EvaluatorParser#multOrDiv}.
	 * @param ctx the parse tree
	 */
	void enterToPow(EvaluatorParser.ToPowContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ToPow}
	 * labeled alternative in {@link EvaluatorParser#multOrDiv}.
	 * @param ctx the parse tree
	 */
	void exitToPow(EvaluatorParser.ToPowContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Power}
	 * labeled alternative in {@link EvaluatorParser#pow}.
	 * @param ctx the parse tree
	 */
	void enterPower(EvaluatorParser.PowerContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Power}
	 * labeled alternative in {@link EvaluatorParser#pow}.
	 * @param ctx the parse tree
	 */
	void exitPower(EvaluatorParser.PowerContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ChangeSign}
	 * labeled alternative in {@link EvaluatorParser#unaryMinus}.
	 * @param ctx the parse tree
	 */
	void enterChangeSign(EvaluatorParser.ChangeSignContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ChangeSign}
	 * labeled alternative in {@link EvaluatorParser#unaryMinus}.
	 * @param ctx the parse tree
	 */
	void exitChangeSign(EvaluatorParser.ChangeSignContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ToAtom}
	 * labeled alternative in {@link EvaluatorParser#unaryMinus}.
	 * @param ctx the parse tree
	 */
	void enterToAtom(EvaluatorParser.ToAtomContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ToAtom}
	 * labeled alternative in {@link EvaluatorParser#unaryMinus}.
	 * @param ctx the parse tree
	 */
	void exitToAtom(EvaluatorParser.ToAtomContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Double}
	 * labeled alternative in {@link EvaluatorParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterDouble(EvaluatorParser.DoubleContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Double}
	 * labeled alternative in {@link EvaluatorParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitDouble(EvaluatorParser.DoubleContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Int}
	 * labeled alternative in {@link EvaluatorParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterInt(EvaluatorParser.IntContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Int}
	 * labeled alternative in {@link EvaluatorParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitInt(EvaluatorParser.IntContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Variable}
	 * labeled alternative in {@link EvaluatorParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterVariable(EvaluatorParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Variable}
	 * labeled alternative in {@link EvaluatorParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitVariable(EvaluatorParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by the {@code Braces}
	 * labeled alternative in {@link EvaluatorParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterBraces(EvaluatorParser.BracesContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Braces}
	 * labeled alternative in {@link EvaluatorParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitBraces(EvaluatorParser.BracesContext ctx);
	/**
	 * Enter a parse tree produced by {@link EvaluatorParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterCondition(EvaluatorParser.ConditionContext ctx);
	/**
	 * Exit a parse tree produced by {@link EvaluatorParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitCondition(EvaluatorParser.ConditionContext ctx);
}