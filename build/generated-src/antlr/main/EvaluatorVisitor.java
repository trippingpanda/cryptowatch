// Generated from Evaluator.g4 by ANTLR 4.5.3

package com.swiggy.alchemist.antlr;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link EvaluatorParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface EvaluatorVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by the {@code CummulativeWithParen}
	 * labeled alternative in {@link EvaluatorParser#cummulative}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCummulativeWithParen(EvaluatorParser.CummulativeWithParenContext ctx);
	/**
	 * Visit a parse tree produced by the {@code OrCondition}
	 * labeled alternative in {@link EvaluatorParser#cummulative}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrCondition(EvaluatorParser.OrConditionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ToEquation}
	 * labeled alternative in {@link EvaluatorParser#cummulative}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToEquation(EvaluatorParser.ToEquationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code AndCondition}
	 * labeled alternative in {@link EvaluatorParser#cummulative}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndCondition(EvaluatorParser.AndConditionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code EqualTo}
	 * labeled alternative in {@link EvaluatorParser#equation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualTo(EvaluatorParser.EqualToContext ctx);
	/**
	 * Visit a parse tree produced by the {@code GreaterThan}
	 * labeled alternative in {@link EvaluatorParser#equation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGreaterThan(EvaluatorParser.GreaterThanContext ctx);
	/**
	 * Visit a parse tree produced by the {@code LessThan}
	 * labeled alternative in {@link EvaluatorParser#equation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLessThan(EvaluatorParser.LessThanContext ctx);
	/**
	 * Visit a parse tree produced by the {@code EquationWithParen}
	 * labeled alternative in {@link EvaluatorParser#equation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEquationWithParen(EvaluatorParser.EquationWithParenContext ctx);
	/**
	 * Visit a parse tree produced by the {@code MaxExpression}
	 * labeled alternative in {@link EvaluatorParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMaxExpression(EvaluatorParser.MaxExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code MinExpression}
	 * labeled alternative in {@link EvaluatorParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinExpression(EvaluatorParser.MinExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Calculate}
	 * labeled alternative in {@link EvaluatorParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCalculate(EvaluatorParser.CalculateContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ToMultOrDiv}
	 * labeled alternative in {@link EvaluatorParser#plusOrMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToMultOrDiv(EvaluatorParser.ToMultOrDivContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Plus}
	 * labeled alternative in {@link EvaluatorParser#plusOrMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlus(EvaluatorParser.PlusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Minus}
	 * labeled alternative in {@link EvaluatorParser#plusOrMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinus(EvaluatorParser.MinusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Multiplication}
	 * labeled alternative in {@link EvaluatorParser#multOrDiv}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplication(EvaluatorParser.MultiplicationContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Division}
	 * labeled alternative in {@link EvaluatorParser#multOrDiv}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivision(EvaluatorParser.DivisionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ToPow}
	 * labeled alternative in {@link EvaluatorParser#multOrDiv}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToPow(EvaluatorParser.ToPowContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Power}
	 * labeled alternative in {@link EvaluatorParser#pow}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPower(EvaluatorParser.PowerContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ChangeSign}
	 * labeled alternative in {@link EvaluatorParser#unaryMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitChangeSign(EvaluatorParser.ChangeSignContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ToAtom}
	 * labeled alternative in {@link EvaluatorParser#unaryMinus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitToAtom(EvaluatorParser.ToAtomContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Double}
	 * labeled alternative in {@link EvaluatorParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDouble(EvaluatorParser.DoubleContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Int}
	 * labeled alternative in {@link EvaluatorParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInt(EvaluatorParser.IntContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Variable}
	 * labeled alternative in {@link EvaluatorParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(EvaluatorParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by the {@code Braces}
	 * labeled alternative in {@link EvaluatorParser#atom}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBraces(EvaluatorParser.BracesContext ctx);
	/**
	 * Visit a parse tree produced by {@link EvaluatorParser#condition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCondition(EvaluatorParser.ConditionContext ctx);
}