// Generated from RuleGrammar.g4 by ANTLR 4.5.3

package com.swiggy.alchemist.antlr;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class RuleGrammarLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, KEY=2, TEXT=3, NUMBER=4, WS=5;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "KEY", "TEXT", "NUMBER", "DIGIT", "NAME", "RULE", "BONUS", "DESCRIPTION", 
		"STATUS_EXP", "STATUS_MESSAGE", "SCOPE", "FOR", "CONDITION", "TIMESLOT", 
		"FROM", "TO", "DEFINE", "COMPUTE_ON", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'RULE'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, "KEY", "TEXT", "NUMBER", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public RuleGrammarLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "RuleGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\7\u00d4\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3?\n\3\3\4\6\4B\n\4\r\4\16\4C\3"+
		"\5\5\5G\n\5\3\5\6\5J\n\5\r\5\16\5K\3\5\3\5\7\5P\n\5\f\5\16\5S\13\5\3\5"+
		"\5\5V\n\5\3\5\6\5Y\n\5\r\5\16\5Z\5\5]\n\5\3\6\3\6\3\7\3\7\3\7\3\7\3\7"+
		"\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3"+
		"\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3"+
		"\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\17\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3"+
		"\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3"+
		"\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\25\6\25\u00cf"+
		"\n\25\r\25\16\25\u00d0\3\25\3\25\2\2\26\3\3\5\4\7\5\t\6\13\2\r\2\17\2"+
		"\21\2\23\2\25\2\27\2\31\2\33\2\35\2\37\2!\2#\2%\2\'\2)\7\3\2\5\b\2$$\'"+
		"(*^aac}\177\177\4\2--//\5\2\13\f\17\17\"\"\u00d9\2\3\3\2\2\2\2\5\3\2\2"+
		"\2\2\7\3\2\2\2\2\t\3\2\2\2\2)\3\2\2\2\3+\3\2\2\2\5>\3\2\2\2\7A\3\2\2\2"+
		"\t\\\3\2\2\2\13^\3\2\2\2\r`\3\2\2\2\17e\3\2\2\2\21j\3\2\2\2\23p\3\2\2"+
		"\2\25|\3\2\2\2\27\u0087\3\2\2\2\31\u0096\3\2\2\2\33\u009c\3\2\2\2\35\u00a0"+
		"\3\2\2\2\37\u00aa\3\2\2\2!\u00b3\3\2\2\2#\u00b8\3\2\2\2%\u00bb\3\2\2\2"+
		"\'\u00c2\3\2\2\2)\u00ce\3\2\2\2+,\7T\2\2,-\7W\2\2-.\7N\2\2./\7G\2\2/\4"+
		"\3\2\2\2\60?\5\r\7\2\61?\5\23\n\2\62?\5\21\t\2\63?\5\23\n\2\64?\5\27\f"+
		"\2\65?\5\25\13\2\66?\5\31\r\2\67?\5\33\16\28?\5\35\17\29?\5\37\20\2:?"+
		"\5!\21\2;?\5#\22\2<?\5%\23\2=?\5\'\24\2>\60\3\2\2\2>\61\3\2\2\2>\62\3"+
		"\2\2\2>\63\3\2\2\2>\64\3\2\2\2>\65\3\2\2\2>\66\3\2\2\2>\67\3\2\2\2>8\3"+
		"\2\2\2>9\3\2\2\2>:\3\2\2\2>;\3\2\2\2><\3\2\2\2>=\3\2\2\2?\6\3\2\2\2@B"+
		"\t\2\2\2A@\3\2\2\2BC\3\2\2\2CA\3\2\2\2CD\3\2\2\2D\b\3\2\2\2EG\t\3\2\2"+
		"FE\3\2\2\2FG\3\2\2\2GI\3\2\2\2HJ\5\13\6\2IH\3\2\2\2JK\3\2\2\2KI\3\2\2"+
		"\2KL\3\2\2\2LM\3\2\2\2MQ\7\60\2\2NP\5\13\6\2ON\3\2\2\2PS\3\2\2\2QO\3\2"+
		"\2\2QR\3\2\2\2R]\3\2\2\2SQ\3\2\2\2TV\t\3\2\2UT\3\2\2\2UV\3\2\2\2VX\3\2"+
		"\2\2WY\5\13\6\2XW\3\2\2\2YZ\3\2\2\2ZX\3\2\2\2Z[\3\2\2\2[]\3\2\2\2\\F\3"+
		"\2\2\2\\U\3\2\2\2]\n\3\2\2\2^_\4\62;\2_\f\3\2\2\2`a\7P\2\2ab\7C\2\2bc"+
		"\7O\2\2cd\7G\2\2d\16\3\2\2\2ef\7T\2\2fg\7W\2\2gh\7N\2\2hi\7G\2\2i\20\3"+
		"\2\2\2jk\7D\2\2kl\7Q\2\2lm\7P\2\2mn\7W\2\2no\7U\2\2o\22\3\2\2\2pq\7F\2"+
		"\2qr\7G\2\2rs\7U\2\2st\7E\2\2tu\7T\2\2uv\7K\2\2vw\7R\2\2wx\7V\2\2xy\7"+
		"K\2\2yz\7Q\2\2z{\7P\2\2{\24\3\2\2\2|}\7U\2\2}~\7V\2\2~\177\7C\2\2\177"+
		"\u0080\7V\2\2\u0080\u0081\7W\2\2\u0081\u0082\7U\2\2\u0082\u0083\7\"\2"+
		"\2\u0083\u0084\7G\2\2\u0084\u0085\7Z\2\2\u0085\u0086\7R\2\2\u0086\26\3"+
		"\2\2\2\u0087\u0088\7U\2\2\u0088\u0089\7V\2\2\u0089\u008a\7C\2\2\u008a"+
		"\u008b\7V\2\2\u008b\u008c\7W\2\2\u008c\u008d\7U\2\2\u008d\u008e\7\"\2"+
		"\2\u008e\u008f\7O\2\2\u008f\u0090\7G\2\2\u0090\u0091\7U\2\2\u0091\u0092"+
		"\7U\2\2\u0092\u0093\7C\2\2\u0093\u0094\7I\2\2\u0094\u0095\7G\2\2\u0095"+
		"\30\3\2\2\2\u0096\u0097\7U\2\2\u0097\u0098\7E\2\2\u0098\u0099\7Q\2\2\u0099"+
		"\u009a\7R\2\2\u009a\u009b\7G\2\2\u009b\32\3\2\2\2\u009c\u009d\7H\2\2\u009d"+
		"\u009e\7Q\2\2\u009e\u009f\7T\2\2\u009f\34\3\2\2\2\u00a0\u00a1\7E\2\2\u00a1"+
		"\u00a2\7Q\2\2\u00a2\u00a3\7P\2\2\u00a3\u00a4\7F\2\2\u00a4\u00a5\7K\2\2"+
		"\u00a5\u00a6\7V\2\2\u00a6\u00a7\7K\2\2\u00a7\u00a8\7Q\2\2\u00a8\u00a9"+
		"\7P\2\2\u00a9\36\3\2\2\2\u00aa\u00ab\7V\2\2\u00ab\u00ac\7K\2\2\u00ac\u00ad"+
		"\7O\2\2\u00ad\u00ae\7G\2\2\u00ae\u00af\7U\2\2\u00af\u00b0\7N\2\2\u00b0"+
		"\u00b1\7Q\2\2\u00b1\u00b2\7V\2\2\u00b2 \3\2\2\2\u00b3\u00b4\7H\2\2\u00b4"+
		"\u00b5\7T\2\2\u00b5\u00b6\7Q\2\2\u00b6\u00b7\7O\2\2\u00b7\"\3\2\2\2\u00b8"+
		"\u00b9\7V\2\2\u00b9\u00ba\7Q\2\2\u00ba$\3\2\2\2\u00bb\u00bc\7F\2\2\u00bc"+
		"\u00bd\7G\2\2\u00bd\u00be\7H\2\2\u00be\u00bf\7K\2\2\u00bf\u00c0\7P\2\2"+
		"\u00c0\u00c1\7G\2\2\u00c1&\3\2\2\2\u00c2\u00c3\7E\2\2\u00c3\u00c4\7Q\2"+
		"\2\u00c4\u00c5\7O\2\2\u00c5\u00c6\7R\2\2\u00c6\u00c7\7W\2\2\u00c7\u00c8"+
		"\7V\2\2\u00c8\u00c9\7G\2\2\u00c9\u00ca\7\"\2\2\u00ca\u00cb\7Q\2\2\u00cb"+
		"\u00cc\7P\2\2\u00cc(\3\2\2\2\u00cd\u00cf\t\4\2\2\u00ce\u00cd\3\2\2\2\u00cf"+
		"\u00d0\3\2\2\2\u00d0\u00ce\3\2\2\2\u00d0\u00d1\3\2\2\2\u00d1\u00d2\3\2"+
		"\2\2\u00d2\u00d3\b\25\2\2\u00d3*\3\2\2\2\f\2>CFKQUZ\\\u00d0\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}