// Generated from RuleGrammar.g4 by ANTLR 4.5.3

package com.swiggy.alchemist.antlr;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link RuleGrammarParser}.
 */
public interface RuleGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link RuleGrammarParser#incentiveRule}.
	 * @param ctx the parse tree
	 */
	void enterIncentiveRule(RuleGrammarParser.IncentiveRuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleGrammarParser#incentiveRule}.
	 * @param ctx the parse tree
	 */
	void exitIncentiveRule(RuleGrammarParser.IncentiveRuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleGrammarParser#pair}.
	 * @param ctx the parse tree
	 */
	void enterPair(RuleGrammarParser.PairContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleGrammarParser#pair}.
	 * @param ctx the parse tree
	 */
	void exitPair(RuleGrammarParser.PairContext ctx);
}