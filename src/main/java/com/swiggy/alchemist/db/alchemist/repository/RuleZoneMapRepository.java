package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.RuleZoneMapEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by RAVISINGH on 06/10/16.
 */
public interface RuleZoneMapRepository extends CrudRepository<RuleZoneMapEntity,Long> {

    @Modifying
    @Query( "delete from RuleZoneMapEntity where ruleId = :ruleId" )
    void deleteByRuleId(@Param("ruleId") Long ruleId);

    @Query( "select id from RuleZoneMapEntity where zoneId in :zones" )
    List<Long> findByZones(@Param("zones") List<Long> zones);
}
