package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.IncentiveGroupEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by RAVISINGH on 04/10/16.
 */
public interface IncentiveGroupRespository extends CrudRepository<IncentiveGroupEntity,Long> {


    @Query( "select id from IncentiveGroupEntity where name in :names" )
    List<Long> findByNames(@Param("names") List<String> names);

}

