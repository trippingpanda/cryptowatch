package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.IncentiveConfigEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface IncentiveConfigRepository extends CrudRepository<IncentiveConfigEntity,Long> {
    IncentiveConfigEntity findByMetaKey(String metaKey);

    List<IncentiveConfigEntity> findAll();


}
