package com.swiggy.alchemist.db.alchemist.entities;

import javax.persistence.*;

/**
 * Created by RAVISINGH on 04/10/16.
 */
@Entity
@Table(name = "incentive_group_rules_mapping")
public class IncentiveGroupRuleMapEntity {
    private long id;
    private long ruleId;
    private long incentiveGroupId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "rule_id")
    public long getRuleId() {
        return ruleId;
    }

    public void setRuleId(long ruleId) {
        this.ruleId = ruleId;
    }

    @Basic
    @Column(name = "incentive_group_id")
    public long getIncentiveGroupId() {
        return incentiveGroupId;
    }

    public void setIncentiveGroupId(long incentiveGroupId) {
        this.incentiveGroupId = incentiveGroupId;
    }
}
