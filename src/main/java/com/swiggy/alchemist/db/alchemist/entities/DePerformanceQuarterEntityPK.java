package com.swiggy.alchemist.db.alchemist.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class DePerformanceQuarterEntityPK implements Serializable {
    private long deId;
    private String quarterId;

    @Column(name = "de_id")
    @Id
    public long getDeId() {
        return deId;
    }

    public void setDeId(long deId) {
        this.deId = deId;
    }

    @Column(name = "quarter_id")
    @Id
    public String getQuarterId() {
        return quarterId;
    }

    public void setQuarterId(String quarterId) {
        this.quarterId = quarterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DePerformanceQuarterEntityPK that = (DePerformanceQuarterEntityPK) o;

        if (deId != that.deId) return false;
        if (quarterId != null ? !quarterId.equals(that.quarterId) : that.quarterId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (deId ^ (deId >>> 32));
        result = 31 * result + (quarterId != null ? quarterId.hashCode() : 0);
        return result;
    }
}
