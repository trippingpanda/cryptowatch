package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.DeTimeslotsEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by RAVISINGH on 28/09/16.
 */
public interface TimeSlotRespository extends CrudRepository<DeTimeslotsEntity,Long> {

    List<DeTimeslotsEntity> findAll();

    List<DeTimeslotsEntity> findByStartTimeEqualsAndEndTimeEqualsAndWeekdaysEquals(int startTime,
                                                                                   int endTime, String weekdays);

    /**
     *
     * @param time
     * @param dayOfWeek
     * @return
     */
    @Query("select id from DeTimeslotsEntity where startTime <= :time and endTime >= :time and weekdays like %:dayOfWeek%")
    List<Long> findByTimeSlot(@Param("time") int time, @Param("dayOfWeek") String dayOfWeek);
}

