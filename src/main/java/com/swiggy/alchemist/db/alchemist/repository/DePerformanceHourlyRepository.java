package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.DePerformanceHourlyEntity;
import com.swiggy.alchemist.db.alchemist.entities.DePerformanceHourlyEntityPK;
import org.springframework.data.repository.CrudRepository;

public interface DePerformanceHourlyRepository extends CrudRepository<DePerformanceHourlyEntity,DePerformanceHourlyEntityPK> {
}
