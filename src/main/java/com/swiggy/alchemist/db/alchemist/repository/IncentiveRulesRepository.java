package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.IncentiveRulesEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IncentiveRulesRepository extends CrudRepository<IncentiveRulesEntity,Long> {

     IncentiveRulesEntity findById(long id);

     IncentiveRulesEntity findByRuleName (String ruleName);


     @Query("select distinct ir from IncentiveRulesEntity ir, IncentiveGroupRuleMapEntity igrm, RuleZoneMapEntity rzm" +
             " where ir.active=1 and ir.id = igrm.ruleId and ir.id = rzm.ruleId and ir.id = rzm.ruleId" +
             " and igrm.incentiveGroupId in :incentiveGroup" +
             " and (ir.computeOn is null or ir.computeOn in :computeOn)" +
             " and rzm.zoneId = :zoneId and (ir.validityStartTime is null or  ir.validityStartTime < CURRENT_TIMESTAMP )" +
             " and (ir.validityEndTime is null or ir.validityEndTime > CURRENT_TIMESTAMP)" +
             " and not exists (select tsrm from TimeSlotRuleMapEntity tsrm where tsrm.ruleId = igrm.ruleId)")
     List<IncentiveRulesEntity> findRecurrentRules(@Param("incentiveGroup") List<Long> incentiveGroupId,
                                                   @Param("zoneId") long zoneId, @Param("computeOn") List<String> computeOn);


     @Query("select distinct ir from IncentiveRulesEntity ir, TimeSlotRuleMapEntity tsrm, IncentiveGroupRuleMapEntity igrm, RuleZoneMapEntity rzm" +
             " where ir.active=1 and ir.id = tsrm.ruleId and ir.id = igrm.ruleId and ir.id = rzm.ruleId " +
             " and tsrm.timeSlotId in :timeSlotIds and igrm.incentiveGroupId in :incentiveGroup" +
             " and ir.computeOn = :computeOn" +
             " and rzm.zoneId = :zoneId and (ir.validityStartTime is null or  ir.validityStartTime < CURRENT_TIMESTAMP )" +
             " and (ir.validityEndTime is null or ir.validityEndTime > CURRENT_TIMESTAMP)")
     List<IncentiveRulesEntity> findValidRules(@Param("timeSlotIds") List<Long> validTimeSlots,
                                               @Param("incentiveGroup") List<Long> incentiveGroupId,
                                               @Param("computeOn") String computeOn,
                                               @Param("zoneId") long zoneId);


     @Query("select distinct ir from IncentiveRulesEntity ir, TimeSlotRuleMapEntity tsrm, IncentiveGroupRuleMapEntity igrm, RuleZoneMapEntity rzm" +
             " where ir.active=1 and ir.id = tsrm.ruleId and ir.id = igrm.ruleId and ir.id = rzm.ruleId" +
             " and tsrm.timeSlotId in :timeSlotIds and igrm.incentiveGroupId in :incentiveGroup" +
             " and ir.computeOn in :computeOn" +
             " and rzm.zoneId = :zoneId and (ir.validityStartTime is null or  ir.validityStartTime < CURRENT_TIMESTAMP )" +
             " and (ir.validityEndTime is null or ir.validityEndTime > CURRENT_TIMESTAMP)")
     List<IncentiveRulesEntity> findValidRules(@Param("timeSlotIds") List<Long> validTimeSlots,
                                               @Param("incentiveGroup") List<Long> incentiveGroupId,
                                               @Param("computeOn") List<String> computeOn,
                                               @Param("zoneId") long zoneId);


     @Query("select distinct ir from IncentiveRulesEntity ir ,IncentiveGroupRuleMapEntity igrm,RuleZoneMapEntity rzm" +
             " where ir.active=1 and ir.id = igrm.ruleId and ir.id = rzm.ruleId" +
             " and igrm.incentiveGroupId in :incentiveGroup" +
             " and rzm.zoneId = :zoneId" +
             " and ir.computeOn = 'logout'and (ir.validityStartTime is null or  ir.validityStartTime < CURRENT_TIMESTAMP )" +
             " and (ir.validityEndTime is null or ir.validityEndTime > CURRENT_TIMESTAMP)")
     List<IncentiveRulesEntity> findRulesForLogout(@Param("incentiveGroup")List<Long> incentiveGroupIds,@Param("zoneId") long zoneId);


     @Query("select distinct ir from IncentiveRulesEntity ir ,IncentiveGroupRuleMapEntity igrm,RuleZoneMapEntity rzm" +
             " where ir.active=1 and ir.id = igrm.ruleId and ir.id = rzm.ruleId" +
             " and igrm.incentiveGroupId in :incentiveGroup" +
             " and rzm.zoneId = :zoneId" +
             " and ir.computeOn = :computeOn and (ir.validityStartTime is null or  ir.validityStartTime < CURRENT_TIMESTAMP )" +
             " and (ir.validityEndTime is null or ir.validityEndTime > CURRENT_TIMESTAMP)")
     List<IncentiveRulesEntity> findRulesForEndOfTime(@Param("incentiveGroup") List<Long> incentiveGroupId,@Param("zoneId") long zoneId,@Param("computeOn") String computeOn);

}
