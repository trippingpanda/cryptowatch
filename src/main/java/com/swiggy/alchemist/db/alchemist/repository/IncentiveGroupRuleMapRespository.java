package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.IncentiveGroupRuleMapEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by RAVISINGH on 04/10/16.
 */
public interface IncentiveGroupRuleMapRespository extends CrudRepository<IncentiveGroupRuleMapEntity,Long> {


    @Modifying
    @Query( "delete from IncentiveGroupRuleMapEntity where ruleId = :ruleId" )
    void deleteByRuleId(@Param("ruleId") Long ruleId);
}

