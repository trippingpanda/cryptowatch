package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.SurgeEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by RAVISINGH on 24/10/16.
 */
public interface SurgeRepository extends CrudRepository<SurgeEntity,Long> {

    @Query("select multiplier from SurgeEntity where zoneId = :zoneId and startTime <= CURRENT_TIMESTAMP and " +
            "endTime >= CURRENT_TIMESTAMP and archived = 0")
    List<Float> getMultiplierForZone(@Param("zoneId") Long zoneId);

    @Query("select multiplier from SurgeEntity where zoneId = :zoneId and ((:startTime >= startTime and :startTime <= endTime) or " +
            "(:endTime >= startTime and :endTime <= endTime)) and archived = 0")
    List<Float> getMultiplierForTime(@Param("zoneId") Long zoneId, @Param("startTime") Timestamp startTime,
                                     @Param("endTime") Timestamp endTime);

    @Query("update SurgeEntity set archived = 1 where id = :id and archived = 0 ")
    @Modifying
    void markSurgeArchived(@Param("id") Long id);

}
