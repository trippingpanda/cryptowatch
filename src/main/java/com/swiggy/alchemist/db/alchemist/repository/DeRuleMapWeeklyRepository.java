package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.aspects.Redis;
import com.swiggy.alchemist.aspects.RedisKey;
import com.swiggy.alchemist.db.alchemist.entities.DeRuleMapWeeklyEntity;
import com.swiggy.alchemist.services.caching.Cache;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;

public interface DeRuleMapWeeklyRepository extends CrudRepository<DeRuleMapWeeklyEntity,Long> {
    @Modifying
    @Query("update DeRuleMapWeeklyEntity set archivedDate = :archivedDate , archived = 1 where (archived is null or archived = 0)")
    void archivePayments(@Param("archivedDate") Timestamp archivedDate);

    @Query("select drmw from DeRuleMapWeeklyEntity drmw " +
            "where drmw.ruleId = :ruleId " +
            "and drmw.deId = :deId " +
            "and drmw.createdOn = :createdOn ")
    DeRuleMapWeeklyEntity findByRuleIdDeIdAndCreatedDate(@Param("ruleId") long ruleId, @Param("deId") Long deId, @Param("createdOn") String weekForWeeklyPerformanceData);

    @Query("select count(*) from DeRuleMapWeeklyEntity drmw , IncentiveRulesEntity ir" +
            " where drmw.ruleId = ir.id " +
            " and ir.ruleName = :ruleName" +
            " and drmw.deId = :deId" +
            " and drmw.createdOn like %:monthString%")
    int getComputeCountInMonth(@Param("ruleName") String ruleName, @Param("monthString") String monthString, @Param("deId") long deId);

    @Query("select drm.deId, drm.archivedDate, ir.ruleName, drm.bonus, drm.count, ir.scope from DeRuleMapWeeklyEntity " +
            " as drm , IncentiveRulesEntity as ir where ir.id = drm.ruleId and drm.archivedDate < :endTime" +
            " and drm.archivedDate > :startTime and drm.deId in :deIds")
    List<Object[]> getWeeklyMapForDes(@Param("deIds") List<Long> deIds, @Param("startTime") Timestamp startTime,
                                     @Param("endTime") Timestamp endTime);

    @Query("select drm.deId, drm.archivedDate, ir.ruleName, drm.bonus, drm.count, ir.scope from DeRuleMapWeeklyEntity " +
            " as drm , IncentiveRulesEntity as ir where ir.id = drm.ruleId and drm.archivedDate is null " +
            "and drm.deId in :deIds")
    List<Object[]> getWeeklyMapForDesToday(@Param("deIds") List<Long> deIds);

    @Query("select sum(drm.bonus) from " +
            "DeRuleMapWeeklyEntity as drm where drm.archivedDate < :endTime and drm.archivedDate > :startTime" +
            " and drm.deId = :deId ")
    Long bonusSum(@Param("deId") Long deId,@Param("startTime") Timestamp startTime,
                   @Param("endTime") Timestamp endTime);

    @Query("select sum(drm.bonus) from " +
            "DeRuleMapWeeklyEntity as drm where drm.archivedDate is null" +
            " and drm.deId = :deId ")
    Long bonusSum(@Param("deId") Long deId);
}
