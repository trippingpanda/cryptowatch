package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.DePerformanceDailyEntity;
import com.swiggy.alchemist.db.alchemist.entities.DePerformanceDailyEntityPK;
import org.springframework.data.repository.CrudRepository;

public interface DePerformanceDailyRepository extends CrudRepository<DePerformanceDailyEntity,DePerformanceDailyEntityPK> {
}
