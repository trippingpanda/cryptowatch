package com.swiggy.alchemist.db.alchemist.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class DePerformanceMonthlyEntityPK implements Serializable {
    private long deId;
    private String month;

    @Column(name = "de_id")
    @Id
    public long getDeId() {
        return deId;
    }

    public void setDeId(long deId) {
        this.deId = deId;
    }

    @Column(name = "month")
    @Id
    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DePerformanceMonthlyEntityPK that = (DePerformanceMonthlyEntityPK) o;

        if (deId != that.deId) return false;
        if (month != null ? !month.equals(that.month) : that.month != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (deId ^ (deId >>> 32));
        result = 31 * result + (month != null ? month.hashCode() : 0);
        return result;
    }
}
