package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.DePerformanceWeeklyEntity;
import com.swiggy.alchemist.db.alchemist.entities.DePerformanceWeeklyEntityPK;
import org.springframework.data.repository.CrudRepository;

public interface DePerformanceWeeklyRepository extends CrudRepository<DePerformanceWeeklyEntity,DePerformanceWeeklyEntityPK> {
}
