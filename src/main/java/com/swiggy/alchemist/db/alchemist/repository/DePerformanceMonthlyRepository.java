package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.DePerformanceMonthlyEntity;
import com.swiggy.alchemist.db.alchemist.entities.DePerformanceMonthlyEntityPK;
import org.springframework.data.repository.CrudRepository;

public interface DePerformanceMonthlyRepository extends CrudRepository<DePerformanceMonthlyEntity,DePerformanceMonthlyEntityPK> {
}
