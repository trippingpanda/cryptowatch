package com.swiggy.alchemist.db.alchemist.entities;

import javax.persistence.*;

import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "de_performance_monthly")
@IdClass(DePerformanceMonthlyEntityPK.class)
public class DePerformanceMonthlyEntity {
    private long id;
    private long deId;
    private String month;
    private String perfBlob;
    private Timestamp createdAt;
    private Timestamp updatedAt;

    @Basic
    @Column(name = "id")
    @GeneratedValue(strategy = IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Id
    @Column(name = "de_id")
    public long getDeId() {
        return deId;
    }

    public void setDeId(long deId) {
        this.deId = deId;
    }

    @Id
    @Column(name = "month")
    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    @Basic
    @Column(name = "perf_blob")
    public String getPerfBlob() {
        return perfBlob;
    }

    public void setPerfBlob(String perfBlob) {
        this.perfBlob = perfBlob;
    }

    @Column(name = "created_at")
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "updated_at")
    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DePerformanceMonthlyEntity that = (DePerformanceMonthlyEntity) o;

        if (id != that.id) return false;
        if (deId != that.deId) return false;
        if (month != null ? !month.equals(that.month) : that.month != null) return false;
        if (perfBlob != null ? !perfBlob.equals(that.perfBlob) : that.perfBlob != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (deId ^ (deId >>> 32));
        result = 31 * result + (month != null ? month.hashCode() : 0);
        result = 31 * result + (perfBlob != null ? perfBlob.hashCode() : 0);
        return result;
    }
}
