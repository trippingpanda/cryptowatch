package com.swiggy.alchemist.db.alchemist.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class DePerformanceWeeklyEntityPK implements Serializable {
    private long deId;
    private String week;

    @Column(name = "de_id")
    @Id
    public long getDeId() {
        return deId;
    }

    public void setDeId(long deId) {
        this.deId = deId;
    }

    @Column(name = "week")
    @Id
    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DePerformanceWeeklyEntityPK that = (DePerformanceWeeklyEntityPK) o;

        if (deId != that.deId) return false;
        if (week != null ? !week.equals(that.week) : that.week != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (deId ^ (deId >>> 32));
        result = 31 * result + (week != null ? week.hashCode() : 0);
        return result;
    }
}
