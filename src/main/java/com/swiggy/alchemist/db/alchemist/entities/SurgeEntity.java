package com.swiggy.alchemist.db.alchemist.entities;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Created by RAVISINGH on 24/10/16.
 */
@Entity
@Table(name = "surge")
@Data
public class SurgeEntity {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false) private Long id;

    @Column(name = "start_time") private Timestamp startTime;

    @Column(name="end_time") private Timestamp endTime;

    @Column(name="multiplier") private Float multiplier;

    @Column(name="zone_id") private Long zoneId;

    @Column(name="archived") private Boolean archived;
}
