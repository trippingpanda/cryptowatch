package com.swiggy.alchemist.db.alchemist.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class DePerformanceDailyEntityPK implements Serializable {
    private long deId;
    private String date;

    @Column(name = "de_id")
    @Id
    public long getDeId() {
        return deId;
    }

    public void setDeId(long deId) {
        this.deId = deId;
    }

    @Column(name = "date")
    @Id
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DePerformanceDailyEntityPK that = (DePerformanceDailyEntityPK) o;

        if (deId != that.deId) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (deId ^ (deId >>> 32));
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
