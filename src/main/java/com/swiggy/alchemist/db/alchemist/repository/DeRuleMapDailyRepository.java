package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.aspects.Redis;
import com.swiggy.alchemist.aspects.RedisKey;
import com.swiggy.alchemist.db.alchemist.entities.DeRuleMapDailyEntity;
import com.swiggy.alchemist.services.caching.Cache;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;

public interface DeRuleMapDailyRepository extends CrudRepository<DeRuleMapDailyEntity,Long> {
    @Modifying
    @Query("update DeRuleMapDailyEntity set archivedDate = :archivedDate , archived = 1 where (archived is null or archived = 0)")
    void archivePayments(@Param("archivedDate") Timestamp archivedDate);

    @Query("select drmd from DeRuleMapDailyEntity drmd " +
            "where drmd.ruleId = :ruleId " +
            "and drmd.deId = :deId " +
            "and drmd.createdOn = :createdOn ")
    DeRuleMapDailyEntity findByRuleIdDeIdAndCreatedDate(@Param("ruleId") long ruleId,
                                                        @Param("deId") Long deId,
                                                        @Param("createdOn") String dateForDailyPerformanceData);

    @Query("select count(*) from DeRuleMapDailyEntity drmw , IncentiveRulesEntity ir" +
            " where drmw.ruleId = ir.id " +
            " and ir.ruleName = :ruleName" +
            " and drmw.deId = :deId" +
            " and drmw.createdOn like %:dateString%")
    int getComputeCountInDuration(@Param("ruleName") String ruleName, @Param("dateString") String dateString, @Param("deId") long deId);

    @Query("select drmw.count from DeRuleMapDailyEntity drmw , IncentiveRulesEntity ir" +
            " where drmw.ruleId = ir.id" +
            " and ir.ruleName = :ruleName" +
            " and drmw.deId = :deId" +
            " and drmw.createdOn like %:dateString%")
    Integer getComputeCountForSameDay(@Param("ruleName") String ruleName, @Param("dateString") String dateString,@Param("deId") long deId);


    @Query("select drm.deId, drm.archivedDate, ir.ruleName, drm.bonus, drm.count, ir.scope, drm.orders from " +
            "DeRuleMapDailyEntity as drm , IncentiveRulesEntity as ir where ir.id = drm.ruleId and " +
            "drm.archivedDate < :endTime and drm.archivedDate > :startTime and drm.deId in :deIds ")
    List<Object[]> getDailyMapForDes(@Param("deIds") List<Long> deIds, @Param("startTime") Timestamp startTime,
                                   @Param("endTime") Timestamp endTime);

    @Query("select drm.deId, drm.archivedDate, ir.ruleName, drm.bonus, drm.count, ir.scope, drm.orders from " +
            "DeRuleMapDailyEntity as drm , IncentiveRulesEntity as ir where ir.id = drm.ruleId and drm.archivedDate is null" +
            " and drm.deId in :deIds ")
    List<Object[]> getDailyMapForDesToday(@Param("deIds") List<Long> deIds);


    @Query("select sum(drm.bonus) from " +
            "DeRuleMapDailyEntity as drm where drm.archivedDate < :endTime and drm.archivedDate > :startTime" +
            " and drm.deId = :deId ")
    Long bonusSum(@Param("deId")Long deId, @Param("startTime") Timestamp startTime,
                   @Param("endTime") Timestamp endTime);

    @Query("select sum(drm.bonus) from " +
            "DeRuleMapDailyEntity as drm where drm.archivedDate is null" +
            " and drm.deId = :deId ")
    Long bonusSum( @Param("deId") Long deId);

}
