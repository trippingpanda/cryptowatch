package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.DePerformanceQuarterEntity;
import com.swiggy.alchemist.db.alchemist.entities.DePerformanceQuarterEntityPK;
import org.springframework.data.repository.CrudRepository;

public interface DePerformanceQuaterlyRepository extends CrudRepository<DePerformanceQuarterEntity,DePerformanceQuarterEntityPK> {
}
