package com.swiggy.alchemist.db.alchemist.entities;

import javax.persistence.*;

/**
 * Created by RAVISINGH on 06/10/16.
 */

@Entity
@Table(name = "rule_zone_mapping")
public class RuleZoneMapEntity {
    private long id;
    private long ruleId;
    private long zoneId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "rule_id")
    public long getRuleId() {
        return ruleId;
    }

    public void setRuleId(long ruleId) {
        this.ruleId = ruleId;
    }

    @Basic
    @Column(name = "zone_id")
    public long getZoneId() {
        return zoneId;
    }

    public void setZoneId(long zoneId) {
        this.zoneId = zoneId;
    }
}
