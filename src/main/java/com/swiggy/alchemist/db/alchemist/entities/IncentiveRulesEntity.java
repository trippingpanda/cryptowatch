package com.swiggy.alchemist.db.alchemist.entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "incentive_rules")
public class IncentiveRulesEntity {
    private long id;
    private String expression;
    private String ruleName;
    private String description;
    private String ruleText;
    private String statusMessage;
    private String statusExpression;
    private boolean active;
    private String bonus;
    private Timestamp validityStartTime;
    private Timestamp validityEndTime;
    private String createdBy;
    private String updatedBy;
    private String computeOn;
    private String scope;
    private Timestamp createdAt;
    private Timestamp updatedAt;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "expression")
    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    @Basic
    @Column(name = "rule_name")
    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "rule_text")
    public String getRuleText() {
        return ruleText;
    }

    public void setRuleText(String ruleText) {
        this.ruleText = ruleText;
    }

    @Basic
    @Column(name = "status_message")
    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Basic
    @Column(name = "status_expression")
    public String getStatusExpression() {
        return statusExpression;
    }

    public void setStatusExpression(String statusExpression) {
        this.statusExpression = statusExpression;
    }

    @Basic
    @Column(name = "active")
    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Basic
    @Column(name = "bonus")
    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    @Basic
    @Column(name = "validity_start_time")
    public Timestamp getValidityStartTime() {
        return validityStartTime;
    }

    public void setValidityStartTime(Timestamp validityStartTime) {
        this.validityStartTime = validityStartTime;
    }

    @Basic
    @Column(name = "validity_end_time")
    public Timestamp getValidityEndTime() {
        return validityEndTime;
    }

    public void setValidityEndTime(Timestamp validityEndTime) {
        this.validityEndTime = validityEndTime;
    }

    @Basic
    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "compute_on")
    public String getComputeOn() {
        return computeOn;
    }

    public void setComputeOn(String computeOn) {
        this.computeOn = computeOn;
    }

    @Basic
    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Basic
    @Column(name = "scope")
    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    @Column(name = "created_at")
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "updated_at")
    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncentiveRulesEntity that = (IncentiveRulesEntity) o;

        if (id != that.id) return false;
        if (active != that.active) return false;
        if (!bonus.equals(that.bonus)) return false;
        if (expression != null ? !expression.equals(that.expression) : that.expression != null) return false;
        if (ruleName != null ? !ruleName.equals(that.ruleName) : that.ruleName != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (ruleText != null ? !ruleText.equals(that.ruleText) : that.ruleText != null) return false;
        if (statusMessage != null ? !statusMessage.equals(that.statusMessage) : that.statusMessage != null)
            return false;
        if (statusExpression != null ? !statusExpression.equals(that.statusExpression) : that.statusExpression != null)
            return false;
        if (validityStartTime != null ? !validityStartTime.equals(that.validityStartTime) : that.validityStartTime != null)
            return false;
        if (validityEndTime != null ? !validityEndTime.equals(that.validityEndTime) : that.validityEndTime != null)
            return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (updatedBy != null ? !updatedBy.equals(that.updatedBy) : that.updatedBy != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (expression != null ? expression.hashCode() : 0);
        result = 31 * result + (ruleName != null ? ruleName.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (ruleText != null ? ruleText.hashCode() : 0);
        result = 31 * result + (statusMessage != null ? statusMessage.hashCode() : 0);
        result = 31 * result + (statusExpression != null ? statusExpression.hashCode() : 0);
        result = 31 * result + (active? 1231:1237);
        result = 31 * result + bonus.hashCode();
        result = 31 * result + (validityStartTime != null ? validityStartTime.hashCode() : 0);
        result = 31 * result + (validityEndTime != null ? validityEndTime.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (updatedBy != null ? updatedBy.hashCode() : 0);
        return result;
    }

}
