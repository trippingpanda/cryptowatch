package com.swiggy.alchemist.db.alchemist.entities;

import javax.persistence.*;

/**
 * Created by RAVISINGH on 29/09/16.
 */
@Entity
@Table(name = "timeslot_rules_mapping")
public class TimeSlotRuleMapEntity {
    private long id;
    private long ruleId;
    private long timeSlotId;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "rule_id")
    public long getRuleId() {
        return ruleId;
    }

    public void setRuleId(long ruleId) {
        this.ruleId = ruleId;
    }

    @Basic
    @Column(name = "timeslot_id")
    public long getTimeSlotId() {
        return timeSlotId;
    }

    public void setTimeSlotId(long timeSlotId) {
        this.timeSlotId = timeSlotId;
    }
}
