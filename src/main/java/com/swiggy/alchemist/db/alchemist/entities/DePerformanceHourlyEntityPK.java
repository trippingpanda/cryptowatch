package com.swiggy.alchemist.db.alchemist.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class DePerformanceHourlyEntityPK implements Serializable {
    private long deId;
    private String hour;

    @Column(name = "de_id")
    @Id
    public long getDeId() {
        return deId;
    }

    public void setDeId(long deId) {
        this.deId = deId;
    }

    @Column(name = "hour")
    @Id
    public String getHour() {
        return hour;
    }

    public void setHour(String date) {
        this.hour = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DePerformanceHourlyEntityPK that = (DePerformanceHourlyEntityPK) o;

        if (deId != that.deId) return false;
        if (hour != null ? !hour.equals(that.hour) : that.hour != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (deId ^ (deId >>> 32));
        result = 31 * result + (hour != null ? hour.hashCode() : 0);
        return result;
    }
}
