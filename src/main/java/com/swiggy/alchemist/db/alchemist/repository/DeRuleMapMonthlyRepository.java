package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.aspects.Redis;
import com.swiggy.alchemist.aspects.RedisKey;
import com.swiggy.alchemist.db.alchemist.entities.DeRuleMapMonthlyEntity;
import com.swiggy.alchemist.services.caching.Cache;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;

public interface DeRuleMapMonthlyRepository extends CrudRepository<DeRuleMapMonthlyEntity,Long> {
    @Modifying
    @Query("update DeRuleMapMonthlyEntity set archivedDate = :archivedDate , archived = 1 where (archived is null or archived = 0)")
    void archivePayments(@Param("archivedDate") Timestamp archivedDate);

    @Query("select drm from DeRuleMapMonthlyEntity drm " +
            "where drm.ruleId = :ruleId " +
            "and drm.deId = :deId " +
            "and drm.createdOn = :createdOn ")
    DeRuleMapMonthlyEntity findByRuleIdDeIdAndCreatedDate(@Param("ruleId") long ruleId, @Param("deId") Long deId, @Param("createdOn") String monthForMonthlyPerformanceData);

    @Query("select drm.deId, drm.archivedDate, ir.ruleName, drm.bonus, drm.count, ir.scope from DeRuleMapMonthlyEntity " +
            " as drm , IncentiveRulesEntity as ir where ir.id = drm.ruleId and drm.archivedDate < :endTime" +
            " and drm.archivedDate > :startTime and drm.deId in :deIds")
    List<Object[]> getMonthlyMapForDes(@Param("deIds") List<Long> deIds, @Param("startTime") Timestamp startTime,
                                     @Param("endTime") Timestamp endTime);

    @Query("select drm.deId, drm.archivedDate, ir.ruleName, drm.bonus, drm.count, ir.scope from DeRuleMapMonthlyEntity " +
            " as drm , IncentiveRulesEntity as ir where ir.id = drm.ruleId and drm.archivedDate is null" +
            " and drm.deId in :deIds")
    List<Object[]> getMonthlyMapForDesToday(@Param("deIds") List<Long> deIds);


    @Query("select sum(drm.bonus) from " +
            "DeRuleMapMonthlyEntity as drm where drm.archivedDate < :endTime and drm.archivedDate > :startTime" +
            " and drm.deId = :deId")
    Long bonusSum(@Param("deId") Long deId, @Param("startTime") Timestamp startTime,
                   @Param("endTime") Timestamp endTime);

    @Query("select sum(drm.bonus) from " +
            "DeRuleMapMonthlyEntity as drm where drm.archivedDate is null" +
            " and drm.deId = :deId ")
    Long bonusSum( @Param("deId") Long deId);
}
