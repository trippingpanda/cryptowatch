package com.swiggy.alchemist.db.alchemist.entities;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "de_rule_map_monthly")
public class DeRuleMapMonthlyEntity {
    private long id;
    private long deId;
    private long ruleId;
    private Byte completed;
    private Byte archived;
    private Timestamp archivedDate;
    private String createdOn;
    private Integer count;
    private Double bonus;
    private Integer statusValue;
    private Timestamp createdAt;
    private Timestamp updatedAt;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = IDENTITY)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "de_id")
    public long getDeId() {
        return deId;
    }

    public void setDeId(long deId) {
        this.deId = deId;
    }

    @Basic
    @Column(name = "rule_id")
    public long getRuleId() {
        return ruleId;
    }

    public void setRuleId(long ruleId) {
        this.ruleId = ruleId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "completed")
    public Byte getCompleted() {
        return completed;
    }

    public void setCompleted(Byte completed) {
        this.completed = completed;
    }

    @Basic
    @Column(name = "archived")
    public Byte getArchived() {
        return archived;
    }

    public void setArchived(Byte archived) {
        this.archived = archived;
    }

    @Basic
    @Column(name = "archived_date")
    public Timestamp getArchivedDate() {
        return archivedDate;
    }

    public void setArchivedDate(Timestamp archivedDate) {
        this.archivedDate = archivedDate;
    }

    @Basic
    @Column(name = "count")
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Basic
    @Column(name = "bonus")
    public Double getBonus() {
        return bonus;
    }

    public void setBonus(Double bonus) {
        this.bonus = bonus;
    }

    @Basic
    @Column(name = "status_value")
    public Integer getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(Integer statusValue) {
        this.statusValue = statusValue;
    }

    @Column(name = "created_at")
    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Column(name = "updated_at")
    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeRuleMapMonthlyEntity that = (DeRuleMapMonthlyEntity) o;

        if (deId != that.deId) return false;
        if (ruleId != that.ruleId) return false;
        if (completed != null ? !completed.equals(that.completed) : that.completed != null) return false;
        if (archived != null ? !archived.equals(that.archived) : that.archived != null) return false;
        if (archivedDate != null ? !archivedDate.equals(that.archivedDate) : that.archivedDate != null) return false;
        if (count != null ? !count.equals(that.count) : that.count != null) return false;
        if (bonus != null ? !bonus.equals(that.bonus) : that.bonus != null) return false;
        if (statusValue != null ? !statusValue.equals(that.statusValue) : that.statusValue != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (deId ^ (deId >>> 32));
        result = 31 * result + (int) (ruleId ^ (ruleId >>> 32));
        result = 31 * result + (completed != null ? completed.hashCode() : 0);
        result = 31 * result + (archived != null ? archived.hashCode() : 0);
        result = 31 * result + (archivedDate != null ? archivedDate.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        result = 31 * result + (bonus != null ? bonus.hashCode() : 0);
        result = 31 * result + (statusValue != null ? statusValue.hashCode() : 0);
        return result;
    }
}
