package com.swiggy.alchemist.db.alchemist.repository;

import com.swiggy.alchemist.db.alchemist.entities.TimeSlotRuleMapEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by RAVISINGH on 29/09/16.
 */

public interface TimeSlotRuleMapRespository extends CrudRepository<TimeSlotRuleMapEntity,Long> {

    @Modifying
    @Query( "delete from TimeSlotRuleMapEntity where ruleId = :ruleId" )
    void deleteByRuleId(@Param("ruleId") Long ruleId);

    @Query("select trm.timeSlotId from TimeSlotRuleMapEntity trm where trm.ruleId = :ruleId")
    List<Long> findTimeSlotIdByRuleId(@Param("ruleId") long ruleId);
}
