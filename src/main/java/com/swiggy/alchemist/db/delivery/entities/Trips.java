package com.swiggy.alchemist.db.delivery.entities;


import lombok.Data;

import javax.persistence.*;

import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "trips")
@Data
public class Trips implements java.io.Serializable {
    @Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false) private Long id;

    @Column(name = "order_id") private String orderId;

    @Column(name="de_id") private Long deId;

    @Column(name="assigned_time") private Timestamp assignedTime;
}
