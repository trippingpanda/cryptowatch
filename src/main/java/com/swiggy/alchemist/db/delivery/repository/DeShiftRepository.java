package com.swiggy.alchemist.db.delivery.repository;

import com.swiggy.alchemist.db.alchemist.entities.IncentiveGroupEntity;
import com.swiggy.alchemist.db.delivery.entities.DeShiftEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by RAVISINGH on 18/10/16.
 */
public interface DeShiftRepository extends CrudRepository<DeShiftEntity,Long> {


    @Query( "select id from DeShiftEntity where name in :names" )
    List<Long> findByNames(@Param("names") List<String> names);

}
