package com.swiggy.alchemist.db.delivery.repository;

import com.swiggy.alchemist.db.delivery.entities.CityEntity;
import com.swiggy.alchemist.db.delivery.entities.ZoneEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by RAVISINGH on 17/11/16.
 */
public interface CityRepository extends CrudRepository<CityEntity,Long> {
    ZoneEntity findById(long id);

    @Query( "select id from CityEntity where name in :names" )
    List<Long> findByNames(@Param("names") List<String> names);

}
