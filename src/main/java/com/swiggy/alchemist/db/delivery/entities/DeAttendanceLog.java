package com.swiggy.alchemist.db.delivery.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "de_attendance_log")
public class DeAttendanceLog  {
    @Id @GeneratedValue(strategy = IDENTITY)
    @Getter @Setter @Column(name="id", nullable = false) private long id;
    @Getter @Setter @Column(name="event", nullable = true) private String event;
    @Getter @Setter @Column(name="time", nullable = true) private Date time;
    @Getter @Setter @Column(name="lat_long", nullable = true) private String latLong;
    @Getter @Setter @Column(name="de_id", nullable = true) private Long deId;

}
