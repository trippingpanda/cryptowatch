package com.swiggy.alchemist.db.delivery.repository;

import com.swiggy.alchemist.db.delivery.entities.ZoneEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by RAVISINGH on 05/10/16.
 */
public interface ZoneRespository extends CrudRepository<ZoneEntity,Long> {
    ZoneEntity findById(long id);

    @Query( "select id from ZoneEntity where name in :names" )
    List<Long> findByNames(@Param("names") List<String> names);

    @Query( "select id from ZoneEntity where cityId = :cityId and enabled = 1" )
    List<Long> findByCityId(@Param("cityId") Long cityId);

    @Query( "select rainMode from ZoneEntity where id = :zoneId" )
    Boolean getRainMode(@Param("zoneId") Long zoneId);

}

