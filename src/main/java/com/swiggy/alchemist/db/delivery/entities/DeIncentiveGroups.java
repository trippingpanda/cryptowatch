package com.swiggy.alchemist.db.delivery.entities;


import lombok.Data;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@Entity
@Table(name = "de_shifts")
public class DeIncentiveGroups {
	
	@Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name="id", nullable = false)
    private long id;

    @Column(name="de_id")
    private Long deId;

    @Column(name="shift_id")
    private Long incentiveGroupId;

}
