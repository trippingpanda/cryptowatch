package com.swiggy.alchemist.db.delivery.repository;

import com.swiggy.alchemist.db.delivery.entities.DeIncentiveGroups;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DeIncentiveGroupRepository extends CrudRepository<DeIncentiveGroups,Long> {

    @Query("select incentiveGroupId from DeIncentiveGroups where deId = :deId")
    List<Long> findIncentiveGroupIdByDeId(@Param("deId") long deId);
}
