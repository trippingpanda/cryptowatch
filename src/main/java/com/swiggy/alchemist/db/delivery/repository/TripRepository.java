package com.swiggy.alchemist.db.delivery.repository;

import com.swiggy.alchemist.db.delivery.entities.Trips;
import org.springframework.data.repository.CrudRepository;

public interface TripRepository extends CrudRepository<Trips,Long>{
    Trips findByOrderId(String orderId);
}
