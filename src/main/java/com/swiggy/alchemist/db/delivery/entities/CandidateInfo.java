package com.swiggy.alchemist.db.delivery.entities;


import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;


@Data
@Entity
@Table (name="candidate_info")
public class CandidateInfo {

	
	@Id @GeneratedValue (strategy = IDENTITY)
    @Column(name="candidate_id", nullable=false) private Long candidate_id;

    @Column(name="de_id", nullable=true) private Long deId;

    @Column(name="record_timestamp", nullable = true) private Timestamp record_timestamp;



}
