package com.swiggy.alchemist.db.delivery.entities;

import javax.persistence.*;

/**
 * Created by RAVISINGH on 05/10/16.
 */
@Entity
@Table(name = "zone")
public class ZoneEntity {
    private long id;
    private long cityId;
    private String name;
    private Boolean rainMode;
    private String orderWeights;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "city_id")
    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "order_weights")
    public String getOrderWeights() {
        return orderWeights;
    }

    public void setOrderWeights(String orderWeights) {
        this.orderWeights = orderWeights;
    }

    @Basic
    @Column(name = "rain_mode")
    public Boolean getRainMode() {
        return rainMode;
    }

    public void setRainMode(boolean rainMode) {
        this.rainMode = rainMode;
    }
}

