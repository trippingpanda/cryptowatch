package com.swiggy.alchemist.db.delivery.repository;

import com.swiggy.alchemist.db.delivery.entities.DeliveryBoys;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DeliveryBoysRepository extends CrudRepository<DeliveryBoys,Long> {
    @Query("select zoneId from DeliveryBoys where id = :id")
    long findZoneIdById(@Param("id") long id);

    @Query( "select id from DeliveryBoys where zoneId in :zones" )
    List<Long> getDeListByZones(@Param("zones") List<Long> zones);

    @Query( "select id from DeliveryBoys where zoneId = :zone" )
    List<Long> getDeListByZone(@Param("zone") Long zone);

    @Query("select id from DeliveryBoys where employmentStatus = 1")
    List<Long> findValidDes();

}
