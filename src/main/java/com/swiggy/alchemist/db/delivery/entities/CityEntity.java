package com.swiggy.alchemist.db.delivery.entities;

import javax.persistence.*;

/**
 * Created by RAVISINGH on 17/11/16.
 */
@Entity
@Table(name = "city")
public class CityEntity {
    private long id;
    private String name;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

