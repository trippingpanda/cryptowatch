package com.swiggy.alchemist.db.delivery.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "delivery_boys")
@Data
public class DeliveryBoys{

    @Id @GeneratedValue(strategy = IDENTITY)
    @Column(name="id", nullable = false)
    private long id;
    @Column(name="name", nullable = false)
    private String name;
    @Column(name="enabled", nullable = false)
    private int enabled = 0;
    @Column(name="status")
    private String status = "FR";
    @Column(name="order_id")
    private String orderId;
    @Column(name="zone_id")
    private long zoneId;
    @Column(name="batch_id")
    private Long batchId;
    @Column(name = "employment_status")
    private Byte employmentStatus;

}
