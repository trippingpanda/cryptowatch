package com.swiggy.alchemist.db.delivery.entities;

import javax.persistence.*;

/**
 * Created by RAVISINGH on 18/10/16.
 */

@Entity
@Table(name = "de_shift_timings")
public class DeShiftEntity {
    private long id;
    private long cityId;
    private String name;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "city_id")
    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    @Basic
    @Column(name = "nickname")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
