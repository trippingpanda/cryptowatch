package com.swiggy.alchemist.db.delivery.repository;

import com.swiggy.alchemist.db.delivery.entities.DeAttendanceLog;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.sql.Timestamp;
import java.util.List;

public interface DeAttendanceLogRepository extends CrudRepository<DeAttendanceLog,Long> {



    @Query("select i from DeAttendanceLog i where time >= :startTime and time <= :endTime and deId = :deId")
    List<DeAttendanceLog> getDeAttendanceLogs(@Param("deId")Long deId,@Param("startTime")Timestamp startTime,
                                              @Param("endTime")Timestamp endTime);

    @Query("select i from DeAttendanceLog i where time <= :startTime and deId = :deId order by id desc")
    List<DeAttendanceLog> getLastDeAttendanceLogs(@Param("deId")Long deId,@Param("startTime")Timestamp startTime, Pageable pageable);


}
