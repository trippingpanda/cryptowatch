package com.swiggy.alchemist.db.delivery.repository;


import com.swiggy.alchemist.db.delivery.entities.CandidateInfo;
import org.springframework.data.repository.CrudRepository;

public interface CandidateInfoRepository extends CrudRepository<CandidateInfo,Long>{
    CandidateInfo findByDeId(long deId);
}
