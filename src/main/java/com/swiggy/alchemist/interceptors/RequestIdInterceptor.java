package com.swiggy.alchemist.interceptors;

import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Component
public class RequestIdInterceptor extends HandlerInterceptorAdapter {
	
	public static final String ATTRIBUTE_NAME = "com.swiggy.request-id";
	public static final String CONTEXT_NAME = "requestId";
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
		throws Exception {
		String id = UUID.randomUUID().toString();
		MDC.put(CONTEXT_NAME, id);
		RequestContextHolder.getRequestAttributes()
			.setAttribute(ATTRIBUTE_NAME, id, RequestAttributes.SCOPE_REQUEST);
		return true;
	}
	
	@Override
	public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		MDC.clear();
	}

}
