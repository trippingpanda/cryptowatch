package com.swiggy.alchemist.interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class RequestTimeInterceptor extends HandlerInterceptorAdapter {
	
	private static final String ATTRIBUTE_NAME = "com.swiggy.start-time";
	
	private static final Logger logger = LoggerFactory.getLogger(RequestTimeInterceptor.class);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
		throws Exception {
		request.setAttribute(ATTRIBUTE_NAME, System.currentTimeMillis());
		return true;
	}
	
	@Override
	public void afterCompletion(
            HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		long endTime = System.currentTimeMillis();
		long startTime = (Long) request.getAttribute(ATTRIBUTE_NAME);
		long processingTime = endTime - startTime;
		String requestId = (String) RequestContextHolder.
				getRequestAttributes().getAttribute(
						RequestIdInterceptor.ATTRIBUTE_NAME, 
						RequestAttributes.SCOPE_REQUEST);
		logger.info("Request completed. RequestId: " + requestId + " processingTime: " + processingTime + "ms");
	}
}
