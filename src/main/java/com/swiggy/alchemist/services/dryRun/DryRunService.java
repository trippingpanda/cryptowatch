package com.swiggy.alchemist.services.dryRun;

import com.swiggy.alchemist.pojo.EventPojo;
import com.swiggy.alchemist.pojo.IncentiveOrderPojo;

public interface DryRunService {
    void processDryRun(EventPojo eventPojo, IncentiveOrderPojo incentiveOrderPojo);
}
