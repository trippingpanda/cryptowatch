package com.swiggy.alchemist.services.dryRun;


import com.swiggy.alchemist.enums.EventNames;
import com.swiggy.alchemist.pojo.*;
import com.swiggy.alchemist.queues.publish.RabbitmqPublish;
import com.swiggy.alchemist.services.evaluator.IRuleService;
import com.swiggy.alchemist.utility.SwiggyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DryRunServiceImpl implements DryRunService {

    @Autowired
    private IRuleService ruleService;

    @Autowired
    private RabbitmqPublish rabbitmqPublish;

    @Override
    public void processDryRun(EventPojo eventPojo, IncentiveOrderPojo incentiveOrderPojo) {
        if (EventNames.get(eventPojo.getEventName()).equals(EventNames.ORDER_STATUS_UPDATE)) {
            StatusUpdatePojo statusUpdatePojo = (StatusUpdatePojo) eventPojo.getEventData();
            if(statusUpdatePojo.getStatus().equals("assigned")) {
                dryRunOnAssigned(eventPojo,incentiveOrderPojo);
            }
        }
    }

    private void dryRunOnAssigned(EventPojo eventPojo, IncentiveOrderPojo incentiveOrderPojo){
        List<DeAppRulePojo> result = new ArrayList<>();
        List<String> bonuses = new ArrayList<>();
        Double total= 0D;
        EventPojo rejectEventPojo =  eventPojo.clone();
        StatusUpdatePojo statusUpdatePojo = (StatusUpdatePojo) eventPojo.getEventData();
        EventPojo deliveredEventPojo =  eventPojo.clone();
        rejectEventPojo.setEventName(EventNames.ORDER_REJECT.getName());
        ((StatusUpdatePojo)deliveredEventPojo.getEventData()).setStatus("delivered");
        List<DeAppRulePojo> deAppRulePojos = ruleService.dryRunRules(rejectEventPojo,incentiveOrderPojo);
        if(deAppRulePojos != null) {
            result.addAll(deAppRulePojos);
        }
        deAppRulePojos = ruleService.dryRunRules(deliveredEventPojo,incentiveOrderPojo);
        if(deAppRulePojos != null) {
            result.addAll(deAppRulePojos);
        }

        for(DeAppRulePojo deAppRulePojo: result) {
            Double bonus = Double.parseDouble(deAppRulePojo.getBonus());
            if (bonus > 0) {
                total+= bonus;
            }
            bonuses.add(SwiggyUtils.convertRulePojoToString(deAppRulePojo));
        }
        rabbitmqPublish.publishDryRunPojo(new DryRunPojo(statusUpdatePojo.getOrderId(),bonuses,total,statusUpdatePojo.getDeliveryBoyId()));
    }
}
