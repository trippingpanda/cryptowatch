package com.swiggy.alchemist.services.aggregation;

import com.swiggy.alchemist.pojo.AggregationPojo;
import com.swiggy.alchemist.pojo.IncentiveOrderPojo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AggregationHelper {

    double getAvgArrivalDelay(AggregationPojo aggregationPojo, IncentiveOrderPojo incentiveOrderPojo) {
        double tripsCount = aggregationPojo.getTrips_count();
        double totalArrivalDelay = aggregationPojo.getAvgArrivalDelay() * tripsCount;
        double arrivalDelay = (double) (incentiveOrderPojo.getArrivedTime() - incentiveOrderPojo.getConfirmationTime())/(1000*60);
        String format = String.format("%.1f", (totalArrivalDelay + arrivalDelay)/(tripsCount+1));
        return Double.parseDouble(format);
    }

    double getAvgConfirmationDelay(AggregationPojo aggregationPojo, IncentiveOrderPojo incentiveOrderPojo) {
        double tripsCount = aggregationPojo.getTrips_count();
        double totalConfirmationDelay = aggregationPojo.getAvgConfirmationDelay() * tripsCount;
        double confirmationDelay = (double) (incentiveOrderPojo.getConfirmationTime() - incentiveOrderPojo.getAssignedTime())/(1000*60);
        String format = String.format("%.1f", (totalConfirmationDelay + confirmationDelay)/(tripsCount+1));
        return Double.parseDouble(format);
    }

    double getAvgPickupDelay(AggregationPojo aggregationPojo, IncentiveOrderPojo incentiveOrderPojo) {
        double tripsCount = aggregationPojo.getTrips_count();
        double totalPickupDelay = aggregationPojo.getAvgPickupDelay() * tripsCount;
        double pickupDelay = (double) (incentiveOrderPojo.getPickedUpTime() - incentiveOrderPojo.getArrivedTime())/(1000*60);
        String format = String.format("%.1f", (totalPickupDelay + pickupDelay)/(tripsCount+1));
        return Double.parseDouble(format);
    }

    double getAvgDeliveryDelay(AggregationPojo aggregationPojo, IncentiveOrderPojo incentiveOrderPojo) {
        double tripsCount = aggregationPojo.getTrips_count();
        double totalDeliveryDelay = aggregationPojo.getAvgDeliveredDelay() * tripsCount;
        double deliveryDelay = (double) (incentiveOrderPojo.getDeliveredTime() - incentiveOrderPojo.getReachedTime())/(1000*60);
        String format = String.format("%.1f", (totalDeliveryDelay + deliveryDelay)/(tripsCount+1));
        return Double.parseDouble(format);
    }

    double getAvgReachedDelay(AggregationPojo aggregationPojo, IncentiveOrderPojo incentiveOrderPojo) {
        double tripsCount = aggregationPojo.getTrips_count();
        double totalReachedDelay = aggregationPojo.getAvgReachedDelay() * tripsCount;
        double reachedDelay = (double) (incentiveOrderPojo.getReachedTime() - incentiveOrderPojo.getPickedUpTime())/(1000*60);
        String format = String.format("%.1f", (totalReachedDelay + reachedDelay)/(tripsCount+1));
        return Double.parseDouble(format);
    }

    double getAvgFirstMile(AggregationPojo aggregationPojo, IncentiveOrderPojo incentiveOrderPojo) {
        double tripsCount = aggregationPojo.getTrips_count();
        double totalFirstMile = aggregationPojo.getAvgFirstMile() * tripsCount;
        String format = String.format("%.1f", (totalFirstMile + incentiveOrderPojo.getFirstMile())/(tripsCount+1));
        return Double.parseDouble(format);
    }

    double getAvgLastMile(AggregationPojo aggregationPojo, IncentiveOrderPojo incentiveOrderPojo) {
        double tripsCount = aggregationPojo.getTrips_count();
        double totalLastMile = aggregationPojo.getAvgLastMile() * tripsCount;
        String format = String.format("%.1f", (totalLastMile + incentiveOrderPojo.getLastMile())/(tripsCount+1));
        return Double.parseDouble(format);
    }

    double getAvgSla(AggregationPojo aggregationPojo, IncentiveOrderPojo incentiveOrderPojo) {
        double tripsCount = aggregationPojo.getTrips_count();
        double totalSla = aggregationPojo.getAvgSla() * tripsCount;
        String format = String.format("%.1f", (totalSla + incentiveOrderPojo.getSla()) / (tripsCount + 1));
        return Double.parseDouble(format);
    }

    double getAvgRating(AggregationPojo aggregationPojo, IncentiveOrderPojo incentiveOrderPojo) {
        long ratingsCount = aggregationPojo.getTotalRatings();
        double totalRatings = aggregationPojo.getAvgRating() * ratingsCount;
        String format = String.format("%.1f", (totalRatings + incentiveOrderPojo.getRating())/(ratingsCount + 1));
        return Double.parseDouble(format);
    }

    public double getAvgSla(List<AggregationPojo> aggregationPojoList) {
        double totalSla = 0;
        double totalTrips = 0;
        for(AggregationPojo pojo : aggregationPojoList){
            totalSla += pojo.getAvgSla()*pojo.getTrips_count();
            totalTrips += pojo.getTrips_count();
        }
        return totalSla/totalTrips;
    }

    public double getAvgReachedDelay(List<AggregationPojo> aggregationPojoList) {
        double reachedDelay = 0;
        double totalTrips = 0;
        for(AggregationPojo pojo : aggregationPojoList){
            reachedDelay += pojo.getAvgReachedDelay()*pojo.getTrips_count();
            totalTrips += pojo.getTrips_count();
        }
        return reachedDelay/totalTrips;
    }

    public double getAvgPickupDelay(List<AggregationPojo> aggregationPojoList) {
        double pickupDelay = 0;
        double totalTrips = 0;
        for(AggregationPojo pojo : aggregationPojoList){
            pickupDelay += pojo.getAvgPickupDelay()*pojo.getTrips_count();
            totalTrips += pojo.getTrips_count();
        }
        return pickupDelay/totalTrips;
    }

    public double getAvgRating(List<AggregationPojo> aggregationPojoList) {
        double rating = 0;
        double totalRatings = 0;
        for(AggregationPojo pojo : aggregationPojoList){
            rating += pojo.getAvgRating()*pojo.getTotalRatings();
            totalRatings += pojo.getTotalRatings();
        }
        return rating/totalRatings;
    }

    public double getAvglastMile(List<AggregationPojo> aggregationPojoList) {
        double lastmile = 0;
        double totalTrips = 0;
        for(AggregationPojo pojo : aggregationPojoList){
            lastmile += pojo.getAvgLastMile()*pojo.getTrips_count();
            totalTrips += pojo.getTrips_count();
        }
        return lastmile/totalTrips;
    }

    public double getAvgFirstMile(List<AggregationPojo> aggregationPojoList) {
        double firstMile = 0;
        double totalTrips = 0;
        for(AggregationPojo pojo : aggregationPojoList){
            firstMile += pojo.getAvgFirstMile()*pojo.getTrips_count();
            totalTrips += pojo.getTrips_count();
        }
        return firstMile/totalTrips;
    }

    public double getAvgDeliveryDelay(List<AggregationPojo> aggregationPojoList) {
        double deliveryDelay = 0;
        double totalTrips = 0;
        for(AggregationPojo pojo : aggregationPojoList){
            deliveryDelay += pojo.getAvgDeliveredDelay()*pojo.getTrips_count();
            totalTrips += pojo.getTrips_count();
        }
        return deliveryDelay/totalTrips;
    }

    public double getAvgConfirmationDelay(List<AggregationPojo> aggregationPojoList) {
        double confirmDelay = 0;
        double totalTrips = 0;
        for(AggregationPojo pojo : aggregationPojoList){
            confirmDelay += pojo.getAvgConfirmationDelay()*pojo.getTrips_count();
            totalTrips += pojo.getTrips_count();
        }
        return confirmDelay/totalTrips;
    }

    public double getAvgArrivalDelay(List<AggregationPojo> aggregationPojoList) {
        double arrivalDelay = 0;
        double totalTrips = 0;
        for(AggregationPojo pojo : aggregationPojoList){
            arrivalDelay += pojo.getAvgArrivalDelay()*pojo.getTrips_count();
            totalTrips += pojo.getTrips_count();
        }
        return arrivalDelay/totalTrips;
    }

    public double getTotalTrips(List<AggregationPojo> aggregationPojoList) {
        double totalTrips = 0;
        for(AggregationPojo pojo : aggregationPojoList){
            totalTrips += pojo.getTrips_count();
        }
        return totalTrips;
    }
}
