package com.swiggy.alchemist.services.aggregation;

import com.swiggy.alchemist.dao.performance.DePerformanceDao;
import com.swiggy.alchemist.db.alchemist.entities.*;
import com.swiggy.alchemist.enums.EventNames;
import com.swiggy.alchemist.pojo.AggregationPojo;
import com.swiggy.alchemist.pojo.IncentiveOrderPojo;
import com.swiggy.alchemist.utility.SwiggyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.time.Instant;


@Service
@Transactional
@Slf4j
public class AggregationServiceImpl implements IDataAggregationService{

    @Autowired
    DePerformanceDao dePerformanceDao;

    @Autowired
    AggregationHelper aggregationHelper;

    @Override
    public void aggregateData(IncentiveOrderPojo eventDataPojo, String eventName) {
        if(eventDataPojo == null || eventDataPojo.getDeDetails() == null){
            return;
        }
        if(eventName.equals(EventNames.END_OF_DAY.getName()) || eventName.equals(EventNames.END_OF_WEEK.getName()) || eventName.equals(EventNames.END_OF_MONTH.getName())){
            return;
        }
        log.info("aggreate order 15 min ");
        aggregate15MinData(eventDataPojo,eventName);
        log.info("aggregate hourly data");
        aggregateHourlyData(eventDataPojo,eventName);
        log.info("aggreate order daily ");
        aggregateDailyData(eventDataPojo,eventName);
        log.info("aggreate order weekly");
        aggregateWeeklyData(eventDataPojo,eventName);
        log.info("aggreate order monthly");
        aggregateMonthlyData(eventDataPojo,eventName);
    }

    private void aggregateHourlyData(IncentiveOrderPojo incentiveOrderPojo, String eventName) {
        DePerformanceHourlyEntity hourPerformanceData = dePerformanceDao.getHourlyPerformanceData(incentiveOrderPojo.getDeDetails().getId(), incentiveOrderPojo.getAssignedTime());
        String perfBlob = performAggregation(hourPerformanceData.getPerfBlob(), incentiveOrderPojo,eventName);
        hourPerformanceData.setPerfBlob(perfBlob);
        hourPerformanceData.setUpdatedAt(Timestamp.from(Instant.now()));
        dePerformanceDao.saveHourlyPerformanceData(hourPerformanceData);
    }

    private void aggregate15MinData(IncentiveOrderPojo incentiveOrderPojo, String eventName) {
        DePerformanceQuarterEntity quaterHourPerformanceData = dePerformanceDao.get15MinPerformanceData(incentiveOrderPojo.getDeDetails().getId(), incentiveOrderPojo.getAssignedTime());
        String perfBlob = performAggregation(quaterHourPerformanceData.getPerfBlob(), incentiveOrderPojo,eventName);
        quaterHourPerformanceData.setPerfBlob(perfBlob);
        quaterHourPerformanceData.setUpdatedAt(Timestamp.from(Instant.now()));
        dePerformanceDao.save15MinPerformanceData(quaterHourPerformanceData);
    }

    private void aggregateDailyData(IncentiveOrderPojo incentiveOrderPojo, String eventName) {
        DePerformanceDailyEntity dailyPerformanceData = dePerformanceDao.getDailyPerformanceData(incentiveOrderPojo.getDeDetails().getId(), incentiveOrderPojo.getAssignedTime());
        String perfBlob = performAggregation(dailyPerformanceData.getPerfBlob(), incentiveOrderPojo,eventName);
        dailyPerformanceData.setPerfBlob(perfBlob);
        dailyPerformanceData.setUpdatedAt(Timestamp.from(Instant.now()));
        dePerformanceDao.saveDailyPerformanceData(dailyPerformanceData);
    }

    private void aggregateWeeklyData(IncentiveOrderPojo incentiveOrderPojo, String eventName) {
        DePerformanceWeeklyEntity weeklyPerformanceData = dePerformanceDao.getWeeklyPerformanceData(incentiveOrderPojo.getDeDetails().getId(), incentiveOrderPojo.getAssignedTime());
        String perfBlob = performAggregation(weeklyPerformanceData.getPerfBlob(), incentiveOrderPojo,eventName);
        weeklyPerformanceData.setPerfBlob(perfBlob);
        weeklyPerformanceData.setUpdatedAt(Timestamp.from(Instant.now()));
        dePerformanceDao.saveWeeklyPerformanceData(weeklyPerformanceData);
    }

    private void aggregateMonthlyData(IncentiveOrderPojo incentiveOrderPojo, String eventName) {
        DePerformanceMonthlyEntity monthlyPerformanceData = dePerformanceDao.getMonthlyPerformanceData(incentiveOrderPojo.getDeDetails().getId(), incentiveOrderPojo.getAssignedTime());
        String perfBlob = performAggregation(monthlyPerformanceData.getPerfBlob(), incentiveOrderPojo,eventName);
        monthlyPerformanceData.setPerfBlob(perfBlob);
        monthlyPerformanceData.setUpdatedAt(Timestamp.from(Instant.now()));
        dePerformanceDao.saveMonthlyPerformanceData(monthlyPerformanceData);
    }

    /**
     * To be done only when delivered
     * @param perfBlob
     * @param incentiveOrderPojo
     * @param eventName
     */
    private String performAggregation(String perfBlob, IncentiveOrderPojo incentiveOrderPojo, String eventName) {
        switch (EventNames.get(eventName)){
            case ORDER_STATUS_UPDATE:
                return performAggregationOnStatusUpdate(perfBlob,incentiveOrderPojo);
            case ORDER_REJECT:
                return performAggregationOnOrderReject(perfBlob);
            case CANCELLED:
                return performAggregationOnOrderCancel(perfBlob,incentiveOrderPojo);
            case DE_RATING:
                return performAggregationForDeRating(perfBlob,incentiveOrderPojo);
            default:
                return perfBlob;
        }

    }

    private String performAggregationForDeRating(String perfBlob,IncentiveOrderPojo incentiveOrderPojo) {
        AggregationPojo aggregationPojo = (AggregationPojo) SwiggyUtils.jsonDecode(perfBlob, AggregationPojo.class);
        if(StringUtils.isEmpty(perfBlob)){
            aggregationPojo = new AggregationPojo();
        }
        aggregationPojo.setAvgRating(aggregationHelper.getAvgRating(aggregationPojo,incentiveOrderPojo));
        aggregationPojo.setTotalRatings(aggregationPojo.getTotalRatings()+1);
        return SwiggyUtils.jsonEncode(aggregationPojo);
    }

    private String performAggregationOnOrderCancel(String perfBlob, IncentiveOrderPojo incentiveOrderPojo) {
        AggregationPojo aggregationPojo = (AggregationPojo) SwiggyUtils.jsonDecode(perfBlob, AggregationPojo.class);
        if(StringUtils.isEmpty(perfBlob)){
            aggregationPojo = new AggregationPojo();
        }
        aggregationPojo.setCancelledTripsCount(aggregationPojo.getCancelledTripsCount()+1);
        if(incentiveOrderPojo.getPickedUpTime() != 0){
            aggregationPojo.setTrips_count(aggregationPojo.getTrips_count()+incentiveOrderPojo.getOrderWeight());
        }
        return SwiggyUtils.jsonEncode(aggregationPojo);
    }

    private String performAggregationOnOrderReject(String perfBlob) {
        AggregationPojo aggregationPojo = (AggregationPojo) SwiggyUtils.jsonDecode(perfBlob, AggregationPojo.class);
        if(StringUtils.isEmpty(perfBlob)){
            aggregationPojo = new AggregationPojo();
        }
        aggregationPojo.setRejectCount(aggregationPojo.getRejectCount()+1);
        return SwiggyUtils.jsonEncode(aggregationPojo);
    }

    private String performAggregationOnStatusUpdate(String perfBlob, IncentiveOrderPojo incentiveOrderPojo) {
        if(incentiveOrderPojo.getDeliveredTime() == 0){
            return perfBlob;
        }
        AggregationPojo aggregationPojo = (AggregationPojo) SwiggyUtils.jsonDecode(perfBlob, AggregationPojo.class);
        if(StringUtils.isEmpty(perfBlob)){
            aggregationPojo = new AggregationPojo();
        }
        aggregationPojo.setAvgConfirmationDelay(aggregationHelper.getAvgConfirmationDelay(aggregationPojo,incentiveOrderPojo));
        aggregationPojo.setAvgArrivalDelay(aggregationHelper.getAvgArrivalDelay(aggregationPojo,incentiveOrderPojo));
        aggregationPojo.setAvgPickupDelay(aggregationHelper.getAvgPickupDelay(aggregationPojo,incentiveOrderPojo));
        aggregationPojo.setAvgDeliveredDelay(aggregationHelper.getAvgDeliveryDelay(aggregationPojo,incentiveOrderPojo));
        aggregationPojo.setAvgFirstMile(aggregationHelper.getAvgFirstMile(aggregationPojo,incentiveOrderPojo));
        aggregationPojo.setAvgLastMile(aggregationHelper.getAvgLastMile(aggregationPojo,incentiveOrderPojo));
        aggregationPojo.setAvgReachedDelay(aggregationHelper.getAvgReachedDelay(aggregationPojo,incentiveOrderPojo));
        aggregationPojo.setAvgSla(aggregationHelper.getAvgSla(aggregationPojo,incentiveOrderPojo));
        aggregationPojo.setTrips_count(aggregationPojo.getTrips_count()+incentiveOrderPojo.getOrderWeight());

        return SwiggyUtils.jsonEncode(aggregationPojo);
    }


}
