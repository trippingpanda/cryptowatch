package com.swiggy.alchemist.services.aggregation;

import com.swiggy.alchemist.pojo.IncentiveOrderPojo;

public interface IDataAggregationService {

    void aggregateData(IncentiveOrderPojo defaultEventDataPojo, String eventName);
}
