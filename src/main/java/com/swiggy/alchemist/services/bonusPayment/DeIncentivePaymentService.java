package com.swiggy.alchemist.services.bonusPayment;

import com.swiggy.alchemist.db.alchemist.entities.IncentiveRulesEntity;

public interface DeIncentivePaymentService {
    void updateDailyIncentiveBonusPayment(Long id, IncentiveRulesEntity incentiveRule, Double bonusAmount, String date);

    void updateWeeklyIncentiveBonusPayment(Long id, IncentiveRulesEntity incentiveRule, Double bonusAmount, String week);

    void updateMonthlyIncentiveBonusPayment(Long id, IncentiveRulesEntity incentiveRule, Double bonusAmount, String month);
}
