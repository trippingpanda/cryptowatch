package com.swiggy.alchemist.services.bonusPayment;

import com.swiggy.alchemist.db.alchemist.entities.IncentiveRulesEntity;
import org.springframework.stereotype.Service;

@Service
public class DeIncentivePaymentServiceImpl implements DeIncentivePaymentService {



    @Override
    public void updateDailyIncentiveBonusPayment(Long id, IncentiveRulesEntity incentiveRule, Double bonusAmount, String date) {

    }

    @Override
    public void updateWeeklyIncentiveBonusPayment(Long id, IncentiveRulesEntity incentiveRule, Double bonusAmount, String week) {

    }

    @Override
    public void updateMonthlyIncentiveBonusPayment(Long id, IncentiveRulesEntity incentiveRule, Double bonusAmount, String month) {

    }
}
