package com.swiggy.alchemist.services.ruleParser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.alchemist.antlr.AntlrHelper;
import com.swiggy.alchemist.constants.EntityConstants;
import com.swiggy.alchemist.dao.city.CityDao;
import com.swiggy.alchemist.dao.deShifts.DeShiftsDao;
import com.swiggy.alchemist.dao.parser.RuleParserDao;
import com.swiggy.alchemist.dao.timeslot.TimeSlotDao;
import com.swiggy.alchemist.dao.zone.ZoneDao;
import com.swiggy.alchemist.exceptions.AlchemistException;
import com.swiggy.alchemist.pojo.RuleDomainPojo;
import com.swiggy.alchemist.pojo.TimeSlotDataArray;
import com.swiggy.alchemist.pojo.TimeslotData;
import com.swiggy.alchemist.utility.RuleParserHelper;
import com.swiggy.alchemist.utility.SwiggyUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by RAVISINGH on 22/09/16.
 */
@Service
public class RuleParserBoImpl implements RuleParserBo {
    private static final Logger logger = LoggerFactory
            .getLogger(RuleParserBoImpl.class);
    @Autowired
    RuleParserDao ruleParserDao;
    @Autowired
    TimeSlotDao timeSlotDao;
    @Autowired
    DeShiftsDao deShiftsDao;
    @Autowired
    ZoneDao zoneDao;
    @Autowired
    CityDao cityDao;
    @Autowired private ObjectMapper objectMapper;

    @Autowired
    RuleParserHelper ruleParserHelper;

    @Override
    public void addRule(String ruleString, Long id, String user, Boolean isActive) throws  AlchemistException {
        HashMap<String,String> keyValuePairs = AntlrHelper.parseRules(ruleString);
        if (!ruleParserHelper.validateRuleString(keyValuePairs)) {
            return;
        }
        RuleDomainPojo ruleDomain = AntlrHelper.parseRuleDomainString(keyValuePairs.get(EntityConstants.FOR));
        if (ruleDomain.getIncentiveGroups() != null && ruleDomain.getIncentiveGroups().size() > 0&&
        deShiftsDao.getIdByName(ruleDomain.getIncentiveGroups()).size() <= 0) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST,"No incentive group with given name");
        }else if (ruleDomain.getZones() != null && ruleDomain.getZones().size() > 0
                && zoneDao.getZonesByName(ruleDomain.getZones()).size() <= 0) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST,"No zone with given name");
        } else if (ruleDomain.getCities() != null && ruleDomain.getCities().size() > 0 &&
                cityDao.getCitiesByName(ruleDomain.getCities()).size() <=0) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST,"No city with given name");
        }
        List<Long> timeSlotIds = addTimeSlots(keyValuePairs.get(EntityConstants.TIMESLOT));
        Long ruleId =  ruleParserDao.addRule(keyValuePairs.get(EntityConstants.NAME),
                keyValuePairs.get(EntityConstants.DESCRIPTION),keyValuePairs.get(EntityConstants.BONUS),
                keyValuePairs.get(EntityConstants.CONDITION), SwiggyUtils.getTimeStamp(keyValuePairs.get(EntityConstants.FROM)),
                SwiggyUtils.getTimeStamp(keyValuePairs.get(EntityConstants.TO)),keyValuePairs.get(EntityConstants.STATUS_EXP),
                keyValuePairs.get(EntityConstants.STATUS_MESSAGE),ruleString,keyValuePairs.get(EntityConstants.COMPUTE_ON),
                keyValuePairs.get(EntityConstants.SCOPE),id, user, isActive);
        if(timeSlotIds != null && timeSlotIds.size() >= 0) {
            timeSlotDao.addTimeSlotMap(ruleId, timeSlotIds);
        }

        if (ruleDomain.getIncentiveGroups() != null && ruleDomain.getIncentiveGroups().size() > 0) {
            ruleParserDao.addIncentiveGroupMap(ruleId, ruleDomain.getIncentiveGroups());
        }
        logger.info("cities :{}", ruleDomain.getCities());
        if (ruleDomain.getCities() != null && ruleDomain.getCities().size() > 0) {
            List<Long> zone = new ArrayList<>();
            for (Long city: cityDao.getCitiesByName(ruleDomain.getCities())) {
                zone.addAll(zoneDao.getZoneByCity(city));
            }
            logger.info("zones in cities :{}", zone);
            ruleParserDao.addZoneMapWithIds(ruleId, zone);
        } else if (ruleDomain.getZones() != null && ruleDomain.getZones().size() > 0) {
            ruleParserDao.addZoneMap(ruleId, ruleDomain.getZones());
        }
    }

    private String processTimeSlotData(String timeSlotData) {
        String result = "{\"timeSlotArray\":[";
        result +=timeSlotData;
        result+="]}";
        logger.info("processed timeslot data: {}",result);
        return result;
    }

    private List<Long> addTimeSlots(String timeSlotString) {
        List<Long> result = new ArrayList<Long>();
        if (timeSlotString == null) {
            return result;
        }
        try {
            TimeSlotDataArray timeSlotDataArray = objectMapper.readValue(
                    processTimeSlotData(timeSlotString), TimeSlotDataArray.class);
            //validating timeslots
            for (TimeslotData timeslotData : timeSlotDataArray.getTimeslotArray()) {
                if(timeslotData.getDaysString() == null ){
                    timeslotData.setDaysString("1,2,3,4,5,6,7");
                }
                List<String> times = Arrays.asList(timeslotData.getTimeString().split("-"));
                int startTime = Integer.parseInt(times.get(0));
                int endTime = Integer.parseInt(times.get(1));
                if (!(startTime >= 0 && startTime <=2459 && endTime >= 0 && endTime <=2459)){
                    throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "Invalid start time");
                }
                times = Arrays.asList(timeslotData.getDaysString().split(","));
                for(String day : times) {
                    int dayInt = Integer.parseInt(day);
                    if (dayInt < 1|| dayInt >7) {
                        throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "Invalid days");
                    }
                }
            }
            for (TimeslotData timeslotData : timeSlotDataArray.getTimeslotArray()) {
                result.add(timeSlotDao.addTimeSlot(timeslotData));
            }
        } catch (IOException e) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "Invalid timeslot");
        }
        return result;
    }

}
