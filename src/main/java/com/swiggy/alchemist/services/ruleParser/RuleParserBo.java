package com.swiggy.alchemist.services.ruleParser;

import com.swiggy.alchemist.exceptions.AlchemistException;

/**
 * Created by RAVISINGH on 22/09/16.
 */
public interface RuleParserBo {

    void addRule(String ruleString, Long id, String user, Boolean isActive) throws AlchemistException;
}
