package com.swiggy.alchemist.services.reports;

import com.swiggy.alchemist.exceptions.AlchemistException;

import java.io.ByteArrayOutputStream;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by RAVISINGH on 17/10/16.
 */

public interface AlchemistReportsBo {

    ByteArrayOutputStream cityReports(Long cityId, Timestamp from, Timestamp to) throws AlchemistException;

    ByteArrayOutputStream zoneReports(List<Long> zoneIds, Timestamp from, Timestamp to, String cityId) throws AlchemistException;
}
