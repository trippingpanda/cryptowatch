package com.swiggy.alchemist.services.reports;

import com.swiggy.alchemist.constants.EntityConstants;
import com.swiggy.alchemist.dao.deliveryBoys.DeliveryBoysDao;
import com.swiggy.alchemist.dao.rule.RuleDao;
import com.swiggy.alchemist.dao.zone.ZoneDao;
import com.swiggy.alchemist.exceptions.AlchemistException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by RAVISINGH on 17/10/16.
 */
@Service
@Slf4j
public class AlchemistReportBoImpl implements AlchemistReportsBo {

    @Autowired
    DeliveryBoysDao deliveryBoysDao;
    @Autowired
    RuleDao ruleDao;

    @Autowired
    ZoneDao zoneDao;

    private static final Logger logger = LoggerFactory
            .getLogger(AlchemistReportBoImpl.class);
    private static final Charset UTF_8_CHARSET = Charset.forName("UTF-8");

    public ByteArrayOutputStream cityReports(Long cityId, Timestamp from, Timestamp to) {
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        addHeaders(bs);
        List<Long> zoneIds = zoneDao.getZoneByCity(cityId);
        List<Long> deIds = deliveryBoysDao.getDeOfZone(zoneIds);
        addDataForDeList(bs,deIds,"",from,to,String.valueOf(cityId));
        return bs;
    }

    public ByteArrayOutputStream zoneReports(List<Long> zoneIds, Timestamp from, Timestamp to, String cityId) {
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        log.info("cityId {}", cityId);
        addHeaders(bs);
        for (Long zoneId: zoneIds) { //looping on zones because we want zoneid in reports.
            List<Long> deList = deliveryBoysDao.getDeOfZone(zoneId);
            addDataForDeList(bs,deList,String.valueOf(zoneId),from,to,cityId);
        }
        return bs;
    }

    private void addDataForDeList(ByteArrayOutputStream bs,List<Long> deList, String zoneId, Timestamp from, Timestamp to, String cityId) {
        log.info("deIds size: {}, from: {}, to:{}",deList,from,to);
        if (deList != null && deList.size() > 0) {
            List<Object[]> reportResult = ruleDao.getDailyMapForDes(deList, from, to);
            for (Object[] o : reportResult) {
                addZoneRow(bs, o, EntityConstants.SCOPE_DAY, zoneId, cityId);
            }
            log.info("size 1 " + reportResult.size());
            reportResult = ruleDao.getWeeklyMapForDes(deList, from, to);
            for (Object[] o : reportResult) {
                addZoneRow(bs, o, EntityConstants.SCOPE_WEEK, zoneId, cityId);
            }
            log.info("size 2 " + reportResult.size());
            reportResult = ruleDao.getMonthlyMapForDes(deList, from, to);
            for (Object[] o : reportResult) {
                addZoneRow(bs, o, EntityConstants.SCOPE_MONTH, zoneId, cityId);
            }
            log.info("size 3 " + reportResult.size());
        }
    }
    private void addHeaders(ByteArrayOutputStream bs) {
        StringBuffer sb = new StringBuffer();
        sb.append("DeId");
        sb.append(",");
        sb.append("Zone");
        sb.append(",");
        sb.append("City");
        sb.append(",");
        sb.append("archived time");
        sb.append(",");
        sb.append("Rule name");
        sb.append(",");
        sb.append("Bonus");
        sb.append(",");
        sb.append("Count");
        sb.append(",");
        sb.append("Scope");
        sb.append("\n");
        try {
            bs.write(sb.toString().getBytes(UTF_8_CHARSET));
        } catch (IOException e) {
            logger.error("I/O Exception when trying to write to ByteArrayOutput Stream", e);
            throw new AlchemistException(HttpStatus.SC_INTERNAL_SERVER_ERROR,
                    "Internal server error when trying to generate the report.");
        }
    }

    private void addZoneRow(ByteArrayOutputStream bs, Object[] object, String scope, String zone, String city) {
        StringBuffer sb = new StringBuffer();
        sb.append(String.valueOf(object[0]));
        sb.append(",");
        sb.append(zone);
        sb.append(",");
        sb.append(city);
        sb.append(",");
        sb.append(String.valueOf(object[1]));
        sb.append(",");
        sb.append(String.valueOf(object[2]));
        sb.append(",");
        sb.append(String.valueOf(object[3]));
        sb.append(",");
        sb.append(String.valueOf(object[4]));
        sb.append(",");
        sb.append(scope);
        sb.append("\n");
        try {
            bs.write(sb.toString().getBytes(UTF_8_CHARSET));
        } catch (IOException e) {
            logger.error("I/O Exception when trying to write to ByteArrayOutput Stream", e);
            throw new AlchemistException(HttpStatus.SC_INTERNAL_SERVER_ERROR,
                    "Internal server error when trying to generate the report.");
        }
    }
}
