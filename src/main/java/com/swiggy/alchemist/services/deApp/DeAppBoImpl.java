package com.swiggy.alchemist.services.deApp;

import com.swiggy.alchemist.dao.rule.RuleDao;
import com.swiggy.alchemist.exceptions.AlchemistException;
import com.swiggy.alchemist.pojo.DeAppDailyResponsePojo;
import com.swiggy.alchemist.pojo.DeAppMontlyResponsePojo;
import com.swiggy.alchemist.pojo.DeAppRulePojo;
import com.swiggy.alchemist.pojo.DeAppWeeklyResponsePojo;
import com.swiggy.alchemist.utility.SwiggyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by RAVISINGH on 07/11/16.
 */
@Service
public class DeAppBoImpl implements DeAppBo {

    @Autowired
    RuleDao ruleDao;

    private static final Logger logger = LoggerFactory
            .getLogger(DeAppBoImpl.class);
    @Autowired @Qualifier("deliveryRedisTemplate") private RedisTemplate<String,String> deliveryRedisTemplate;
    @Autowired private StringRedisTemplate stringRedisTemplate;
    private static final String TID_KEY = "DE_TID_";


    @Override
    public Long authenticateRequest(String auth) throws AlchemistException{
        if (auth != null && auth.startsWith("Basic")) {
            // Authorization: Basic base64credentials
            String base64Credentials = auth.substring("Basic".length()).trim();
            String credentials = new String(Base64.getDecoder().decode(base64Credentials),
                    Charset.forName("UTF-8"));
            // credentials = username:password
            final String[] values = credentials.split(":", 2);
            logger.info("Basic auth user:{}, passwrd:{}", values[0],values[1]);
            long deId = Long.parseLong(values[0]);
            String tid = deliveryRedisTemplate.opsForValue().get(TID_KEY+deId);
            logger.info("Tid from cache: {} key, {}",tid,TID_KEY+deId);
            if (tid == null || !tid.equals(values[1])){
                throw new AlchemistException(HttpStatus.SC_UNAUTHORIZED, "Incorrect password");
            }
            return deId;
        }
        throw new AlchemistException(HttpStatus.SC_UNAUTHORIZED, "Auth not provided");
    }

    @Override
    public DeAppDailyResponsePojo getDailyData(int day, Long deId){
        List<Long> deIds = new ArrayList<Long>(Arrays.asList(deId));
        DeAppDailyResponsePojo deAppDailyResponsePojo = new DeAppDailyResponsePojo();
        deAppDailyResponsePojo.setDeAppRulePojos(new ArrayList<>());
        List<Object[]> reportResult;
        if (day == 0 ){
            reportResult = ruleDao.getDailyMapForToday(deIds);
            reportResult.addAll(ruleDao.getWeeklyMapForToday(deIds));
            reportResult.addAll(ruleDao.getMonthlyMapForToday(deIds));
        } else {
            Timestamp startTime = SwiggyUtils.getStartDayTime(day);
            Timestamp endTime = SwiggyUtils.getEndDayTime(day);
            logger.info("startTIme: {} endTIme: {}",startTime, endTime);
            reportResult = ruleDao.getDailyMapForDes(deIds, startTime,endTime);
            reportResult.addAll(ruleDao.getWeeklyMapForDes(deIds,startTime,endTime));
            reportResult.addAll(ruleDao.getMonthlyMapForDes(deIds,startTime,endTime));
        }
        for (Object[] o : reportResult) {
            addRuleRow(deAppDailyResponsePojo , o);
        }
        return deAppDailyResponsePojo;
    }

    public DeAppWeeklyResponsePojo getWeekData(int week, Long deId) {
        DeAppWeeklyResponsePojo deAppWeeklyResponsePojo = new DeAppWeeklyResponsePojo();
        Long total =0L ;
        Timestamp[] timestamps = SwiggyUtils.getDaysOfWeek(week);
        for (int i =0 ; i<timestamps.length-1; i++) {
            Long dayTotal = 0L;
            logger.info("startTIme: {} endTIme: {}",timestamps[i+1], timestamps[i]);
            dayTotal+=ruleDao.getDailyBonus(deId,timestamps[i+1],timestamps[i]);
            dayTotal+=ruleDao.getWeeklyBonus(deId,timestamps[i+1],timestamps[i]);
            dayTotal+=ruleDao.getMonthlyBonus(deId,timestamps[i+1],timestamps[i]);
            addDayDataInWeek(deAppWeeklyResponsePojo,i+1,String.valueOf(dayTotal));
            total+=dayTotal;
        }
        deAppWeeklyResponsePojo.setTotal(String.valueOf(total));
        return deAppWeeklyResponsePojo;
    }

    public DeAppMontlyResponsePojo getMonthData(Long deId) {
        DeAppMontlyResponsePojo deAppMonthlyResponsePojo = new DeAppMontlyResponsePojo();
        Long total =0L ;
        Timestamp[] timestamps = SwiggyUtils.getTimesForMonth();
        for (int i =0 ; i<timestamps.length-1; i++) {
            Long dayTotal = 0L;
            logger.info("startTIme: {} endTIme: {}",timestamps[i+1], timestamps[i]);
            dayTotal+=ruleDao.getDailyBonus(deId,timestamps[i+1],timestamps[i]);
            dayTotal+=ruleDao.getWeeklyBonus(deId,timestamps[i+1],timestamps[i]);
            dayTotal+=ruleDao.getMonthlyBonus(deId,timestamps[i+1],timestamps[i]);
            addDayDataInMonth(deAppMonthlyResponsePojo,i+1,String.valueOf(dayTotal));
            total+=dayTotal;
        }
        deAppMonthlyResponsePojo.setTotal(String.valueOf(total));
        return deAppMonthlyResponsePojo;
    }

    private void addRuleRow(DeAppDailyResponsePojo deAppDailyResponsePojo ,Object[] object) {
        logger.info("object array :{}",object);
        String total = deAppDailyResponsePojo.getTotal();
        if (total == null) {
            deAppDailyResponsePojo.setTotal(String.valueOf(object[3]).trim());
        } else {
            deAppDailyResponsePojo.setTotal(String.valueOf(Double.parseDouble(total)+
                    Double.parseDouble(String.valueOf(object[3]).trim())));
        }

        String orders = null;
        if(object.length > 6) {
            orders = String.valueOf(object[6]).trim();
        }
        logger.info("order string :{}",orders);
        if (StringUtils.isEmpty(orders) || orders.equals("null")) {
            Double bonus = Double.parseDouble(String.valueOf(object[3]).trim());
            DeAppRulePojo deAppRulePojo = new DeAppRulePojo();
            deAppRulePojo.setRuleName(String.valueOf(object[2]).trim());
            deAppRulePojo.setBonus(String.valueOf(object[3]).trim());
            deAppRulePojo.setTime(new Timestamp(Calendar.getInstance().getTime().getTime()).getTime());
            if (true || bonus > 0) {
                deAppDailyResponsePojo.getDeAppRulePojos().add(deAppRulePojo);
            }
        } else {
            List<String> orderList = Arrays.asList(orders.split(","));
            int count = Integer.parseInt(String.valueOf(object[4]).trim());
            for (String order : orderList) {
                Double bonus = Double.parseDouble(String.valueOf(object[3]).trim())/count;
                DeAppRulePojo deAppRulePojo = new DeAppRulePojo();
                deAppRulePojo.setRuleName(String.valueOf(object[2]).trim()+ "-" +order);
                deAppRulePojo.setBonus(String.valueOf(bonus).trim());
                deAppRulePojo.setTime(new Timestamp(Calendar.getInstance().getTime().getTime()).getTime());
                if (true || bonus > 0) {
                    deAppDailyResponsePojo.getDeAppRulePojos().add(deAppRulePojo);
                }
            }
        }

    }

    private void addDayDataInWeek(DeAppWeeklyResponsePojo deAppWeeklyResponsePojo, int day,String bonus) {
        switch (day) {
            case 1:
                deAppWeeklyResponsePojo.setFirstDay(bonus);
                break;
            case 2:
                deAppWeeklyResponsePojo.setSecondDay(bonus);
                break;
            case 3:
                deAppWeeklyResponsePojo.setThirdDay(bonus);
                break;
            case 4:
                deAppWeeklyResponsePojo.setFourthDay(bonus);
                break;
            case 5:
                deAppWeeklyResponsePojo.setFifthDay(bonus);
                break;
            case 6:
                deAppWeeklyResponsePojo.setSixthDay(bonus);
                break;
            case 7:
                deAppWeeklyResponsePojo.setSeventhDay(bonus);
                break;

        }
    }
    private void addDayDataInMonth(DeAppMontlyResponsePojo deAppMontlyResponsePojo, int week,String bonus) {
        switch (week) {
            case 1:
                deAppMontlyResponsePojo.setFirstWeek(bonus);
                break;
            case 2:
                deAppMontlyResponsePojo.setSecondWeek(bonus);
                break;
            case 3:
                deAppMontlyResponsePojo.setThirdWeek(bonus);
                break;
            case 4:
                deAppMontlyResponsePojo.setFourthWeek(bonus);
                break;

        }
    }
}
