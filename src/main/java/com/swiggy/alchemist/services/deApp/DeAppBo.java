package com.swiggy.alchemist.services.deApp;

import com.swiggy.alchemist.exceptions.AlchemistException;
import com.swiggy.alchemist.pojo.DeAppDailyResponsePojo;
import com.swiggy.alchemist.pojo.DeAppMontlyResponsePojo;
import com.swiggy.alchemist.pojo.DeAppWeeklyResponsePojo;

/**
 * Created by RAVISINGH on 07/11/16.
 */
public interface DeAppBo {

    DeAppDailyResponsePojo getDailyData(int day, Long deId);

    DeAppWeeklyResponsePojo getWeekData(int week, Long deId);

    DeAppMontlyResponsePojo getMonthData(Long deId);

    public Long authenticateRequest(String auth) throws AlchemistException;
}
