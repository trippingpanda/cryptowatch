package com.swiggy.alchemist.services.order;

import com.swiggy.alchemist.pojo.EventPojo;
import com.swiggy.alchemist.pojo.IncentiveOrderPojo;

/**
 * Created by abhishek.rawat on 9/28/16.
 */
public interface OrdersService {
    IncentiveOrderPojo cacheOrder(EventPojo eventPojo);

    public boolean doesOrderExists(String orderId);

    public IncentiveOrderPojo getIncentiveOrderPojo(String orderId);
}
