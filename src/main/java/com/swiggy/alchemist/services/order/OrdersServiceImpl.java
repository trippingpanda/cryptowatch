package com.swiggy.alchemist.services.order;

import com.swiggy.alchemist.dao.deliveryBoys.DeliveryBoysDao;
import com.swiggy.alchemist.dao.surge.SurgeDao;
import com.swiggy.alchemist.dao.zone.ZoneDao;
import com.swiggy.alchemist.db.delivery.entities.ZoneEntity;
import com.swiggy.alchemist.db.delivery.repository.TripRepository;
import com.swiggy.alchemist.enums.EventNames;
import com.swiggy.alchemist.pojo.*;
import com.swiggy.alchemist.services.caching.RedisLock;
import com.swiggy.alchemist.utility.SwiggyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

/**
 * Created by abhishek.rawat on 9/28/16.
 */
@Service
@Slf4j
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    RedisTemplate<String, String> redisTemplate;

    @Autowired
    RedisLock redisLock;

    @Autowired
    TripRepository tripRepository;

    @Autowired
    ZoneDao zoneDao;

    @Autowired
    SurgeDao surgeDao;

    private static final String  INCENTIVE_ORDER_PREFIX = "INCENTIVE_ORDER_";

    @Autowired
    DeliveryBoysDao deliveryBoysDao;
    @Override
    public IncentiveOrderPojo cacheOrder(EventPojo eventPojo) {
        switch (EventNames.get(eventPojo.getEventName())) {
            case ORDER_STATUS_UPDATE:
                return saveOrderOnStatusUpdate((StatusUpdatePojo) eventPojo.getEventData());
            case NEW_ORDER:
                return saveNewOrder((PortalOrderData) eventPojo.getEventData());
            case DE_RATING:
                return saveDeRating((DeRatingPojo) eventPojo.getEventData());
            case ORDER_REJECT:
                return saveOrderReject((StatusUpdatePojo) eventPojo.getEventData());
            case CANCELLED:
                return saveOrderCancel((CancelOrderPojo) eventPojo.getEventData());
            default:
                return null;
        }
    }

    @Override
    public IncentiveOrderPojo getIncentiveOrderPojo(String orderId) {
        String incentiveOrderJson = redisTemplate.opsForValue().get(INCENTIVE_ORDER_PREFIX + orderId);
        IncentiveOrderPojo incentiveOrderPojo = (IncentiveOrderPojo) SwiggyUtils.jsonDecode(incentiveOrderJson, IncentiveOrderPojo.class);

        if (incentiveOrderPojo == null) {
            incentiveOrderPojo = new IncentiveOrderPojo();
        }
        return  incentiveOrderPojo;
    }

    private IncentiveOrderPojo saveOrderCancel(CancelOrderPojo eventData) {
        if (true) {
            String incentiveOrderJson = redisTemplate.opsForValue().get(INCENTIVE_ORDER_PREFIX + eventData.getOrderId());
            IncentiveOrderPojo incentiveOrderPojo = (IncentiveOrderPojo) SwiggyUtils.jsonDecode(incentiveOrderJson, IncentiveOrderPojo.class);

            if (incentiveOrderPojo == null) {
                incentiveOrderPojo = new IncentiveOrderPojo();
            }
            incentiveOrderPojo.setCancelled(true);
            if(null != incentiveOrderPojo.getBatchId()) {
                decrementBatchCount(incentiveOrderPojo.getBatchId());
            }
            redisTemplate.opsForValue().set(INCENTIVE_ORDER_PREFIX + eventData.getOrderId(), SwiggyUtils.jsonEncode(incentiveOrderPojo), 4, TimeUnit.HOURS);

            return incentiveOrderPojo;
        }
        return null;
    }

    private IncentiveOrderPojo saveOrderReject(StatusUpdatePojo eventData) {
        if (true) {
            String incentiveOrderJson = redisTemplate.opsForValue().get(INCENTIVE_ORDER_PREFIX + eventData.getOrderId());
            IncentiveOrderPojo incentiveOrderPojo = (IncentiveOrderPojo) SwiggyUtils.jsonDecode(incentiveOrderJson, IncentiveOrderPojo.class);

            if (incentiveOrderPojo == null) {
                incentiveOrderPojo = new IncentiveOrderPojo();
            }
            incentiveOrderPojo.setRejectCount(incentiveOrderPojo.getRejectCount() + 1);
            redisTemplate.opsForValue().set(INCENTIVE_ORDER_PREFIX + eventData.getOrderId(), SwiggyUtils.jsonEncode(incentiveOrderPojo), 4, TimeUnit.HOURS);

            return incentiveOrderPojo;
        }
        return null;
    }

    private IncentiveOrderPojo saveDeRating(DeRatingPojo eventData) {
        if (true) {
            String incentiveOrderJson = redisTemplate.opsForValue().get(INCENTIVE_ORDER_PREFIX + eventData.getOrderId());
            IncentiveOrderPojo incentiveOrderPojo = (IncentiveOrderPojo) SwiggyUtils.jsonDecode(incentiveOrderJson, IncentiveOrderPojo.class);

            if (incentiveOrderPojo == null) {
                incentiveOrderPojo = new IncentiveOrderPojo();
                incentiveOrderPojo.setRating(eventData.getRating());
                DeDetails deDetails = new DeDetails();
                deDetails.setId(Long.valueOf(eventData.getDeId()));
                incentiveOrderPojo.setDeDetails(deDetails);
                incentiveOrderPojo.setAssignedTime(tripRepository.findByOrderId(eventData.getOrderId()).getAssignedTime().getTime());
            } else {
                incentiveOrderPojo.setRating(eventData.getRating());
            }

            redisTemplate.opsForValue().set(INCENTIVE_ORDER_PREFIX + eventData.getOrderId(), SwiggyUtils.jsonEncode(incentiveOrderPojo), 4, TimeUnit.HOURS);

            return incentiveOrderPojo;
        }
        return null;
    }

    private IncentiveOrderPojo saveNewOrder(PortalOrderData portalOrderData) {
        if (true) {
            String incentiveOrderJson = redisTemplate.opsForValue().get(INCENTIVE_ORDER_PREFIX + portalOrderData.getOrderId());
            log.info("update order");
            IncentiveOrderPojo incentiveOrderPojo = (IncentiveOrderPojo) SwiggyUtils.jsonDecode(incentiveOrderJson, IncentiveOrderPojo.class);

            if (incentiveOrderPojo == null) {
                incentiveOrderPojo = new IncentiveOrderPojo();
            }

            incentiveOrderPojo.setLastMile(Double.parseDouble(portalOrderData.getPredictedlastMile()));
            incentiveOrderPojo.setOrderId(portalOrderData.getOrderId());
            redisTemplate.opsForValue().set(INCENTIVE_ORDER_PREFIX + portalOrderData.getOrderId(), SwiggyUtils.jsonEncode(incentiveOrderPojo), 4, TimeUnit.HOURS);


            return incentiveOrderPojo;
        }
        return null;
    }

    /**
     * @param statusUpdatePojo
     */
    private IncentiveOrderPojo saveOrderOnStatusUpdate(StatusUpdatePojo statusUpdatePojo) {
        if (true) {
            Long zoneId;
            String incentiveOrderJson = redisTemplate.opsForValue().get(INCENTIVE_ORDER_PREFIX + statusUpdatePojo.getOrderId());
            log.info("update order");
            IncentiveOrderPojo incentiveOrderPojo = (IncentiveOrderPojo) SwiggyUtils.jsonDecode(incentiveOrderJson, IncentiveOrderPojo.class);

            if (incentiveOrderPojo == null) {
                incentiveOrderPojo = new IncentiveOrderPojo();
            }
            incentiveOrderPojo.setBatchId(statusUpdatePojo.getBatchId());
            switch (statusUpdatePojo.getStatus()) {
                case "assigned":
                    zoneId = deliveryBoysDao.getZoneForDe(statusUpdatePojo.getDeliveryBoyId());
                    incentiveOrderPojo.setAssignedTime(statusUpdatePojo.getTime());
                    incentiveOrderPojo.setDeDetails(statusUpdatePojo.getDeDetails());
                    incentiveOrderPojo.setBatchCount(getIncrementedOrderCount(statusUpdatePojo.getBatchId()));
                    incentiveOrderPojo.setOrderWeight(getOrderWeight(incentiveOrderPojo));
                    incentiveOrderPojo.setBatchType("1");
                    incentiveOrderPojo.setSurge(String.valueOf(surgeDao.getSurge(zoneId)));
                    //TODO optimise this and get rain mode from cache.
                    incentiveOrderPojo.setRainMode(zoneDao.isRainModeOn(zoneId));
                    break;
                case "confirmed":
                    incentiveOrderPojo.setConfirmationTime(statusUpdatePojo.getTime());
                    correctBatchCountIfRequired(incentiveOrderPojo, statusUpdatePojo.getBatchId());
                    break;
                case "pickedup":
                    incentiveOrderPojo.setPickedUpTime(statusUpdatePojo.getTime());
                    correctBatchCountIfRequired(incentiveOrderPojo, statusUpdatePojo.getBatchId());
                    break;
                case "reached":
                    incentiveOrderPojo.setReachedTime(statusUpdatePojo.getTime());
                    correctBatchCountIfRequired(incentiveOrderPojo, statusUpdatePojo.getBatchId());
                    break;
                case "arrived":
                    incentiveOrderPojo.setArrivedTime(statusUpdatePojo.getTime());
                    correctBatchCountIfRequired(incentiveOrderPojo, statusUpdatePojo.getBatchId());
                    break;
                case "delivered":
                    zoneId = deliveryBoysDao.getZoneForDe(statusUpdatePojo.getDeliveryBoyId());
                    incentiveOrderPojo.setDeliveredTime(statusUpdatePojo.getTime());
                    incentiveOrderPojo.setRainMode(zoneDao.isRainModeOn(zoneId));
                    incentiveOrderPojo.setSurge(String.valueOf(surgeDao.getSurge(zoneId)));
                    incentiveOrderPojo.setSla(Math.round((double) (incentiveOrderPojo.getDeliveredTime() - incentiveOrderPojo.getAssignedTime()) / (1000 * 60)));
                    correctBatchCountIfRequired(incentiveOrderPojo, statusUpdatePojo.getBatchId());
                    break;
                case "unassigned":
                    decrementBatchCount(statusUpdatePojo.getBatchId());
                    break;

            }
            redisTemplate.opsForValue().set(INCENTIVE_ORDER_PREFIX + statusUpdatePojo.getOrderId(), SwiggyUtils.jsonEncode(incentiveOrderPojo), 4, TimeUnit.HOURS);

            return incentiveOrderPojo;
        }
        return null;
    }

    private double getOrderWeight(IncentiveOrderPojo incentiveOrderPojo) {
        if(incentiveOrderPojo.getBatchCount() == 1){
            return 1;
        }

        long zoneID = deliveryBoysDao.getZoneForDe(incentiveOrderPojo.getDeDetails().getId());
        ZoneEntity zone = zoneDao.getZone(zoneID);
        String orderWeights = zone.getOrderWeights();

        if(StringUtils.isEmpty(orderWeights)){
            return 0.5; //default weight for orders after 1st one.
        }

        String[] weights = orderWeights.split(",");

        if(weights.length < incentiveOrderPojo.getBatchCount()){
            return Double.parseDouble(weights[weights.length-1]); // return last value if weights are not defined till this batch count;
        }

        return Double.parseDouble(weights[incentiveOrderPojo.getBatchCount()-1]);
    }

    private int getIncrementedOrderCount(Long batchId) {
        String ordersInBatch = redisTemplate.opsForValue().get("ORDERS_IN_BATCH_" + batchId);
        if (StringUtils.isEmpty(ordersInBatch)) {
            redisTemplate.opsForValue().set("ORDERS_IN_BATCH_" + batchId, "1");
            return 1;
        }
        redisTemplate.opsForValue().set("ORDERS_IN_BATCH_" + batchId, String.valueOf(Integer.parseInt(ordersInBatch) + 1));
        return Integer.parseInt(ordersInBatch) + 1;
    }

    private void correctBatchCountIfRequired(IncentiveOrderPojo incentiveOrderPojo, Long batchId){
        //TODO this will not work if batch count > 2. This should be refactored and ORDERS_IN_BATCH must be a list.
        int batchCount = incentiveOrderPojo.getBatchCount();
        int totalBatchCount = getOrderCount(batchId);
        if (totalBatchCount < batchCount){
            incentiveOrderPojo.setBatchCount(totalBatchCount);
            incentiveOrderPojo.setOrderWeight(getOrderWeight(incentiveOrderPojo));
        }
    }

    private int getOrderCount(Long batchId) {
        String ordersInBatch = redisTemplate.opsForValue().get("ORDERS_IN_BATCH_" + batchId);
        if (StringUtils.isEmpty(ordersInBatch)) {
            return 0;
        }
        return Integer.parseInt(ordersInBatch);
    }

    private void decrementBatchCount(Long batchId) {
        String ordersInBatch = redisTemplate.opsForValue().get("ORDERS_IN_BATCH_" + batchId);
        if (!StringUtils.isEmpty(ordersInBatch)) {
            redisTemplate.opsForValue().set("ORDERS_IN_BATCH_" + batchId, String.valueOf(Integer.parseInt(ordersInBatch) - 1));
        }

    }

    public boolean doesOrderExists(String orderId){
        return redisTemplate.hasKey(INCENTIVE_ORDER_PREFIX + orderId);

    }
}
