package com.swiggy.alchemist.services.evaluator;

import com.swiggy.alchemist.db.alchemist.entities.IncentiveRulesEntity;
import com.swiggy.alchemist.pojo.DeLogoutPojo;
import com.swiggy.alchemist.pojo.IncentiveOrderPojo;

import java.util.HashMap;
import java.util.List;

public interface EvaluatorService {
    boolean checkWeeklyRuleConditionExpression(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo, String weekForWeeklyPerformanceData);

    boolean checkDailyRuleConditionExpression(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo, String dateForDailyPerformanceData, List<Long> timeSlotsForRule);

    boolean checkMonthlyRuleConditionExpression(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo, String monthForMonthlyPerformanceData);

    Double calculateDailyBonusExpression(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo, String dateForDailyPerformanceData, List<Long> timeSlotsForRule);

    Double calculateWeeklyBonusExpression(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo, String weekForWeeklyPerformanceData);

    Double calculateMonthlyBonusExpression(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo, String monthForMonthlyPerformanceData);

    boolean checkAttendanceRuleCondition(IncentiveRulesEntity incentiveRule, DeLogoutPojo deLogoutPojo, HashMap aggregationData);

    Double calculateAttendanceBonus(IncentiveRulesEntity incentiveRule, DeLogoutPojo deLogoutPojo, HashMap aggregationData);

    boolean isValidExpression(String conditionExpression, Boolean isEquation);
}
