package com.swiggy.alchemist.services.evaluator;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.swiggy.alchemist.enums.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class ExpressionCalculationService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    private LoadingCache<String, String> guavaCache;

    @PostConstruct
    public void initialize(){
        guavaCache = CacheBuilder.newBuilder()
                .maximumSize(100)
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String key) throws Exception {
                        return null;
                    }
                });
    }

    private static final String MINUS = "-";
    private static final String PLUS = "+";
    private static final String DIVIDE = "/";
    private static final String MULTIPLY = "*";
    private static final String OPERATORS = "()*/+-";
    private static final String EMPTY_STRING = "";
    private static final String SPACE_REGEX = "\\s+";
    private static final String LEFT_PARENTHESIS = "(";
    private static final String RIGHT_PARENTHESIS = ")";
    private static final String ZERO = "0";


    public float calculateExpression(Object object, String expression) {
        float result = 0;
        try {
            result = compute(object, expression);
        } catch (IllegalAccessException | EmptyStackException | NoSuchFieldException e) {

        }
        return result;
    }


    private float compute(Object object, String expression) throws IllegalAccessException, NoSuchFieldException {
        expression = expression.replaceAll(SPACE_REGEX, EMPTY_STRING);
        if (StringUtils.isEmpty(expression) || StringUtils.isEmpty(expression.replaceAll(SPACE_REGEX, EMPTY_STRING))) {
            return 0;
        }

        boolean isNegativeValue = false;
        if (expression.charAt(0) == MINUS.charAt(0) && expression.charAt(1) == LEFT_PARENTHESIS.charAt(0) && expression.charAt(expression.length() - 1) == RIGHT_PARENTHESIS.charAt(0)) {
            isNegativeValue = true;
            expression = expression.substring(1, expression.length());
        }

        String postFixString = getFromCache(StringConstants.POSTFIX_KEY + expression);
        if (!StringUtils.isEmpty(postFixString)) {
            List<String> postfixExpression = getPostFixExpressionList(postFixString);
            float value = evaluatePostFix(postfixExpression, object);
            if (isNegativeValue) return value * (-1);
            else return value;
        }

        Stack<String> stack = new Stack<>();
        StringTokenizer tokens = new StringTokenizer(expression, OPERATORS, true);
        List<String> postFixExpression = new ArrayList<>();
        boolean isNegative = true;
        boolean addMinusToValue = false;
        while (tokens.hasMoreTokens()) {
            String token = tokens.nextToken();

            if (isNegative) {
                if (token.equals(MINUS)) {
                    isNegative = false;
                    addMinusToValue = true;
                    continue;
                }
                isNegative = false;
            }

            if (!isOperator(token) && !token.equals(LEFT_PARENTHESIS) && !token.equals(RIGHT_PARENTHESIS)) {
                if (addMinusToValue) {
                    postFixExpression.add(MINUS + token);
                    addMinusToValue = false;
                } else postFixExpression.add(token);
            }

            if (token.equals(LEFT_PARENTHESIS)) {
                stack.push(token);
                isNegative = true;
            }
            if (token.equals(RIGHT_PARENTHESIS)) {
                while (!stack.isEmpty() && !stack.peek().equals(LEFT_PARENTHESIS)) {
                    postFixExpression.add(stack.pop());
                }
                stack.pop(); //popping  "("
            }

            if (isOperator(token)) {
                if (stack.isEmpty() || stack.peek().equals(LEFT_PARENTHESIS)) {
                    stack.push(token);
                } else {
                    while (!stack.isEmpty() && !stack.peek().equals(LEFT_PARENTHESIS) && getPrecedence(token) <= getPrecedence(stack.peek())) {
                        postFixExpression.add(stack.pop());
                    }
                    stack.push(token);
                }
            }
        }
        while (!stack.isEmpty()) {
            postFixExpression.add(stack.pop());
        }
        guavaCache.put(StringConstants.POSTFIX_KEY + expression, getPostFixString(postFixExpression));
        redisTemplate.opsForValue().set(StringConstants.POSTFIX_KEY + expression, getPostFixString(postFixExpression));
        float value = evaluatePostFix(postFixExpression, object);
        if (isNegativeValue) return value * (-1);
        else return value;
    }

    private String getFromCache(String expression) {
        return guavaCache.getIfPresent(expression);
    }

    private String getPostFixString(List<String> postFixExpression) {
        StringBuilder stringBuilder = new StringBuilder(EMPTY_STRING);
        for (String s : postFixExpression) {
            stringBuilder.append(s).append(StringConstants.POSTFIX_DELIMITER);
        }
        return stringBuilder.toString();
    }

    private List<String> getPostFixExpressionList(String s) {
        StringTokenizer tokens = new StringTokenizer(s, StringConstants.POSTFIX_DELIMITER, false);
        List<String> postFixList = new ArrayList<>();
        while (tokens.hasMoreTokens()) {
            postFixList.add(tokens.nextToken());
        }
        return postFixList;
    }

    private boolean isOperator(String token) {
        return token.equals(MULTIPLY) || token.equals(DIVIDE) || token.equals(PLUS) || token.equals(MINUS);
    }

    private float evaluatePostFix(List<String> postFixList, Object object) throws IllegalAccessException, NoSuchFieldException {
        Map<String, String> fieldValueMap = getFieldValueMap(object);
        Stack<String> stack = new Stack<>();
        int i = 0;
        while (i < postFixList.size()) {
            if (!isOperator(postFixList.get(i))) {
                stack.push(postFixList.get(i));
            }
            if (isOperator(postFixList.get(i))) {
                float a = getValue(stack.pop(), fieldValueMap);
                float b = getValue(stack.pop(), fieldValueMap);
                float value = compute(b, postFixList.get(i), a);
                stack.push(String.valueOf(value));
            }
            i++;
        }

        return getValue(stack.pop(), fieldValueMap);
    }

    private Map<String, String> getFieldValueMap(Object object) {
        Map<String, String> objectMap = new HashMap<>();
        Field[] declaredFields = object.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            try {
                if (field.get(object) == null) {
                    objectMap.put(field.getName(), ZERO);
                }
                String value = String.valueOf(field.get(object));
                objectMap.put(field.getName().toLowerCase(), value);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return objectMap;
    }

    private float compute(float op1, String operator, float op2) {
        if (operator.equals(MULTIPLY))
            return (op1 * op2);
        else if (operator.equals(DIVIDE))
            return (op1 / op2);
        else if (operator.equals(PLUS))
            return (op1 + op2);
        else if (operator.equals(MINUS))
            return (op1 - op2);
        else
            return 0;
    }

    private int getPrecedence(String op) {
        switch (op) {
            case MULTIPLY:
            case DIVIDE:
                return 2;
            case PLUS:
            case MINUS:
                return 1;
            default:
                return Integer.MIN_VALUE;
        }
    }

    private float getValue(String variable, Map<String, String> objectMap) throws IllegalAccessException, NoSuchFieldException {
        try {
            return Float.parseFloat(variable);
        } catch (Exception e) {
            //do nothing. Its a variable.
        }
        int negative = 1;
        if (variable.charAt(0) == MINUS.charAt(0)) {
            negative = -1;
            variable = variable.substring(1, variable.length());
        }
        boolean variableFound = false;
        float value = 0;
        if (objectMap.containsKey(variable.toLowerCase())) {
            value = Float.parseFloat(objectMap.get(variable.toLowerCase()));
            variableFound = true;
        }

        if (!variableFound) {
            throw new NoSuchFieldException();
        }

        return value * negative;
    }
}
