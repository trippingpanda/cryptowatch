package com.swiggy.alchemist.services.evaluator;

import com.swiggy.alchemist.pojo.DeAppRulePojo;
import com.swiggy.alchemist.pojo.EventPojo;
import com.swiggy.alchemist.pojo.IncentiveOrderPojo;

import java.util.List;

/**
 * Created by abhishek.rawat on 9/28/16.
 */
public interface IRuleService {
    void computeRules(EventPojo eventPojo, IncentiveOrderPojo incentiveOrderPojo);

    List<DeAppRulePojo> computeRules(EventPojo eventPojo, IncentiveOrderPojo incentiveOrderPojo, boolean dryRun);

    List<DeAppRulePojo> dryRunRules(EventPojo eventPojo, IncentiveOrderPojo incentiveOrderPojo);
}
