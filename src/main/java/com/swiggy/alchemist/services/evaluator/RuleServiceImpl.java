package com.swiggy.alchemist.services.evaluator;

import com.swiggy.alchemist.dao.deliveryBoys.DeliveryBoysDao;
import com.swiggy.alchemist.dao.incentiveRules.IncentiveRulesDao;
import com.swiggy.alchemist.dao.timeslot.TimeSlotDao;
import com.swiggy.alchemist.db.alchemist.entities.IncentiveRulesEntity;
import com.swiggy.alchemist.enums.EventNames;
import com.swiggy.alchemist.pojo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abhishek.rawat on 9/28/16.
 */
@Service
@Slf4j
public class RuleServiceImpl implements IRuleService {

    @Autowired
    private IncentiveRulesHelper rulesHelper;

    @Autowired
    private DeliveryBoysDao deliveryBoysDao;

    @Autowired
    private TimeSlotDao timeSlotDao;

    @Autowired
    private IncentiveRulesDao incentiveRulesDao;

    @Override
    @Async
    public void computeRules(EventPojo eventPojo, IncentiveOrderPojo incentiveOrderPojo){
        computeRules(eventPojo,incentiveOrderPojo,false);
    }

    @Override
    @Async
    public List<DeAppRulePojo> dryRunRules(EventPojo eventPojo, IncentiveOrderPojo incentiveOrderPojo){
        return computeRules(eventPojo,incentiveOrderPojo,true);
    }


    @Override
    @Async
    public List<DeAppRulePojo> computeRules(EventPojo eventPojo, IncentiveOrderPojo incentiveOrderPojo, boolean dryRun) {
        log.info("compute rules");

        switch (EventNames.get(eventPojo.getEventName())) {
            case ORDER_STATUS_UPDATE:
                return computeRulesOnStatusUpdate((StatusUpdatePojo) eventPojo.getEventData(),incentiveOrderPojo,dryRun);
            case ORDER_REJECT:
                return computeRulesOnOrderReject((StatusUpdatePojo) eventPojo.getEventData(),incentiveOrderPojo,dryRun);
            case CANCELLED:
                computeRuleOnCancellation(incentiveOrderPojo);
                break;
            case LOGOUT:
                computeRuleOnLogout((DeLogoutPojo)eventPojo.getEventData());
                break;
            case END_OF_DAY:
                computeRuleOnEOD((EODPojo)eventPojo.getEventData());
                break;
            case END_OF_MONTH:
                computeRuleOnEOM((EOMPojo)eventPojo.getEventData());
                break;
            case END_OF_WEEK:
                computeRuleOnEOW((EOWPojo)eventPojo.getEventData());
                break;
            default:
        }
        return null;
    }

    private void computeRuleOnEOD(EODPojo eventData) {
        List<Long> deIds = deliveryBoysDao.getAllDeliveryBoys();
        deIds.forEach(x -> {
            List<Long> incentiveGroupId = deliveryBoysDao.getIncentiveGroupForDe(x);
            log.info("incentiveGroup: {} for de: {}",incentiveGroupId,x);
            long zoneId = deliveryBoysDao.getZoneForDe(x);
            log.info("zone: id {}, de: {}",zoneId,x);
            List<IncentiveRulesEntity> validRules = incentiveRulesDao.getRulesValidOnEOD(incentiveGroupId,zoneId);
            validRules.forEach(y -> log.info("valid rules for eod :{}", y.getId()));
            validRules.forEach(y -> rulesHelper.computeEODRules(y,eventData.getTime(),x));
        });
    }

    private void computeRuleOnEOW(EOWPojo eventData) {
        List<Long> deIds = deliveryBoysDao.getAllDeliveryBoys();
        deIds.forEach(x -> {
            List<Long> incentiveGroupId = deliveryBoysDao.getIncentiveGroupForDe(x);
            log.info("incentiveGroup: {} for de: {}",incentiveGroupId,x);
            long zoneId = deliveryBoysDao.getZoneForDe(x);
            log.info("zone: id {}, de: {}",zoneId,x);
            List<IncentiveRulesEntity> validRules = incentiveRulesDao.getRulesValidOnEOW(incentiveGroupId,zoneId);
            validRules.forEach(y -> log.info("valid rules for eow :{}", y.getId()));
            validRules.forEach(y -> rulesHelper.computeEOWRules(y,eventData.getTime(),x));
        });
    }

    private void computeRuleOnEOM(EOMPojo eventData) {
        List<Long> deIds = deliveryBoysDao.getAllDeliveryBoys();
        deIds.forEach(x -> {
            List<Long> incentiveGroupId = deliveryBoysDao.getIncentiveGroupForDe(x);
            log.info("incentiveGroup: {} for de: {}",incentiveGroupId,x);
            long zoneId = deliveryBoysDao.getZoneForDe(x);
            log.info("zone: id {}, de: {}",zoneId,x);
            List<IncentiveRulesEntity> validRules = incentiveRulesDao.getRulesValidOnEOM(incentiveGroupId,zoneId);
            validRules.forEach(y -> log.info("valid rules for eom :{}", y.getId()));
            validRules.forEach(y -> rulesHelper.computeEOMRules(y,eventData.getTime(),x));
        });
    }

    private void computeRuleOnLogout(DeLogoutPojo deLogoutPojo) {
        List<Long> incentiveGroupId = deliveryBoysDao.getIncentiveGroupForDe(deLogoutPojo.getDeId());
        log.info("incentiveGroup: {} for de: {}",incentiveGroupId,deLogoutPojo.getDeId());
        long zoneId = deliveryBoysDao.getZoneForDe(deLogoutPojo.getDeId());
        log.info("zone: id {}, de: {}",zoneId,deLogoutPojo.getDeId());
        List<IncentiveRulesEntity> validRules = incentiveRulesDao.getRulesValidOnLogout(incentiveGroupId,zoneId);
        validRules.forEach(x -> log.info("valid rules for logout :{}", x.getId()));
        validRules.forEach(x -> rulesHelper.computeAttendanceRules(x,deLogoutPojo));

    }

    private void computeRuleOnCancellation(IncentiveOrderPojo incentiveOrderPojo) {
        if(incentiveOrderPojo.getPickedUpTime() == 0){
            return;
        }
        List<Long> incentiveGroupId = deliveryBoysDao.getIncentiveGroupForDe(incentiveOrderPojo.getDeDetails().getId());
        log.info("incentiveGroup: {} for de: {}",incentiveGroupId,incentiveOrderPojo.getDeDetails().getId());
        long zoneId = deliveryBoysDao.getZoneForDe(incentiveOrderPojo.getDeDetails().getId());
        log.info("zone: id {}, de: {}",zoneId,incentiveOrderPojo.getDeDetails().getId());
        List<Long> validTimeSlots = timeSlotDao.getValidTimeSlots(incentiveOrderPojo.getAssignedTime());
        log.info("valid time slot for time : {} are {}",incentiveOrderPojo.getAssignedTime(),validTimeSlots);
        List<IncentiveRulesEntity> validRules = incentiveRulesDao.getRulesValidOnPickedUp(validTimeSlots,incentiveGroupId,zoneId);
        validRules.forEach(x -> log.info("valid rules cancellation:{}", x.getId()));
        validRules.forEach(x -> rulesHelper.computeAndPut(x,incentiveOrderPojo, validTimeSlots));
    }

    private List<DeAppRulePojo> computeRulesOnOrderReject(StatusUpdatePojo statusUpdatePojo, IncentiveOrderPojo incentiveOrderPojo,
                                           boolean dryRun) {
        List<Long> incentiveGroupId = deliveryBoysDao.getIncentiveGroupForDe(statusUpdatePojo.getDeliveryBoyId());
        List<DeAppRulePojo> dryRunRules = new ArrayList<>();
        log.info("incentiveGroup: {} for de: {}",incentiveGroupId,statusUpdatePojo.getDeliveryBoyId());
        long zoneId = deliveryBoysDao.getZoneForDe(incentiveOrderPojo.getDeDetails().getId());
        log.info("zone: id {}, de: {}",zoneId,statusUpdatePojo.getDeliveryBoyId());
        List<Long> validTimeSlots = timeSlotDao.getValidTimeSlots(incentiveOrderPojo.getAssignedTime());
        log.info("valid time slot for time : {} are {}",incentiveOrderPojo.getAssignedTime(),validTimeSlots);
        List<IncentiveRulesEntity> validRules = incentiveRulesDao.getRulesValidOnRejected(validTimeSlots,incentiveGroupId,zoneId);
        validRules.forEach(x -> log.info("valid rules reject :{}", x.getId()));
        for (IncentiveRulesEntity incentiveRulesEntity : validRules) {
            DeAppRulePojo deAppRulePojos = rulesHelper.computeAndPut(incentiveRulesEntity,incentiveOrderPojo, validTimeSlots, dryRun);
            if (dryRun && deAppRulePojos != null) {
                dryRunRules.add(deAppRulePojos);
            }
        }
        //validRules.forEach(x -> dryRunRules.add(rulesHelper.computeAndPut(x,incentiveOrderPojo,validTimeSlots,dryRun)));
        if(dryRun) {
            return  dryRunRules;
        }
        return null;
    }

    /**
     *
     * @param statusUpdatePojo
     * @param incentiveOrderPojo
     */
    private List<DeAppRulePojo> computeRulesOnStatusUpdate(StatusUpdatePojo statusUpdatePojo, IncentiveOrderPojo incentiveOrderPojo,
                                            boolean dryRun) {
        if(statusUpdatePojo.getStatus().equals("delivered")){
            List<Long> incentiveGroupId = deliveryBoysDao.getIncentiveGroupForDe(statusUpdatePojo.getDeliveryBoyId());
            List<DeAppRulePojo> dryRunRules = new ArrayList<>();
            log.info("incentiveGroup: {} for de: {}",incentiveGroupId,statusUpdatePojo.getDeliveryBoyId());
            long zoneId = deliveryBoysDao.getZoneForDe(statusUpdatePojo.getDeliveryBoyId());
            log.info("zone: id {}, de: {}",zoneId,statusUpdatePojo.getDeliveryBoyId());
            List<Long> validTimeSlots = timeSlotDao.getValidTimeSlots(incentiveOrderPojo.getAssignedTime());
            log.info("valid time slot for time : {} are {}",incentiveOrderPojo.getAssignedTime(),validTimeSlots);
            List<IncentiveRulesEntity> validRules = incentiveRulesDao.getRulesValidOnDelivered(validTimeSlots,incentiveGroupId,zoneId);
            validRules.forEach(x -> log.info("valid rules status update:{}", x.getId()));
            for (IncentiveRulesEntity incentiveRulesEntity : validRules) {
               DeAppRulePojo deAppRulePojos = rulesHelper.computeAndPut(incentiveRulesEntity,incentiveOrderPojo, validTimeSlots, dryRun);
                if (dryRun && deAppRulePojos != null) {
                    dryRunRules.add(deAppRulePojos);
                }
            }
            if(dryRun) {
                return  dryRunRules;
            }
        }
        return null;
    }

}
