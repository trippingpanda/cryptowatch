package com.swiggy.alchemist.services.evaluator;

import com.swiggy.alchemist.antlr.AntlrHelper;
import com.swiggy.alchemist.constants.EntityConstants;
import com.swiggy.alchemist.dao.dePayments.DePaymentsDao;
import com.swiggy.alchemist.dao.deliveryBoys.DeliveryBoysDao;
import com.swiggy.alchemist.dao.incentiveRules.IncentiveRulesDao;
import com.swiggy.alchemist.dao.performance.DePerformanceDao;
import com.swiggy.alchemist.dao.timeslot.TimeSlotDao;
import com.swiggy.alchemist.db.alchemist.entities.DeTimeslotsEntity;
import com.swiggy.alchemist.db.alchemist.entities.IncentiveRulesEntity;
import com.swiggy.alchemist.db.alchemist.repository.IncentiveRulesRepository;
import com.swiggy.alchemist.pojo.DeLogoutPojo;
import com.swiggy.alchemist.pojo.IncentiveOrderPojo;
import com.swiggy.alchemist.services.order.OrdersService;
import com.swiggy.alchemist.utility.IncentiveUtils;
import com.swiggy.alchemist.utility.SwiggyUtils;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Slf4j
public class EvaluatorServiceImpl implements EvaluatorService {

    @Autowired
    DePerformanceDao dePerformanceDao;

    @Autowired
    IncentiveRulesDao incentiveRulesDao;

    @Autowired
    TimeSlotDao timeSlotDao;

    @Autowired
    DeliveryBoysDao deliveryBoysDao;

    @Autowired
    DePaymentsDao dePaymentsDao;

    @Autowired
    OrdersService ordersService;

    @Autowired
    IncentiveRulesRepository incentiveRulesRepository;

    @Override
    public boolean checkWeeklyRuleConditionExpression(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo, String weekForWeeklyPerformanceData) {
        long deId = getDeId(incentiveOrderPojo,deLogoutPojo);
        String conditionExpression = incentiveRule.getExpression();
        Pattern p = Pattern.compile("(count)(\\()(\\w+)(\\))") ;
        Matcher m = p.matcher(conditionExpression);
        while (m.find()){
            int counts = getCountsInWeek(m.group(3),weekForWeeklyPerformanceData,deId);
            log.info("CONDITION replacing {} in {} to {}",m.group(3),conditionExpression,counts);
            conditionExpression = conditionExpression.replace(m.group(1) + m.group(2) + m.group(3) + m.group(4), String.valueOf(counts));
        }
        HashMap aggregationData = dePerformanceDao.getWeeklyPerformanceData(deId, weekForWeeklyPerformanceData);

        p = Pattern.compile("(\\w+)");
        m=p.matcher(conditionExpression);
        JSONObject expressionObject = new JSONObject();
        while (m.find()){
            expressionObject.put(m.group(1),getFieldValue(m.group(1),aggregationData,incentiveOrderPojo, incentiveRule, deLogoutPojo));
        }

        log.info("CONDITION evaluating equation {} for {}",conditionExpression,expressionObject);
        return AntlrHelper.evaluateEquation(conditionExpression, expressionObject) == 1;
    }

    private String getFieldValue(String keyName, HashMap aggregationData, IncentiveOrderPojo incentiveOrderPojo, IncentiveRulesEntity incentiveRule, DeLogoutPojo deLogoutPojo) {
        if(keyName.equals(EntityConstants.LOGGED_IN_TIME)){
            return String.valueOf(getLoggedInTime(deLogoutPojo,incentiveRule));
        }
        if(keyName.equals("tenure")){
            return deliveryBoysDao.getDeTenureInDays(getDeId(incentiveOrderPojo,deLogoutPojo));
        }
        if(aggregationData != null && aggregationData.containsKey(keyName))
            return String.valueOf(aggregationData.get(keyName));
        if(incentiveOrderPojo != null) {
            HashMap singleOrderData = SwiggyUtils.getHashMapFromJsonObject(SwiggyUtils.jsonEncode(incentiveOrderPojo));
            if (singleOrderData != null && singleOrderData.containsKey(keyName))
                return String.valueOf(singleOrderData.get(keyName));
        }
        return keyName;
    }



    private long getLoggedInTime(DeLogoutPojo deLogoutPojo, IncentiveRulesEntity incentiveRule) {
        List<Long> timeSlotsForRule = timeSlotDao.getTimeSlotsForRule(incentiveRule.getId());
        log.info("got timeslot  {} for login for rule : {}",timeSlotsForRule,incentiveRule.getId());
        long loggedInTime = 0;
        for(Long id : timeSlotsForRule){
            loggedInTime += getLoggedInTimeForTimeSlot(deLogoutPojo,id);
        }
        return loggedInTime;
    }

    private long getLoggedInTimeForTimeSlot(DeLogoutPojo deLogoutPojo, Long timeSlotId) {
        log.info("getting logged in time for  timeslot  {} for de : {}",timeSlotId,deLogoutPojo.getDeId());
        DeTimeslotsEntity timeSlot = timeSlotDao.getTimeSlotById(timeSlotId);
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(deLogoutPojo.getLogoutTime()), ZoneId.systemDefault());
        int weekday = localDateTime.getDayOfWeek().getValue();
        log.info("getting logged in time for  timeslot  {} for de : {}",timeSlotId,deLogoutPojo.getDeId());
        if(timeSlot.getWeekdays().contains(String.valueOf(weekday)))
            return deliveryBoysDao.getLoginTime(deLogoutPojo.getDeId(),IncentiveUtils.getTimeStampFromTimeSlot(timeSlot.getStartTime(),localDateTime),IncentiveUtils.getTimeStampFromTimeSlot(timeSlot.getEndTime(),localDateTime));
        log.info("invalid weekday for timeslot {}",timeSlotId);
        return 0;
    }

    @Override
    public boolean checkDailyRuleConditionExpression(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo, String dateForDailyPerformanceData, List<Long> timeSlotsForRule) {
        long deId = getDeId(incentiveOrderPojo,deLogoutPojo);
        String conditionExpression = incentiveRule.getExpression();
        Pattern p = Pattern.compile("(count)(\\()(\\w+)(\\))") ;
        Matcher m = p.matcher(conditionExpression);
        while (m.find()){
            int counts = getCountsInDay(m.group(3),dateForDailyPerformanceData,deId);
            log.info("replacing {} in {} to {}",m.group(3),conditionExpression,counts);
            conditionExpression = conditionExpression.replace(m.group(1) + m.group(2) + m.group(3) + m.group(4), String.valueOf(counts));
        }
        HashMap aggregationData = dePerformanceDao.getDailyPerformanceData(deId, dateForDailyPerformanceData,timeSlotsForRule);

        p = Pattern.compile("(\\w+)");
        m=p.matcher(conditionExpression);
        JSONObject expressionObject = new JSONObject();
        while (m.find()){
            expressionObject.put(m.group(1),getFieldValue(m.group(1),aggregationData,incentiveOrderPojo, incentiveRule, deLogoutPojo));
        }
        log.info("evaluating equation {} for {}",conditionExpression,expressionObject);
        return AntlrHelper.evaluateEquation(conditionExpression, expressionObject) == 1;
    }


    @Override
    public boolean checkMonthlyRuleConditionExpression(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo, String monthForMonthlyPerformanceData) {
        long deId = getDeId(incentiveOrderPojo,deLogoutPojo);
        String conditionExpression = incentiveRule.getExpression();
        Pattern p = Pattern.compile("(count)(\\()(\\w+)(\\))") ;
        Matcher m = p.matcher(conditionExpression);
        while (m.find()){
            int counts = getCountsInMonth(m.group(3),monthForMonthlyPerformanceData,deId);
            log.info("replacing {} in {} to {}",m.group(3),conditionExpression,counts);
            conditionExpression = conditionExpression.replace(m.group(1) + m.group(2) + m.group(3) + m.group(4), String.valueOf(counts));
        }

        HashMap aggregationData = dePerformanceDao.getMonthlyPerformanceData(deId, monthForMonthlyPerformanceData);

        p = Pattern.compile("(\\w+)");
        m=p.matcher(conditionExpression);
        JSONObject expressionObject = new JSONObject();
        while (m.find()){
            expressionObject.put(m.group(1),getFieldValue(m.group(1),aggregationData,incentiveOrderPojo, incentiveRule,deLogoutPojo));
        }
        log.info("evaluating equation {} for {}",conditionExpression,expressionObject);
        return AntlrHelper.evaluateEquation(conditionExpression, expressionObject) == 1;
    }

    @Override
    public Double calculateDailyBonusExpression(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo, String dateForDailyPerformanceData, List<Long> timeSlotsForRule) {
        long deId = getDeId(incentiveOrderPojo,deLogoutPojo);
        String bonusExpression = incentiveRule.getBonus();
        Pattern p = Pattern.compile("(count)(\\()(\\w+)(\\))") ;
        Matcher m = p.matcher(bonusExpression);
        while (m.find()){
            int counts = getCountsInDay(m.group(3),dateForDailyPerformanceData,deId);
            log.info("replacing {} in {} to {}",m.group(3),bonusExpression,counts);
            bonusExpression = bonusExpression.replace(m.group(1) + m.group(2) + m.group(3) + m.group(4), String.valueOf(counts));
        }
        p = Pattern.compile("(\\w+)");
        m = p.matcher(bonusExpression);


        HashMap aggregationData = dePerformanceDao.getDailyPerformanceData(deId, dateForDailyPerformanceData, timeSlotsForRule);

        JSONObject expressionObject = new JSONObject();
        while (m.find()){
            expressionObject.put(m.group(1),getFieldValue(m.group(1),aggregationData,incentiveOrderPojo,incentiveRule, deLogoutPojo));
        }
        log.info("evaluating bonus {} for {}",bonusExpression,expressionObject);
        return AntlrHelper.evaluate(bonusExpression,expressionObject);
    }

    @Override
    public Double calculateWeeklyBonusExpression(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo, String weekForWeeklyPerformanceData) {
        long deId = getDeId(incentiveOrderPojo,deLogoutPojo);
        String bonusExpression = incentiveRule.getBonus();
        Pattern p = Pattern.compile("(count)(\\()(\\w+)(\\))") ;
        Matcher m = p.matcher(bonusExpression);
        while (m.find()){
            int counts = getCountsInWeek(m.group(3),weekForWeeklyPerformanceData,deId);
            log.info("BONUS: replacing {} in {} to {}",m.group(3),bonusExpression,counts);
            bonusExpression = bonusExpression.replace(m.group(1) + m.group(2) + m.group(3) + m.group(4), String.valueOf(counts));
        }
        p = Pattern.compile("(\\w+)");
        m = p.matcher(bonusExpression);

        HashMap aggregationData = dePerformanceDao.getWeeklyPerformanceData(deId, weekForWeeklyPerformanceData);

        JSONObject expressionObject = new JSONObject();
        while (m.find()){
            expressionObject.put(m.group(1),getFieldValue(m.group(1),aggregationData,incentiveOrderPojo,incentiveRule,deLogoutPojo));
        }
        log.info("BONUS evaluating bonus {} for {}",bonusExpression,expressionObject);
        return AntlrHelper.evaluate(bonusExpression,expressionObject);
    }

    @Override
    public Double calculateMonthlyBonusExpression(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo, String monthForMonthlyPerformanceData) {
        long deId = getDeId(incentiveOrderPojo,deLogoutPojo);
        String bonusExpression = incentiveRule.getBonus();
        Pattern p = Pattern.compile("(count)(\\()(\\w+)(\\))") ;
        Matcher m = p.matcher(bonusExpression);
        while (m.find()){
            int counts = getCountsInMonth(m.group(3),monthForMonthlyPerformanceData,deId);
            log.info("replacing {} in {} to {}",m.group(3),bonusExpression,counts);
            bonusExpression = bonusExpression.replace(m.group(1) + m.group(2) + m.group(3) + m.group(4), String.valueOf(counts));
        }
        p = Pattern.compile("(\\w+)");
        m = p.matcher(bonusExpression);


        HashMap aggregationData = dePerformanceDao.getMonthlyPerformanceData(deId, monthForMonthlyPerformanceData);

        JSONObject expressionObject = new JSONObject();
        while (m.find()){
            expressionObject.put(m.group(1),getFieldValue(m.group(1),aggregationData,incentiveOrderPojo,incentiveRule, deLogoutPojo));
        }
        log.info("evaluating bonus {} for {}",bonusExpression,expressionObject);
        return AntlrHelper.evaluate(bonusExpression,expressionObject);
    }

    private long getDeId(IncentiveOrderPojo incentiveOrderPojo, DeLogoutPojo deLogoutPojo) {
        if(deLogoutPojo == null)
            return incentiveOrderPojo.getDeDetails().getId();
        return deLogoutPojo.getDeId();
    }

    @Override
    public boolean checkAttendanceRuleCondition(IncentiveRulesEntity incentiveRule, DeLogoutPojo deLogoutPojo, HashMap aggregationData) {
        String conditionExpression = incentiveRule.getExpression();
        Pattern p = Pattern.compile("(count)(\\()(\\w+)(\\))") ;
        Matcher m = p.matcher(conditionExpression);
        while (m.find()){
            int counts = getCountsInDay(m.group(3), IncentiveUtils.getDateForDailyPerformanceData(deLogoutPojo.getLogoutTime()),deLogoutPojo.getDeId());
            log.info("replacing {} in {} to {}",m.group(3),conditionExpression,counts);
            conditionExpression = conditionExpression.replace(m.group(1) + m.group(2) + m.group(3) + m.group(4), String.valueOf(counts));
        }

        p = Pattern.compile("(\\w+)");
        m=p.matcher(conditionExpression);
        JSONObject expressionObject = new JSONObject();
        while (m.find()){
            expressionObject.put(m.group(1),getFieldValue(m.group(1),aggregationData,null, incentiveRule,deLogoutPojo));
        }
        log.info("evaluating equation {} for {}",conditionExpression,expressionObject);
        return AntlrHelper.evaluateEquation(conditionExpression, expressionObject) == 1;
    }

    @Override
    public Double calculateAttendanceBonus(IncentiveRulesEntity incentiveRule, DeLogoutPojo deLogoutPojo, HashMap aggregationData) {
        String bonusExpression = incentiveRule.getBonus();
        Pattern p = Pattern.compile("(count)(\\()(\\w+)(\\))") ;
        Matcher m = p.matcher(bonusExpression);
        while (m.find()){
            int counts = getCountsInDay(m.group(3), IncentiveUtils.getDateForDailyPerformanceData(deLogoutPojo.getLogoutTime()),deLogoutPojo.getDeId());
            log.info("replacing {} in {} to {}",m.group(3),bonusExpression,counts);
            bonusExpression = bonusExpression.replace(m.group(1) + m.group(2) + m.group(3) + m.group(4), String.valueOf(counts));
        }
        p = Pattern.compile("(\\w+)");
        m = p.matcher(bonusExpression);

        JSONObject expressionObject = new JSONObject();
        while (m.find()){
            expressionObject.put(m.group(1),getFieldValue(m.group(1),aggregationData,null,incentiveRule, deLogoutPojo));
        }
        log.info("evaluating bonus {} for {}",bonusExpression,expressionObject);
        return AntlrHelper.evaluate(bonusExpression,expressionObject);
    }

    private int getCountsInMonth(String ruleName, String monthForMonthlyPerformanceData, long deId) {
        IncentiveRulesEntity rulesEntity = incentiveRulesDao.getRuleByName(ruleName);

        if(rulesEntity.getScope().equals(EntityConstants.SCOPE_WEEK)){
            return dePaymentsDao.getRuleCountInWeekForMonthScope(ruleName,monthForMonthlyPerformanceData,deId);
        }

        if(rulesEntity.getScope().equals(EntityConstants.SCOPE_DAY)){
            return dePaymentsDao.getRuleCountInDaysForMonthScope(ruleName, monthForMonthlyPerformanceData,deId);
        }

        return 0;
    }

    private int getCountsInWeek(String ruleName, String weekForWeeklyPerformanceData, long deId) {
        IncentiveRulesEntity rulesEntity = incentiveRulesDao.getRuleByName(ruleName);
        if(rulesEntity.getScope().equals(EntityConstants.SCOPE_DAY)){
            return dePaymentsDao.getRuleCountInDaysForWeekScope(ruleName,weekForWeeklyPerformanceData,deId);
        }
        return 0;
    }

    private int getCountsInDay(String ruleName, String dateForDailyPerformanceData, long deId) {
        return dePaymentsDao.getRuleCountInDay(ruleName, dateForDailyPerformanceData,deId);
    }


    public boolean isValidExpression(String conditionExpression, Boolean isEquation) {
        Pattern p = Pattern.compile("(count)(\\()(\\w+)(\\))") ;
        Matcher m = p.matcher(conditionExpression);
        while (m.find()){
            log.info("matcher");
            int counts = 1;
            if(!checkIfRuleExists(m.group(3))){
                log.info("Rule does not exist");
                return false;
            }
            log.info("replacing {} in {} to {}",m.group(3),conditionExpression,counts);
            conditionExpression = conditionExpression.replace(m.group(1) + m.group(2) + m.group(3) + m.group(4), String.valueOf(counts));
        }
        log.info("condition expressions {}", conditionExpression);
        if(isEquation) {
            return AntlrHelper.evaluateEquation(conditionExpression, new JSONObject()) != null;
        } else {
            return AntlrHelper.evaluate(conditionExpression, new JSONObject()) != null;
        }
    }

    private boolean checkIfRuleExists(String ruleName){
        IncentiveRulesEntity incentiveRulesEntity = incentiveRulesRepository.findByRuleName(ruleName);
        return null != incentiveRulesEntity;
    }

}
