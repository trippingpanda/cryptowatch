package com.swiggy.alchemist.services.evaluator;

import com.swiggy.alchemist.constants.EntityConstants;
import com.swiggy.alchemist.dao.dePayments.DePaymentsDao;
import com.swiggy.alchemist.dao.performance.DePerformanceDao;
import com.swiggy.alchemist.dao.timeslot.TimeSlotDao;
import com.swiggy.alchemist.db.alchemist.entities.DeRuleMapDailyEntity;
import com.swiggy.alchemist.db.alchemist.entities.DeRuleMapMonthlyEntity;
import com.swiggy.alchemist.db.alchemist.entities.DeRuleMapWeeklyEntity;
import com.swiggy.alchemist.db.alchemist.entities.IncentiveRulesEntity;
import com.swiggy.alchemist.db.alchemist.repository.DeRuleMapDailyRepository;
import com.swiggy.alchemist.db.alchemist.repository.DeRuleMapMonthlyRepository;
import com.swiggy.alchemist.db.alchemist.repository.DeRuleMapWeeklyRepository;
import com.swiggy.alchemist.pojo.DeAppRulePojo;
import com.swiggy.alchemist.pojo.DeLogoutPojo;
import com.swiggy.alchemist.pojo.IncentiveOrderPojo;
import com.swiggy.alchemist.utility.IncentiveUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

@Service
@Transactional
@Slf4j
public class IncentiveRulesHelper {
    @Autowired
    private DePaymentsDao dePaymentsDao;

    @Autowired
    private DeRuleMapDailyRepository deRuleMapDailyRepository;

    @Autowired
    private DeRuleMapWeeklyRepository deRuleMapWeeklyRepository;

    @Autowired
    private DeRuleMapMonthlyRepository deRuleMapMonthlyRepository;

    @Autowired
    private EvaluatorService evaluatorService;

    @Autowired
    private DePerformanceDao dePerformanceDao;

    @Autowired
    private TimeSlotDao timeSlotDao;

    public void computeAndPut(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo,
                              List<Long> validTimeSlots){
         computeAndPut(incentiveRule,incentiveOrderPojo,validTimeSlots,false);
    }

    public DeAppRulePojo computeAndPut(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, List<Long> validTimeSlots, boolean dryRun) {
        if(incentiveRule == null){
            return null ;
        }
        log.info("compute out : {}",incentiveRule.getId());
        List<Long> timeSlotsForRule = timeSlotDao.getTimeSlotsForRule(incentiveRule.getId());
        timeSlotsForRule.retainAll(validTimeSlots);

        if(StringUtils.isEmpty(incentiveRule.getScope())){
            return computeOtherRules(incentiveRule,incentiveOrderPojo,timeSlotsForRule,dryRun);
        }

        switch (incentiveRule.getScope().toLowerCase()){
            case EntityConstants.SCOPE_DAY:
                computeDailyRules(incentiveRule,incentiveOrderPojo,timeSlotsForRule);
                break;
            case EntityConstants.SCOPE_WEEK:
                computeWeeklyRules(incentiveRule,incentiveOrderPojo);
                break;
            case EntityConstants.SCOPE_MONTH:
                computeMonthlyRules(incentiveRule,incentiveOrderPojo);
                break;
            default:
                return computeOtherRules(incentiveRule,incentiveOrderPojo,timeSlotsForRule,dryRun);
        }
        return null;

    }

    /**
     * Each Order Rule.
     * @param incentiveRule
     * @param incentiveOrderPojo
     * @param timeSlotsForRule
     */
    private DeAppRulePojo computeOtherRules(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, List<Long> timeSlotsForRule, boolean dryRun) {
        log.info("computing rule {} for de {} for orderId {} ",incentiveRule.getId(),incentiveOrderPojo.getOrderId());
        String dateForDailyPerformanceData = IncentiveUtils.getDateForDailyPerformanceData(incentiveOrderPojo.getAssignedTime());
        DeRuleMapDailyEntity deRuleMapDailyEntity = deRuleMapDailyRepository.findByRuleIdDeIdAndCreatedDate(incentiveRule.getId(), incentiveOrderPojo.getDeDetails().getId(), dateForDailyPerformanceData);

        if(deRuleMapDailyEntity == null && evaluatorService.checkDailyRuleConditionExpression(incentiveRule,incentiveOrderPojo,null, dateForDailyPerformanceData, timeSlotsForRule)){
            log.info("new daily entry for de {},rule {}",incentiveOrderPojo.getDeDetails().getId(),incentiveRule.getId());
            Double bonusAmount = evaluatorService.calculateDailyBonusExpression(incentiveRule, incentiveOrderPojo, null, dateForDailyPerformanceData, timeSlotsForRule);
            log.info("bonus for rule {} to de {} on {}",incentiveRule.getId(),incentiveOrderPojo.getDeDetails(),dateForDailyPerformanceData);
            if(!dryRun) {
                dePaymentsDao.updateDailyIncentiveBonusPayment(incentiveOrderPojo.getDeDetails().getId(), incentiveRule, bonusAmount, dateForDailyPerformanceData, false, incentiveOrderPojo.getOrderId());
                return null;
            } else {
                return new DeAppRulePojo(incentiveRule.getRuleName(),String.valueOf(bonusAmount));
            }
        }

        if(evaluatorService.checkDailyRuleConditionExpression(incentiveRule,incentiveOrderPojo, null, dateForDailyPerformanceData, timeSlotsForRule)){
            log.info("updating existing entry {}",deRuleMapDailyEntity.getCreatedOn());
            Double bonusAmount = evaluatorService.calculateDailyBonusExpression(incentiveRule, incentiveOrderPojo, null, dateForDailyPerformanceData, timeSlotsForRule);
            if(!dryRun) {
            dePaymentsDao.updateRecurringBonusPayment(deRuleMapDailyEntity,bonusAmount,incentiveOrderPojo.getOrderId());
                return null;
            } else {
                return new DeAppRulePojo(incentiveRule.getRuleName(),String.valueOf(bonusAmount));
            }
        }
        return null;
    }

    /**
     * Rule with monthly scope
     *
     * @param incentiveRule
     * @param incentiveOrderPojo
     */
    private void computeMonthlyRules(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo) {
        log.info("computing rule {} for de {} for orderId {} ",incentiveRule.getId(),incentiveOrderPojo.getOrderId());
        String monthForMonthlyPerformanceData = IncentiveUtils.getMonthForMonthlyPerformanceData(incentiveOrderPojo.getAssignedTime());
        DeRuleMapMonthlyEntity deRuleMapMonthlyEntity = deRuleMapMonthlyRepository.findByRuleIdDeIdAndCreatedDate(incentiveRule.getId(), incentiveOrderPojo.getDeDetails().getId(), monthForMonthlyPerformanceData);

        if(deRuleMapMonthlyEntity == null && evaluatorService.checkMonthlyRuleConditionExpression(incentiveRule,incentiveOrderPojo, null, monthForMonthlyPerformanceData)){
            log.info("new monthly entry for de {},rule {}",incentiveOrderPojo.getDeDetails().getId(),incentiveRule.getId());
            Double bonusAmount = evaluatorService.calculateMonthlyBonusExpression(incentiveRule, incentiveOrderPojo, null, monthForMonthlyPerformanceData);
            log.info("bonus {} for rule {} to de {} on {}",bonusAmount,incentiveRule.getId(),incentiveOrderPojo.getDeDetails(),monthForMonthlyPerformanceData);
            dePaymentsDao.updateMonthlyIncentiveBonusPayment(incentiveOrderPojo.getDeDetails().getId(),incentiveRule,bonusAmount,monthForMonthlyPerformanceData);
        }
    }


    /**
     * Rule with weekly scope.
     * @param incentiveRule
     * @param incentiveOrderPojo
     */
    private void computeWeeklyRules(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo) {
        log.info("computing rule {} for de {} for orderId {} ",incentiveRule.getId(),incentiveOrderPojo.getOrderId());
        String weekForWeeklyPerformanceData = IncentiveUtils.getWeekForWeeklyPerformanceData(incentiveOrderPojo.getAssignedTime());
        DeRuleMapWeeklyEntity deRuleMapWeeklyEntity = deRuleMapWeeklyRepository.findByRuleIdDeIdAndCreatedDate(incentiveRule.getId(), incentiveOrderPojo.getDeDetails().getId(), weekForWeeklyPerformanceData);

        if(deRuleMapWeeklyEntity == null && evaluatorService.checkWeeklyRuleConditionExpression(incentiveRule,incentiveOrderPojo, null, weekForWeeklyPerformanceData)){
            log.info("new monthly entry for de {},rule {}",incentiveOrderPojo.getDeDetails().getId(),incentiveRule.getId());
            Double bonusAmount = evaluatorService.calculateWeeklyBonusExpression(incentiveRule, incentiveOrderPojo, null, weekForWeeklyPerformanceData);
            log.info("bonus {} for rule {} to de {} on {}",bonusAmount,incentiveRule.getId(),incentiveOrderPojo.getDeDetails(),weekForWeeklyPerformanceData);
            dePaymentsDao.updateWeeklyIncentiveBonusPayment(incentiveOrderPojo.getDeDetails().getId(),incentiveRule,bonusAmount,weekForWeeklyPerformanceData);
        }
    }

    /**
     * Rule with daily scope.
     * @param incentiveRule
     * @param incentiveOrderPojo
     * @param timeSlotsForRule
     */
    private void computeDailyRules(IncentiveRulesEntity incentiveRule, IncentiveOrderPojo incentiveOrderPojo, List<Long> timeSlotsForRule) {
        log.info("computing rule {} for de {} for orderId {} ",incentiveRule.getId(),incentiveOrderPojo.getOrderId());
        String dateForDailyPerformanceData = IncentiveUtils.getDateForDailyPerformanceData(incentiveOrderPojo.getAssignedTime());
        DeRuleMapDailyEntity deRuleMapDailyEntity = deRuleMapDailyRepository.findByRuleIdDeIdAndCreatedDate(incentiveRule.getId(), incentiveOrderPojo.getDeDetails().getId(), dateForDailyPerformanceData);

        if(deRuleMapDailyEntity == null && evaluatorService.checkDailyRuleConditionExpression(incentiveRule,incentiveOrderPojo, null, dateForDailyPerformanceData,timeSlotsForRule)){
            log.info("new monthly entry for de {},rule {}",incentiveOrderPojo.getDeDetails().getId(),incentiveRule.getId());
            Double bonusAmount = evaluatorService.calculateDailyBonusExpression(incentiveRule, incentiveOrderPojo, null, dateForDailyPerformanceData,timeSlotsForRule);
            log.info("bonus {} for rule {} to de {} on {}",bonusAmount,incentiveRule.getId(),incentiveOrderPojo.getDeDetails(),dateForDailyPerformanceData);
            dePaymentsDao.updateDailyIncentiveBonusPayment(incentiveOrderPojo.getDeDetails().getId(),incentiveRule,bonusAmount,dateForDailyPerformanceData, true, incentiveOrderPojo.getOrderId());
        }
    }

    public void computeAttendanceRules(IncentiveRulesEntity incentiveRule, DeLogoutPojo deLogoutPojo) {
        String dateForDailyPerformanceData = IncentiveUtils.getDateForDailyPerformanceData(deLogoutPojo.getLogoutTime());
        log.info("computing attendance rule {} for de {} ",incentiveRule.getId(),deLogoutPojo.getDeId());
        DeRuleMapDailyEntity deRuleMapDailyEntity = deRuleMapDailyRepository.findByRuleIdDeIdAndCreatedDate(incentiveRule.getId(), deLogoutPojo.getDeId(), dateForDailyPerformanceData);
        List<Long> timeSlotsForRule = timeSlotDao.getTimeSlotsForRule(incentiveRule.getId());
        log.info("timeslots for rule {}: are : {}",incentiveRule.getId(),timeSlotsForRule);
        HashMap aggregationData = dePerformanceDao.getDailyPerformanceData(deLogoutPojo.getDeId(),dateForDailyPerformanceData,timeSlotsForRule);
        log.info("aggregation data for logout rule : {} is {}",incentiveRule.getId(),aggregationData);
        if(deRuleMapDailyEntity == null &&  evaluatorService.checkAttendanceRuleCondition(incentiveRule,deLogoutPojo,aggregationData)){
            Double bonus   = evaluatorService.calculateAttendanceBonus(incentiveRule,deLogoutPojo,aggregationData);
            log.info("bonus {} for rule {} to de {} on {}",bonus,incentiveRule.getId(),deLogoutPojo,IncentiveUtils.getDateForDailyPerformanceData(deLogoutPojo.getLogoutTime()));
            dePaymentsDao.updateDailyIncentiveBonusPayment(deLogoutPojo.getDeId(),incentiveRule,bonus,IncentiveUtils.getDateForDailyPerformanceData(deLogoutPojo.getLogoutTime()), true, null);
        }
    }

    public void computeEODRules(IncentiveRulesEntity incentiveRule, long time, Long deId) {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault());
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,localDateTime.getYear());
        cal.set(Calendar.DAY_OF_MONTH,localDateTime.getDayOfMonth()-1);
        cal.set(Calendar.MONTH,localDateTime.getMonthValue()-1);
        cal.set(Calendar.HOUR_OF_DAY,23);
        cal.set(Calendar.MINUTE,59);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);
        String dateForDailyPerformanceData = IncentiveUtils.getDateForDailyPerformanceData(cal.getTimeInMillis());
        log.info("computing attendance rule {} for de {} for date String {}",incentiveRule.getId(),deId,dateForDailyPerformanceData);
        DeRuleMapDailyEntity deRuleMapDailyEntity = deRuleMapDailyRepository.findByRuleIdDeIdAndCreatedDate(incentiveRule.getId(),deId, dateForDailyPerformanceData);
        List<Long> timeSlotsForRule = timeSlotDao.getTimeSlotsForRule(incentiveRule.getId());
        log.info("timeslots for rule {}: are : {}",incentiveRule.getId(),timeSlotsForRule);
        if(deRuleMapDailyEntity == null &&  evaluatorService.checkDailyRuleConditionExpression(incentiveRule,null,getDeLogoutPojo(deId,cal.getTimeInMillis()),dateForDailyPerformanceData,timeSlotsForRule)){
            Double bonus   = evaluatorService.calculateDailyBonusExpression(incentiveRule,null,getDeLogoutPojo(deId,cal.getTimeInMillis()),dateForDailyPerformanceData,timeSlotsForRule);
            log.info("eod bonus {} for rule {} to de {} on {}",bonus,incentiveRule.getId(),deId,dateForDailyPerformanceData);
            dePaymentsDao.updateDailyIncentiveBonusPayment(deId,incentiveRule,bonus,dateForDailyPerformanceData, true, null);
        }
    }

    public void computeEOWRules(IncentiveRulesEntity incentiveRule, long time, Long deId) {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault());
        log.info("day:{} , month:{}",localDateTime.getDayOfMonth(),localDateTime.getMonthValue());
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,localDateTime.getYear());
        cal.set(Calendar.DAY_OF_MONTH,localDateTime.getDayOfMonth()-7);
        cal.set(Calendar.MONTH,localDateTime.getMonthValue()-1);
        cal.set(Calendar.HOUR_OF_DAY,23);
        cal.set(Calendar.MINUTE,59);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);

        String weekForWeeklyPerformanceData = IncentiveUtils.getWeekForWeeklyPerformanceData(cal.getTimeInMillis());
        log.info("computing attendance rule {} for de {}  week string {}",incentiveRule.getId(),deId , weekForWeeklyPerformanceData);
        DeRuleMapWeeklyEntity deRuleMapWeeklyEntity = deRuleMapWeeklyRepository.findByRuleIdDeIdAndCreatedDate(incentiveRule.getId(), deId, weekForWeeklyPerformanceData);
        if(deRuleMapWeeklyEntity == null &&  evaluatorService.checkWeeklyRuleConditionExpression(incentiveRule,null,getDeLogoutPojo(deId,cal.getTimeInMillis()),weekForWeeklyPerformanceData)){
            Double bonus   = evaluatorService.calculateWeeklyBonusExpression(incentiveRule,null,getDeLogoutPojo(deId,cal.getTimeInMillis()),weekForWeeklyPerformanceData);
            log.info("eow bonus {} for rule {} to de {} on {}",bonus,incentiveRule.getId(),deId,weekForWeeklyPerformanceData);
            dePaymentsDao.updateWeeklyIncentiveBonusPayment(deId,incentiveRule,bonus,weekForWeeklyPerformanceData);
        }
    }

    private DeLogoutPojo getDeLogoutPojo(Long deId,long time) {
        return DeLogoutPojo.builder().deId(deId).logoutTime(time).build();
    }

    public void computeEOMRules(IncentiveRulesEntity incentiveRule, long time, Long deId) {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault());
        log.info("day:{} , month:{}",localDateTime.getDayOfMonth(),localDateTime.getMonthValue());
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,localDateTime.getYear());
        cal.set(Calendar.DAY_OF_MONTH,localDateTime.getDayOfMonth());
        cal.set(Calendar.MONTH,localDateTime.getMonthValue()-2);
        cal.set(Calendar.HOUR_OF_DAY,23);
        cal.set(Calendar.MINUTE,59);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);
        String monthForMonthlyPerformanceData = IncentiveUtils.getMonthForMonthlyPerformanceData(cal.getTimeInMillis());
        log.info("computing attendance rule {} for de {} for month String ",incentiveRule.getId(),deId,monthForMonthlyPerformanceData);
        DeRuleMapMonthlyEntity deRuleMapMonthlyEntity = deRuleMapMonthlyRepository.findByRuleIdDeIdAndCreatedDate(incentiveRule.getId(), deId, monthForMonthlyPerformanceData);
        if(deRuleMapMonthlyEntity == null &&  evaluatorService.checkMonthlyRuleConditionExpression(incentiveRule,null,getDeLogoutPojo(deId,cal.getTimeInMillis()),monthForMonthlyPerformanceData)){
            Double bonus   = evaluatorService.calculateMonthlyBonusExpression(incentiveRule,null,getDeLogoutPojo(deId,cal.getTimeInMillis()),monthForMonthlyPerformanceData);
            log.info("eom bonus {} for rule {} to de {} on {}",bonus,incentiveRule.getId(),deId,monthForMonthlyPerformanceData);
            dePaymentsDao.updateMonthlyIncentiveBonusPayment(deId,incentiveRule,bonus,monthForMonthlyPerformanceData);
        }
    }

}
