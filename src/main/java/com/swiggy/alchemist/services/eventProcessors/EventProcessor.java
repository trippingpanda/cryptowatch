package com.swiggy.alchemist.services.eventProcessors;

/**
 * Created by abhishek.rawat on 9/28/16.
 */

import com.swiggy.alchemist.aspects.LockDE;
import com.swiggy.alchemist.pojo.*;
import com.swiggy.alchemist.services.aggregation.IDataAggregationService;
import com.swiggy.alchemist.services.dryRun.DryRunService;
import com.swiggy.alchemist.services.evaluator.IRuleService;
import com.swiggy.alchemist.services.order.OrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
@Slf4j
public class EventProcessor implements IEventProcessor {

    @Autowired
    OrdersService ordersService;

    @Autowired
    IDataAggregationService dataAggregationService;

    @Autowired
    IRuleService ruleService;

    @Autowired
    private DryRunService dryRunService;

    @Override
    @LockDE
    public void processEvent(EventPojo eventPojo,Long deId) {
        log.info("processing event");
        IncentiveOrderPojo incentiveOrderPojo = ordersService.cacheOrder(eventPojo);
        dataAggregationService.aggregateData(incentiveOrderPojo,eventPojo.getEventName());
        ruleService.computeRules(eventPojo,incentiveOrderPojo);
        dryRunService.processDryRun(eventPojo, incentiveOrderPojo);
    }



    @Override
    public void processEvent(EventPojo eventPojo) {
        processEvent(eventPojo,null);
    }

    @Override
    public void processPendingEvents(EventPojo eventPojo) {
        processEvent(eventPojo);
    }





}
