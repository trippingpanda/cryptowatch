package com.swiggy.alchemist.services.eventProcessors;

import com.swiggy.alchemist.pojo.EventPojo;

public interface IEventProcessor {
    void processEvent(EventPojo eventPojo,Long deId);

    void processEvent(EventPojo eventPojo);

    void processPendingEvents(EventPojo eventPojo);
}
