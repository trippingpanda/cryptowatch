package com.swiggy.alchemist.services.caching;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.swiggy.alchemist.db.alchemist.entities.IncentiveConfigEntity;
import com.swiggy.alchemist.db.alchemist.repository.IncentiveConfigRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;



@Component
public class OptionCache {
    private final LoadingCache<String, OptionMap> cache;

    private static final String optionKey = "all_options";
    private static final int expiryTime = -1;
    private static final long GUAVA_EXPIRY_TIME = 120;
    private static final Logger logger = LoggerFactory
            .getLogger(OptionCache.class);

    @Autowired
    CacheService<HashMap<String,String>> optionTemplate;

    @Autowired
    IncentiveConfigRepository incentiveConfigRepository;

    public OptionCache() {
        cache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(GUAVA_EXPIRY_TIME, TimeUnit.SECONDS)
                .<String, OptionMap>build(new CacheLoader<String, OptionMap>() {
                    public OptionMap load(String k1) throws Exception {
                        logger.info("In guava loader");
                        //Thread.sleep(30);   //todo check if other requesst will load guava cache if current thread is loading, else make the method synchronize
                        return loadCache();
                    }
                });
    }

    public OptionMap getEntry( String productId ) {
        return (OptionMap)cache.getUnchecked(  optionKey);
    }

    private OptionMap loadCache() {
        return new OptionMap(loadFromRedis());
    }



    public HashMap<String,String> loadFromRedis(){
        logger.info("getting from redis");
        HashMap<String,String> options = optionTemplate.get(optionKey,HashMap.class);
        if(options == null || options.size() ==0) {
            List<IncentiveConfigEntity> incentiveConfigs = incentiveConfigRepository.findAll();
            options = new HashMap<>();
            for(IncentiveConfigEntity optionsEntity : incentiveConfigs){
                options.put(optionsEntity.getMetaKey(),optionsEntity.getMetaValue());
            }
            optionTemplate.set(optionKey,options,expiryTime);
        }
        return options;
    }
}
