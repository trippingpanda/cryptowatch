package com.swiggy.alchemist.services.caching;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.EnumSet;


/**
 * Created by atulagrawal1 on 20/10/15.
 */
public enum Cache {

    DEFAULT, STRING;


    @Component
    public static class CacheInjector {
        @Autowired private CacheService<Object> defaultCacheService;
        @Autowired private CacheService<String> stringCacheService;



        @PostConstruct
        public  void postConstruct(){
            for(Cache c : EnumSet.allOf(Cache.class)){
                switch (c){
                    case STRING:
                        c.setCacheService(stringCacheService);
                        break;
                    case DEFAULT:
                        c.setCacheService(defaultCacheService);
                        break;
                }
            }
        }
    }

    private CacheService cacheService;

    private void setCacheService(CacheService c){
        cacheService = c;
    }

    public CacheService getCacheService() {
        return cacheService;
    }



}
