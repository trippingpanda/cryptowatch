package com.swiggy.alchemist.services.caching;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class RedisLock {

    @Autowired private StringRedisTemplate stringRedisTemplate;
    private static final TimeUnit cacheTimeoutUnit;
    private static final String LOCK = "LOCK_";

    static
    {
        cacheTimeoutUnit=TimeUnit.SECONDS;
    }

    public boolean getLock(String resourceName, String value, long expireTime){

        boolean isSet = stringRedisTemplate.opsForValue().setIfAbsent(resourceName, value);
        if (isSet)
            stringRedisTemplate.expire(resourceName, expireTime, cacheTimeoutUnit);
        return isSet;
    }

    public void deleteLock(String key){
        String lockKey = LOCK + key;
        stringRedisTemplate.delete(lockKey);
    }

    public boolean getLock(String key, long expireTime) {
        String lockKey = LOCK + key;
        return getLock(lockKey, "1", expireTime);
    }

    public boolean getLock(String key) {
        String lockKey = LOCK + key;
        return stringRedisTemplate.opsForValue().setIfAbsent(lockKey, "1");
    }

}
