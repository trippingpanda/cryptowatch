package com.swiggy.alchemist.services.caching;

import java.util.HashMap;


public class OptionMap {
    private HashMap<String,String> options;
    public OptionMap(HashMap<String, String> options){
        this.options = options;
    }
    public String getOption(String key){
        if(options.containsKey(key))
            return options.get(key);
        return null;
    }
}
