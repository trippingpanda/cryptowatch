package com.swiggy.alchemist.services.caching;

import com.swiggy.alchemist.utility.SwiggyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by IntelliJ IDEA.
 * User: bothravinit
 * Date: 8/10/15
 * Time: 1:51 PM
 */
@Service
@Primary
@Slf4j
public class CacheService<K> {

    @Autowired
    private RedisTemplate<String, String> stringTemplate;

    public void set(String key, K obj, int expiry) {
        log.info("setting value in redis for key {} with object {} for expiry: {} in seconds",key, String.valueOf(obj),expiry);
        String value= "";
        StringBuffer stringBuffer = new StringBuffer();
        log.info("type of object: {}",obj.getClass());
        if(obj instanceof ArrayList) {
           // stringBuffer.append("[");
            int size = ((ArrayList) obj).size();
            for (int i=0 ;i<size ; i++){
                Object[] object = ((ArrayList<Object[]>) obj).get(i);
                stringBuffer.append(Arrays.deepToString(object));
                if(i< size-1) {
                    stringBuffer.append("$");
                }
            }

            //stringBuffer.append("]");
            value= stringBuffer.toString();
            log.info("value for list: {}",value);
            stringTemplate.opsForValue().set(key,value,expiry,TimeUnit.SECONDS);
        } else {
            stringTemplate.opsForValue().set(key, obj.toString(), expiry, TimeUnit.SECONDS);
        }
    }

    public String get(String key) {
        return stringTemplate.opsForValue().get(key);
    }

    public K get(String key, Class K) {
        String s ="";
        s = get(key);
        log.info("value from cache:{}  key:{}" ,s,key);
        log.info("class required: {}", K );
        if (!StringUtils.isEmpty(s)&& K == List.class) {
            List<Object[]> result = new ArrayList<Object[]>();
            for (String str : Arrays.asList(s.split("\\$"))){
                String subStr = str.substring(1,str.length()-1);
                log.info("subStr: {}", str, subStr);
                String[] strings = subStr.split(",");
                for(int i=7;i<strings.length;i++){
                    strings[6] +=","+ strings[i];
                }
                result.add(strings);
            }
            return (K)result;
        }
        return  (K) SwiggyUtils.jsonDecode(s, K);
    }
}

