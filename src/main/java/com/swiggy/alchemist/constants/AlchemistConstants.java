package com.swiggy.alchemist.constants;

/**
 * Created by RAVISINGH on 10/10/16.
 */
public class AlchemistConstants {
    public static final int NAME_LENGTH_LIMIT = 70;

    public static final int DESCRIPTION_LENGTH_LIMIT = 200;

    public static final int STATUS_MESSAGE_LIMIT = 200;
}
