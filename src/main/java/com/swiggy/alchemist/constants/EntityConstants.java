package com.swiggy.alchemist.constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVISINGH on 22/09/16.
 */
public class EntityConstants {

    public static String NAME = "NAME";

    public static String DESCRIPTION = "DESCRIPTION";

    public static String FOR = "FOR";

    public static String BONUS = "BONUS";

    public static String CONDITION = "CONDITION";

    public static final String SCOPE_DAY = "day";

    public static final String SCOPE_WEEK = "week";

    public static final String SCOPE_MONTH = "month";


    public static String FROM = "FROM";

    public static String TO = "TO";

    public static String TIMESLOT = "TIMESLOT";

    public static String STATUS_EXP = "STATUS EXP";

    public static String STATUS_MESSAGE = "STATUS MESSAGE";

    public static String DEFINE = "DEFINE";

    public static String COMPUTE_ON = "COMPUTE ON";

    public static String INCENTIVE_GROUP = "INCENTIVE_GROUP";

    public static String ZONE = "ZONE";

    public static String CITY = "CITY";

    public static String SCOPE = "SCOPE";

    public static String COMPUTE_EOD = "eod";

    public static String COMPUTE_EOW = "eow";

    public static String COMPUTE_EOM = "eom";

    public static String COMPUTE_EOY = "eoy";

    public static String COMPUTE_ASSIGNED = "assigned";

    public static String COMPUTE_CONFIRMED = "confirmed";

    public static String COMPUTE_ARRIVED = "arrived";

    public static String COMPUTE_PICKEDUP = "pickedup";

    public static String COMPUTE_REACHED = "reached";

    public static String COMPUTE_DELIVERED = "delivered";

    public static String COMPUTE_REJECT = "rejected";

    public static String COMPUTE_AUTO_REJECT = "auto_rejected";

    public static String COMPUTE_CANCELLED = "cancelled";

    public static String COMPUTE_LOGGED_IN = "login";

    public static String COMPUTE_LOGGED_OUT = "logout";

    public static String LOGGED_IN_TIME = "login_time";

    public static List<String> obligatoryEntities;

    public static List<String> computeOnFields;

    static {
        obligatoryEntities = new ArrayList<String>();
        obligatoryEntities.add(NAME);
        obligatoryEntities.add(DESCRIPTION);
        obligatoryEntities.add(FOR);
        obligatoryEntities.add(CONDITION);
        obligatoryEntities.add(BONUS);

        computeOnFields = new ArrayList<String>();
        computeOnFields.add(COMPUTE_EOD);
        computeOnFields.add(COMPUTE_EOW);
        computeOnFields.add(COMPUTE_EOM);
        computeOnFields.add(COMPUTE_EOY);
        computeOnFields.add(COMPUTE_ASSIGNED);
        computeOnFields.add(COMPUTE_CONFIRMED);
        computeOnFields.add(COMPUTE_ARRIVED);
        computeOnFields.add(COMPUTE_PICKEDUP);
        computeOnFields.add(COMPUTE_REACHED);
        computeOnFields.add(COMPUTE_DELIVERED);
        computeOnFields.add(COMPUTE_AUTO_REJECT);
        computeOnFields.add(COMPUTE_REJECT);
        computeOnFields.add(COMPUTE_CANCELLED);
        computeOnFields.add(COMPUTE_LOGGED_IN);
        computeOnFields.add(COMPUTE_LOGGED_OUT);


    }
}
