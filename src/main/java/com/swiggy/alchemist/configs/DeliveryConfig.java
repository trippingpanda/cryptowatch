package com.swiggy.alchemist.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/**
 * Created by RAVISINGH on 05/10/16.
 */

@Configuration
@EnableJpaRepositories(
        basePackages = "com.swiggy.alchemist.db.delivery.repository",
        entityManagerFactoryRef = "deliveryEntityManager",
        transactionManagerRef = "deliveryTransactionManager"
)
public class DeliveryConfig {

    @Autowired
    private Environment environment;

    @Bean(name = "deliveryDataSource")
    public DataSource deliveryDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl(environment.getProperty("delivery.db.url"));
        dataSource.setUsername(environment.getProperty("delivery.db.user"));
        dataSource.setPassword(environment.getProperty("delivery.db.password"));
        return dataSource;
    }

    @Bean(name = "deliveryEntityManager")
    public EntityManagerFactory deliveryEntityManager() {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(false);

        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setPackagesToScan("com.swiggy.alchemist.db.delivery.entities");
        factory.setDataSource(deliveryDataSource());
        factory.setPersistenceUnitName("deliveryEntityManager");
        factory.afterPropertiesSet();

        return factory.getObject();
    }

    @Bean(name = "deliveryTransactionManager")
    public PlatformTransactionManager deliveryTransactionManager() {
        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(deliveryEntityManager());
        return txManager;
    }

    /**
     * Redis Template
     * @return
     */
    @Bean(name = "deliveryRedisConnectionFactory")
    public RedisConnectionFactory jedisConnectionFactory(){
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setUsePool(true);
        jedisConnectionFactory.setHostName(environment.getProperty("redis.delivery.host"));
        jedisConnectionFactory.setDatabase(Integer.valueOf(environment.getProperty("redis.delivery.database")));
        return jedisConnectionFactory;
    }

    @Bean(name = "deliveryRedisTemplate")
    public RedisTemplate redisTemplate(){
        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(jedisConnectionFactory());
        RedisSerializer<String> stringSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(stringSerializer);
        redisTemplate.setValueSerializer(stringSerializer);
        redisTemplate.setHashKeySerializer(stringSerializer);
        redisTemplate.setHashValueSerializer(stringSerializer);
        return redisTemplate;
    }
}
