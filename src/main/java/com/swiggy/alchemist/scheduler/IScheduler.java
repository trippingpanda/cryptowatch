package com.swiggy.alchemist.scheduler;

public interface IScheduler {
    void runEndOfDay();
    void runEndOfWeek();
    void runEndOfMonth();
}
