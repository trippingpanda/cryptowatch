package com.swiggy.alchemist.scheduler;

import com.swiggy.alchemist.dao.dePayments.DePaymentsDao;
import com.swiggy.alchemist.enums.EventNames;
import com.swiggy.alchemist.pojo.*;
import com.swiggy.alchemist.queues.publish.RabbitmqPublish;
import com.swiggy.alchemist.services.caching.RedisLock;
import com.swiggy.alchemist.services.eventProcessors.IEventProcessor;
import com.swiggy.alchemist.utility.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@Slf4j
public class SchedulerImpl implements IScheduler{

    @Autowired
    private DePaymentsDao dePaymentsDao;

    @Autowired
    private IEventProcessor eventProcessor;

    @Autowired
    private EmailService emailService;

    @Autowired
    private RedisLock redisLock;

    @Autowired
    private RabbitmqPublish rabbitmqPublish;

    private static final String CRON_LOCK_KEY_EOD = "cron_lock_key_eod";

    private static final String CRON_LOCK_KEY_EOW = "cron_lock_key_eow";

    private static final String CRON_LOCK_KEY_EOM = "cron_lock_key_eom";


    @Override
    @Scheduled(cron = "0 1 0 * * *")
    public void runEndOfDay() {
        if(!redisLock.getLock(CRON_LOCK_KEY_EOD,60)){
            log.info("Cron already running on other box");
            return;
        }
        dePaymentsDao.archiveDailyBonusPayments();
        try {
            eventProcessor.processEvent(EventPojo.builder().eventName(EventNames.END_OF_DAY.getName()).eventData(EODPojo.builder().time(Instant.now().getEpochSecond()*1000).build()).build());
        }catch (Exception e){
            log.info("error",e);
            rabbitmqPublish.publishFailedMessagePojo(FailedMessagePojo.builder().type(EventNames.END_OF_DAY.getName()).build());
            emailService.sendEmail("Error in eod incentive cron", ExceptionUtils.getStackTrace(e));
        }
    }

    /**
     * Every Monday 0016 hours.
     */
    @Override
    @Scheduled(cron = "0 16 0 * * MON")
    public void runEndOfWeek() {
        if(!redisLock.getLock(CRON_LOCK_KEY_EOW,60)){
            log.info("Cron already running on other box");
            return;
        }
        dePaymentsDao.archiveWeeklyBonusPayments();
        try {
            eventProcessor.processEvent(EventPojo.builder().eventName(EventNames.END_OF_WEEK.getName()).eventData(EOWPojo.builder().time(Instant.now().getEpochSecond()*1000).build()).build());
        }catch (Exception e){
            log.info("error",e);
            rabbitmqPublish.publishFailedMessagePojo(FailedMessagePojo.builder().type(EventNames.END_OF_WEEK.getName()).build());
            emailService.sendEmail("Error in incentive eow cron",ExceptionUtils.getStackTrace(e));
        }
    }

    /**
     * 1st of every month at 0040.
     */
    @Override
    @Scheduled(cron = "0 40 0 1 * ?")
    public void runEndOfMonth() {
        if(!redisLock.getLock(CRON_LOCK_KEY_EOM,60)){
            log.info("Cron already running on other box");
            return;
        }
        dePaymentsDao.archiveMonthlyBonusPayments();
        try {
            eventProcessor.processEvent(EventPojo.builder().eventName(EventNames.END_OF_MONTH.getName()).eventData(EOMPojo.builder().time(Instant.now().getEpochSecond()*1000).build()).build());
        }catch (Exception e){
            log.info("error",e);
            rabbitmqPublish.publishFailedMessagePojo(FailedMessagePojo.builder().type(EventNames.END_OF_MONTH.getName()).build());
            emailService.sendEmail("Error in incentive eom cron",ExceptionUtils.getStackTrace(e));
        }
    }
}
