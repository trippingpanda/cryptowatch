package com.swiggy.alchemist.exceptions;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by RAVISINGH on 22/09/16.
 */

@SuppressWarnings("serial")
public class AlchemistException extends RuntimeException {

    @Getter
    @Setter
    private int httpStatusCode;
    @Getter @Setter private String userMessage;
    @Getter @Setter private int swiggyResponseCode;

    public AlchemistException() {
        super();
        this.httpStatusCode = 400;
        this.swiggyResponseCode = 1;
    }

    public AlchemistException(int httpStatusCode, String message) {
        super(message);
        this.userMessage = message;
        this.httpStatusCode = httpStatusCode;
        this.swiggyResponseCode = 1;
    }

    public AlchemistException(int httpStatusCode, String message, String userMessage) {
        super(message);
        this.httpStatusCode = httpStatusCode;
        this.userMessage = userMessage;
        this.swiggyResponseCode = 1;
    }

    public AlchemistException(int httpStatusCode, int swiggyResponseCode,
                                    String message) {
        super(message);
        this.httpStatusCode = httpStatusCode;
        this.swiggyResponseCode = swiggyResponseCode;
        this.userMessage = message;
    }

    public AlchemistException(int httpStatusCode, int swiggyResponseCode,
                                    String message, String userMessage) {
        super(message);
        this.httpStatusCode = httpStatusCode;
        this.swiggyResponseCode = swiggyResponseCode;
        this.userMessage = userMessage;
    }
}
