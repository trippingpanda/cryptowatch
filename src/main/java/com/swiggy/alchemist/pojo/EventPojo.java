package com.swiggy.alchemist.pojo;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Builder
@Data
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Slf4j
public class EventPojo {
    @JsonProperty("event_name")
    String eventName;

    @JsonProperty("data")
    DefaultEventDataPojo eventData;

    @JsonIgnore
    public EventPojo clone(){
        try {
            EventPojo eventPojo = new EventPojo();
            eventPojo.setEventName(this.eventName);
            StatusUpdatePojo statusUpdatePojo = (StatusUpdatePojo) this.eventData;
            eventPojo.setEventData(statusUpdatePojo.clone());
            return eventPojo;
        } catch (Exception e){
            log.error("Error while cloning event obejct:{}",this.eventData);
        }
        return null;
    }
}
