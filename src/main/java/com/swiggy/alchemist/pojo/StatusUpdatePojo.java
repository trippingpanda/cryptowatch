package com.swiggy.alchemist.pojo;

/**
 * Created by murthykaja on 2/21/16.
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.swiggy.alchemist.utility.SwiggyUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.ParseException;
import java.text.SimpleDateFormat;


@Builder
@Data
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class StatusUpdatePojo extends DefaultEventDataPojo{

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("batch_id")
    private Long batchId;

    @JsonProperty("status")
    private String status;

    @JsonProperty("source")
    private String source;


    @JsonProperty("partner_name")
    private String partnerName;

    @JsonProperty("time")
    private String time;

    @JsonProperty("deDetails")
    private DeDetails deDetails;

    @JsonProperty("deliveryboy_id")
    private Long deliveryBoyId;

    @JsonProperty("_de_order_restaurant_bill")
    private Integer bill;

    @JsonProperty("_de_order_spending")
    private Integer spending;

    @JsonProperty("_de_order_incoming")
    private Integer incoming;

    @JsonProperty("params")
    private StatusParams params ;

    @JsonProperty("batch_count")
    private Integer batchOrderCount;

    @JsonProperty("order_ack_time")
    private Object orderAckTime;


    public long getTime(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            return format.parse(this.time).getTime();
        } catch (ParseException e) {
            return 0;
        }
    }

    @JsonIgnore
    public StatusUpdatePojo clone(){
        String jsonString = SwiggyUtils.jsonEncode(this);
        return  (StatusUpdatePojo) SwiggyUtils.jsonDecode(jsonString, StatusUpdatePojo.class);
    }
}
