package com.swiggy.alchemist.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by RAVISINGH on 07/11/16.
 */
@Builder
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class DeAppRulePojo {
    public DeAppRulePojo (String ruleName, String bonus) {
        this.ruleName = ruleName;
        this.bonus = bonus;
        this.time = 0;
    }
    @JsonProperty("RuleName")
    String ruleName;

    @JsonProperty("Bonus")
    String bonus;

    @JsonProperty("TimeStamp")
    long time;
}
