package com.swiggy.alchemist.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAVISINGH on 06/10/16.
 */
@Builder
@Data
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RuleDomainPojo {

    @JsonProperty("incentive_groups")
    List<String> incentiveGroups;

    @JsonProperty("zones")
    List<String> zones;

    @JsonProperty("cities")
    List<String> cities;

    public RuleDomainPojo() {
        incentiveGroups = new ArrayList<String>();
        zones = new ArrayList<String>();
        cities = new ArrayList<String>();
    }



}
