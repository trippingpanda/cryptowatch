package com.swiggy.alchemist.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Created by RAVISINGH on 24/10/16.
 */
@Builder
@Data
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SurgeCreateRequest {

    @JsonProperty("zoneId")
    Long zoneId;

    @JsonProperty("startTime")
    String startTime;

    @JsonProperty("endTime")
    String endTime;

    @JsonProperty("multiplier")
    float multiplier;


}
