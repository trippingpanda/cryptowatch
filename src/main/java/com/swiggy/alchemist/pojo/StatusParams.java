package com.swiggy.alchemist.pojo;

/**
 * Created by murthykaja on 2/21/16.
 */
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;
import java.lang.Integer;


@Builder
@Data
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class StatusParams {

    @JsonProperty("pay")
    private Integer pay;

    @JsonProperty("bill")
    private Integer bill;

    @JsonProperty("collect")
    private Integer collect;

    @JsonProperty("oos")
    private String oos;

    @JsonProperty("place_by")
    private String placedBy;

    @JsonProperty("order_id")
    private String orderId;

}
