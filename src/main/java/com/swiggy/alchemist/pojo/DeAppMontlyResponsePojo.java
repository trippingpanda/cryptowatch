package com.swiggy.alchemist.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by RAVISINGH on 08/11/16.
 */
@Builder
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class DeAppMontlyResponsePojo {

    @JsonProperty("Total")
    String total;

    //Seems bad but legacy :(

    @JsonProperty("1")
    String firstWeek;

    @JsonProperty("2")
    String secondWeek;

    @JsonProperty("3")
    String thirdWeek;

    @JsonProperty("4")
    String fourthWeek;

}
