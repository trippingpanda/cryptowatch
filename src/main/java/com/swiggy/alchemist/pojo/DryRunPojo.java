package com.swiggy.alchemist.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by RAVISINGH on 22/11/16.
 */
@Builder
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class DryRunPojo {

    @JsonProperty("orderId")
    String orderId;

    @JsonProperty("bonuses")
    List<String> bonuses;

    @JsonProperty("total")
    Double total;

    @JsonProperty("deId")
    Long deId;
}
