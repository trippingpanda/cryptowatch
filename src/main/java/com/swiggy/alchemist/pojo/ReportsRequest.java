package com.swiggy.alchemist.pojo;

/**
 * Created by RAVISINGH on 17/10/16.
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Created by RAVISINGH on 28/09/16.
 */
@Builder
@Data
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportsRequest {
    @JsonProperty("zoneIds")
    List<Long> zoneIds;

    @JsonProperty("cityId")
    Long cityId;

    @JsonProperty("from")
    String fromDate;

    @JsonProperty("to")
    String toDate;
}
