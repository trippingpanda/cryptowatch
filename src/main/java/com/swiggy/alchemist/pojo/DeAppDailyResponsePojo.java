package com.swiggy.alchemist.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by RAVISINGH on 07/11/16.
 */
@Builder
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class DeAppDailyResponsePojo {
    @JsonProperty("Total")
    String total;

    @JsonProperty("details")
    List<DeAppRulePojo> deAppRulePojos;
}
