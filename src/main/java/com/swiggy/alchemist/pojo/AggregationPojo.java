package com.swiggy.alchemist.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class AggregationPojo {
    @JsonProperty("avg_confirmation_delay")
    double avgConfirmationDelay;

    @JsonProperty("avg_arrived_delay")
    double avgArrivalDelay;

    @JsonProperty("avg_pickedup_delay")
    double avgPickupDelay;

    @JsonProperty("avg_reached_delay")
    double avgReachedDelay;

    @JsonProperty("avg_delivered_delay")
    double avgDeliveredDelay;

    @JsonProperty("avg_sla")
    double avgSla;

    @JsonProperty("trips_count")
    double trips_count;

    @JsonProperty("avg_last_mile")
    double avgLastMile;

    @JsonProperty("avg_first_mile")
    double avgFirstMile;

    @JsonProperty("logged_in_time")
    long loggedInTime;

    @JsonProperty("reject_count")
    long rejectCount;

    @JsonProperty("qr_fail_count")
    long qrFailCount;

    @JsonProperty("avg_rating")
    double avgRating;

    @JsonProperty("total_ratings")
    long totalRatings;

    @JsonProperty("cancelled_trips_count")
    long cancelledTripsCount;


}
