package com.swiggy.alchemist.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PortalOrderData extends DefaultEventDataPojo{
    @JsonProperty("order_id")
    String orderId;

    @JsonProperty("order_time")
    String orderTime;

    @JsonProperty("restaurant_city_name")
    String restaurantCity;

    @JsonProperty("payment_method")
    String paymentMethod;

    @JsonProperty("order_incoming")
    long orderIncoming;

    @JsonProperty("order_total")
    long orderTotal;

    @JsonProperty("order_restaurant_bill")
    long orderRestaurantBill;

    @JsonProperty("order_spending")
    long orderspending;

    @JsonProperty("prep_time")
    int preptime;

    @JsonProperty("restaurant_customer_distance")
    String predictedlastMile;
}
