package com.swiggy.alchemist.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * Created by RAVISINGH on 28/09/16.
 */
@Builder
@Data
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RuleCreateRequest {
    @JsonProperty("ruleString")
    String ruleString;

    @JsonProperty("user")
    String user;

    @JsonProperty("id")
    Long id;

    @JsonProperty("active")
    Boolean active;
}
