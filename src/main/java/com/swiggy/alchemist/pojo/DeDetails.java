package com.swiggy.alchemist.pojo;

/**
 * Created by murthykaja on 2/21/16.
 */
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

@Builder
@Data
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class DeDetails {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("nick")
    private String nick;

    @JsonProperty("mobileNumber")
    private String mobile;

    @JsonProperty("altMobile")
    private String altMobile;

    @JsonProperty("lat")
    private String lat;

    @JsonProperty("lng")
    private String lng;

    public DeDetails(Long id, String name, String nick, String mobile, String altMobile, String latLng) {
        this.id= id;
        this.name = name;
        this.nick = nick;
        this.mobile = mobile;
        this.altMobile = altMobile;
        String[] location = latLng.split(",");
        if(location.length >=2) {
            this.lat = location[0];
            this.lng = location[1];
        }
    }

}
