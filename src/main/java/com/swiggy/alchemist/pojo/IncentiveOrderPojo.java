package com.swiggy.alchemist.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by abhishek.rawat on 9/28/16.
 */
@Builder
@Data
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class IncentiveOrderPojo {
    @JsonProperty("order_id")
    String orderId;

    @JsonProperty("assigned_time")
    long assignedTime;

    @JsonProperty("confirmation_time")
    long confirmationTime;

    @JsonProperty("arrived_time")
    long arrivedTime;

    @JsonProperty("pickedup_time")
    long pickedUpTime;

    @JsonProperty("reached_time")
    long reachedTime;

    @JsonProperty("delivered_time")
    long deliveredTime;

    @JsonProperty("order_reject_count")
    long rejectCount;

    @JsonProperty("is_cancelled")
    boolean cancelled;

    @JsonProperty("batch_type")
    String batchType;

    @JsonProperty("batch_count")
    int batchCount;

    @JsonProperty("order_weight")
    double orderWeight;

    @JsonProperty("rating")
    double rating;

    @JsonProperty("last_mile")
    double lastMile;

    @JsonProperty("first_mile")
    double firstMile;

    @JsonProperty("is_rain_mode")
    boolean rainMode;

    @JsonProperty("sla")
    double sla;

    @JsonProperty("deDetails")
    private DeDetails deDetails;

    @JsonProperty("surge")
    private String surge;

    @JsonProperty("batch_id")
    Long batchId;
}
