package com.swiggy.alchemist.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by RAVISINGH on 08/11/16.
 */
@Builder
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class DeAppWeeklyResponsePojo {
    @JsonProperty("Total")
    String total;

    //Seems bad but legacy :(

    @JsonProperty("1")
    String firstDay;

    @JsonProperty("2")
    String secondDay;

    @JsonProperty("3")
    String thirdDay;

    @JsonProperty("4")
    String fourthDay;

    @JsonProperty("5")
    String fifthDay;

    @JsonProperty("6")
    String sixthDay;

    @JsonProperty("7")
    String seventhDay;

}
