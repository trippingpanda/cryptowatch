package com.swiggy.alchemist.pojo;

/**
 * Created by RAVISINGH on 28/09/16.
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeslotData {

    @JsonProperty("time")
    String timeString;

    @JsonProperty("days")
    String daysString;
}
