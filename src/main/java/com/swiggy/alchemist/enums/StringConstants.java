package com.swiggy.alchemist.enums;

public class StringConstants {
    public static final String POSTFIX_KEY = "POSTFIX_";
    public static final String POSTFIX_DELIMITER = "|";
    public static final String EVENT_PROCESS_LOCK_KEY = "EVENT_PROCESS_LOCK_DE_";
    public static final String PENDING_EVENTS_KEY = "PENDING_EVENTS_";
}
