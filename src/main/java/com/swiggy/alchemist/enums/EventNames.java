package com.swiggy.alchemist.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public enum EventNames {

    UNKONWN(0,"unknown"),
    ORDER_STATUS_UPDATE(1,"status_update"),
    LOGIN(2,"login"),
    LOGOUT(3,"logout"),
    END_OF_DAY(4,"eod"),
    END_OF_MONTH(5,"eom"),
    NEW_ORDER(6,"new_order"),
    ORDER_REJECT(7,"order_reject"),
    CANCELLED(8,"cancelled"),
    DE_RATING(9,"de_rating"),
    END_OF_WEEK(10,"eow");

    public long getValue() {
        return value;
    }
    public String getName(){
        return name;
    }

    private long value;
    private String name;

    EventNames(long value,String name){
        this.value=value;
        this.name=name;
    }

    private static Map lookup = new HashMap<Long,EventNames>();


    public static EventNames get(String key) {
        return lookup.containsKey(key)? (EventNames) lookup.get(key) :UNKONWN;
    }

    static {
        lookup = (Map) EnumSet.allOf(EventNames.class).stream().collect(Collectors.toMap((x) -> x.name, (x) -> x));
    }
}
