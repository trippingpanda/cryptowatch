package com.swiggy.alchemist.antlr;

import com.swiggy.alchemist.constants.EntityConstants;
import com.swiggy.alchemist.exceptions.AlchemistException;
import com.swiggy.alchemist.pojo.RuleDomainPojo;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.http.HttpStatus;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by RAVISINGH on 20/09/16.
 */
public class AntlrHelper {

    public static HashMap<String,String> parseRules(String ruleString) throws AlchemistException {
        HashMap<String,String> keyValuePair = new HashMap<>();
        HashMap<String, String> result;
        String errorMessage = null;
        ANTLRInputStream stringStream = new ANTLRInputStream(ruleString);
        try {
            BaseErrorListener errorListener = new BaseErrorListener() {
                @Override
                public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int column, String msg,
                                        RecognitionException e) {

                    System.out.println("caught exception");
                    throw new AlchemistException(HttpStatus.SC_BAD_REQUEST,"extra input");
                }
            };
            RuleGrammarLexer lexer = new RuleGrammarLexer(stringStream);
            lexer.addErrorListener(errorListener);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            RuleGrammarParser parser = new RuleGrammarParser(tokens);
            parser.addErrorListener(errorListener);
            RuleGrammarParser.IncentiveRuleContext tree = parser.incentiveRule(); // root of grammar
            RuleTreeWalker ruleTreeWalker = new RuleTreeWalker();
            result = ruleTreeWalker.visitIncentiveRule(tree);
            System.out.println("result1 =>" + result);
            if (result.containsKey(EntityConstants.DEFINE)) {
                String defineParams = result.get(EntityConstants.DEFINE);
                System.out.println("result1 =>" + defineParams.indexOf(" ") + " length "+ defineParams.length());
                if(defineParams == null || !defineParams.contains(" ")) {
                    errorMessage = "two params required for define";
                    throw new AlchemistException(HttpStatus.SC_BAD_REQUEST,errorMessage );
                }
                String key = defineParams.substring(0, defineParams.indexOf(" "));
                String value = defineParams.substring(defineParams.indexOf(" ") + 1, defineParams.length());
                System.out.println("key " + key + " value " + value);
                replaceAll(result, key, value);
            }
            System.out.println("result2 =>" + result);
        } catch (Exception e){
            //e.printStackTrace();
            errorMessage = "Error in parsing rule: ";
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST,errorMessage + e.getMessage() );
        }
        return result;
    }

    private static void replaceAll(HashMap<String,String> hashMap, String key,String value) {
        Iterator it = hashMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            String keyString = pair.getKey().toString();
            String valueString = pair.getValue().toString();
           // System.out.println("iterator pairs"+pair.getKey() + " = " + pair.getValue());
            pair.setValue(valueString.replace(key,value));
          //  it.remove();
        }
    }

    public static Double evaluate(String expression, JSONObject object) {
        ANTLRInputStream stringStream = new ANTLRInputStream(expression);
        EvaluatorLexer lexer = new EvaluatorLexer(stringStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        EvaluatorParser parser = new EvaluatorParser(tokens);

        ParseTree tree = parser.expression();
        ExpressionTreeWalker calcVisitor = new ExpressionTreeWalker(object);
        Double result = calcVisitor.visit(tree);
        System.out.println("Result: " + result);
        return  result;

    }

    public static Double evaluateEquation(String expression, JSONObject object) {
        ANTLRInputStream stringStream = new ANTLRInputStream(expression);
        EvaluatorLexer lexer = new EvaluatorLexer(stringStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        EvaluatorParser parser = new EvaluatorParser(tokens);

        ParseTree tree = parser.cummulative();
        ExpressionTreeWalker calcVisitor = new ExpressionTreeWalker(object);
        Double result = calcVisitor.visit(tree);
        System.out.println("Result: " + result);
        return result;

    }

    public static RuleDomainPojo parseRuleDomainString(String ruleDomain) {
        ANTLRInputStream stringStream = new ANTLRInputStream(ruleDomain);
        ForGrammarLexer lexer = new ForGrammarLexer(stringStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        ForGrammarParser parser = new ForGrammarParser(tokens);
        ForGrammarParser.RuleDomainContext tree = parser.ruleDomain(); // root of grammar
        RuleDomainTreeWalker ruleDomainTreeWalker = new RuleDomainTreeWalker();
        RuleDomainPojo result = ruleDomainTreeWalker.visitRuleDomain(tree);
        System.out.println("Rule domain: " + result);
        return  result;
    }

    private static HashMap<String, String> getFieldValueMap(Object object) {
        HashMap<String, String> objectMap = new HashMap<>();
        Field[] declaredFields = object.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            try {
                if (field.get(object) == null) {
                    objectMap.put(field.getName(), "0");
                }
                String value = String.valueOf(field.get(object));
                objectMap.put(field.getName().toLowerCase(), value);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return objectMap;
    }

}
