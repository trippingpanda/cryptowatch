package com.swiggy.alchemist.antlr;

import org.json.JSONException;
import org.antlr.v4.runtime.misc.NotNull;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by RAVISINGH on 03/10/16.
 */
public class ExpressionTreeWalker extends EvaluatorBaseVisitor<Double> {
    HashMap<String, String> dataMap;
    JSONObject data;
    ExpressionTreeWalker(JSONObject data) {
        this.data = data;
    }

    @Override
    public Double visitPlus(@NotNull EvaluatorParser.PlusContext ctx) {
        return visit(ctx.plusOrMinus()) + visit(ctx.multOrDiv());
    }

    @Override
    public Double visitMinus(@NotNull EvaluatorParser.MinusContext ctx) {
        return visit(ctx.plusOrMinus()) - visit(ctx.multOrDiv());
    }

    @Override
    public Double visitMultiplication(@NotNull EvaluatorParser.MultiplicationContext ctx) {
        return visit(ctx.multOrDiv()) * visit(ctx.pow());
    }

    @Override
    public Double visitDivision(@NotNull EvaluatorParser.DivisionContext ctx) {
        return visit(ctx.multOrDiv()) / visit(ctx.pow());
    }


    @Override
    public Double visitPower(@NotNull EvaluatorParser.PowerContext ctx) {
        if (ctx.pow() != null)
            return Math.pow(visit(ctx.unaryMinus()), visit(ctx.pow()));
        return visit(ctx.unaryMinus());
    }

    @Override
    public Double visitChangeSign(@NotNull EvaluatorParser.ChangeSignContext ctx) {
        return -1*visit(ctx.unaryMinus());
    }

    @Override
    public Double visitBraces(@NotNull EvaluatorParser.BracesContext ctx) {
        return visit(ctx.plusOrMinus());
    }


    @Override
    public Double visitInt(@NotNull EvaluatorParser.IntContext ctx) {
        return Double.parseDouble(ctx.INT().getText());
    }

    @Override
    public Double visitVariable(@NotNull EvaluatorParser.VariableContext ctx) {
        String variableValue;
        try {
            variableValue = (String) data.get(ctx.ID().toString());
        } catch (JSONException e){
            variableValue = null;
        }
        if(variableValue == null) {
            return 0D;
        }
        if (variableValue.equalsIgnoreCase("true")) {
            variableValue = "1";
        } else if (variableValue.equalsIgnoreCase("false")) {
            variableValue = "0";
        }
        try {
            Double value = Double.parseDouble(variableValue);
            return value;
        } catch (Exception e) {
            return 0D;
        }

    }

    @Override
    public Double visitDouble(@NotNull EvaluatorParser.DoubleContext ctx) {
        return Double.parseDouble(ctx.DOUBLE().getText());
    }

    @Override
    public Double visitCalculate(@NotNull EvaluatorParser.CalculateContext ctx) {
        return visit(ctx.plusOrMinus());
    }

    @Override
    public Double visitEqualTo(@NotNull EvaluatorParser.EqualToContext ctx) {

        if (visit(ctx.expression().get(0)).equals(visit(ctx.expression().get(1)))) {
            return 1D;
        } else {
            return 0D;
        }

    }

    @Override
    public Double visitMinExpression(@NotNull EvaluatorParser.MinExpressionContext ctx) {

        Double d1 = visit(ctx.expression().get(0));
        Double d2 = visit(ctx.expression().get(1));
        if (d1.compareTo(d2) >= 0 ) {
            return d2;
        } else {
            return d1;
        }

    }

    @Override
    public Double visitMaxExpression(@NotNull EvaluatorParser.MaxExpressionContext ctx) {

        Double d1 = visit(ctx.expression().get(0));
        Double d2 = visit(ctx.expression().get(1));
        if (d1.compareTo(d2) >= 0 ) {
            return d1;
        } else {
            return d2;
        }

    }

    @Override
    public Double visitGreaterThan(@NotNull EvaluatorParser.GreaterThanContext ctx) {

        if (visit(ctx.expression().get(0)).compareTo(visit(ctx.expression().get(1))) > 0) {
            return 1D;
        } else {
            return 0D;
        }

    }

    @Override
    public Double visitLessThan(@NotNull EvaluatorParser.LessThanContext ctx) {

        if (visit(ctx.expression().get(0)).compareTo(visit(ctx.expression().get(1))) < 0) {
            return 1D;
        } else {
            return 0D;
        }

    }

    @Override
    public Double visitAndCondition(@NotNull EvaluatorParser.AndConditionContext ctx) {

       // System.out.println("visiting and condition " + visit(ctx.equation().get(0)) + " equation "+ visit(ctx.equation().get(1)));
        if ((visit(ctx.equation()) > 0) && (visit(ctx.cummulative()) > 0))  {
            return 1D;
        } else {
            return 0D;
        }

    }

    @Override
    public Double visitOrCondition(@NotNull EvaluatorParser.OrConditionContext ctx) {

        if ((visit(ctx.equation()) > 0) || (visit(ctx.cummulative()) > 0))  {
            return 1D;
        } else {
            return 0D;
        }

    }

    @Override
    public Double visitEquationWithParen(@NotNull EvaluatorParser.EquationWithParenContext ctx) {
        if (visit(ctx.equation()) > 0)  {
            return 1D;
        } else {
            return 0D;
        }

    }

    @Override
    public Double visitCummulativeWithParen(@NotNull EvaluatorParser.CummulativeWithParenContext ctx) {
        if (visit(ctx.cummulative()) > 0)  {
            return 1D;
        } else {
            return 0D;
        }

    }

}
