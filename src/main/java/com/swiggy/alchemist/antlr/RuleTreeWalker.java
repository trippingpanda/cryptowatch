package com.swiggy.alchemist.antlr;

import com.google.common.collect.Maps;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.HashMap;
import java.util.List;

/**
 * Created by RAVISINGH on 15/09/16.
 */
public class RuleTreeWalker extends RuleGrammarBaseVisitor<HashMap<String,String>>
        implements RuleGrammarVisitor<HashMap<String,String>> {

    public HashMap<String,String> visitIncentiveRule(RuleGrammarParser.IncentiveRuleContext ctx) {
        HashMap<String,String> response = new HashMap<>();
        List<RuleGrammarParser.PairContext> pairs = ctx.pair();
        for (RuleGrammarParser.PairContext pairContext : pairs) {
            response.putAll(Maps.difference(this.visitPair(pairContext), response).entriesOnlyOnLeft());
        }

        return response;
    }



    public HashMap<String,String> visitPair(RuleGrammarParser.PairContext ctx) {
        HashMap<String,String> response = new HashMap<>();
        List<TerminalNode>  texts = ctx.TEXT();
        String key = ctx.KEY().toString();
        String combinedText = "";
        for(TerminalNode terminalNode : texts) {
            combinedText = combinedText + " "+terminalNode.toString();
        }
        response.put(key.toUpperCase().trim(),combinedText.trim()); //making to uppercase as validations become easy
      //  System.out.println("key,value  " +key+" , "+combinedText);
        return response;
    }


}