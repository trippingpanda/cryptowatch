package com.swiggy.alchemist.antlr;

import com.google.common.collect.Maps;
import com.swiggy.alchemist.constants.EntityConstants;
import com.swiggy.alchemist.pojo.RuleDomainPojo;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by RAVISINGH on 04/10/16.
 */
public class RuleDomainTreeWalker extends ForGrammarBaseVisitor<RuleDomainPojo>
        implements ForGrammarVisitor<RuleDomainPojo> {
    public RuleDomainPojo visitRuleDomain(ForGrammarParser.RuleDomainContext ctx) {
        RuleDomainPojo result = new RuleDomainPojo();
        List<ForGrammarParser.PairContext> pairs = ctx.pair();
        for (ForGrammarParser.PairContext pairContext : pairs) {
            result.getIncentiveGroups().addAll(this.visitPair(pairContext).getIncentiveGroups());
            result.getZones().addAll(this.visitPair(pairContext).getZones());
            result.getCities().addAll(this.visitPair(pairContext).getCities());
        }

        return result;
    }



    public RuleDomainPojo visitPair(ForGrammarParser.PairContext ctx) {
        RuleDomainPojo response = new RuleDomainPojo();
        String key = ctx.KEY().toString();
        List<TerminalNode>  texts = ctx.TEXT();
        String combinedText = "";
        for(TerminalNode terminalNode : texts) {
            combinedText = combinedText + " "+terminalNode.toString();
        }
        if (key.equalsIgnoreCase(EntityConstants.INCENTIVE_GROUP)) {
            response.getIncentiveGroups().add(combinedText.trim());
        } else if (key.equalsIgnoreCase(EntityConstants.ZONE)) {
            response.getZones().add(combinedText.trim());
        } else if (key.equalsIgnoreCase(EntityConstants.CITY)) {
            response.getCities().add(combinedText.trim());
        }
        return response;
    }
}
