package com.swiggy.alchemist.aspects;

import com.swiggy.alchemist.enums.StringConstants;
import com.swiggy.alchemist.pojo.EventPojo;
import com.swiggy.alchemist.services.caching.RedisLock;
import com.swiggy.alchemist.services.eventProcessors.IEventProcessor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;


@Aspect
@Component
@Slf4j
public class DeLockAspect {

    @Autowired
    RedisLock redisLock;

    @Autowired
    RedisTemplate<String,EventPojo> eventPojoRedisTemplate;

    @Autowired
    IEventProcessor eventProcessor;

    /**
     * Get a lock of the de. All the other requests for this de are sent to redis in a pending list.
     * Once this process is complete. Pending list for this de is checked and events are passed until the list is empty.
     * Lock is then released.
     *
     * If the lock is release before then there can be race condition between a new update and the update processing.
     * In that case, if pending event does not get the lock then it will be added to the end of the list and sequence
     * of events for the de will be lost.
     * @param joinPoint
     * @throws Throwable
     */
    @Around("execution(@com.swiggy.alchemist.aspects.LockDE * *(..))")
    public void lockDeForEvent(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        if(args[1] == null){
            joinPoint.proceed();
            return;
        }

        String deId = String.valueOf(args[1]);
        EventPojo eventPojo = (EventPojo) args[0];
        boolean deProcessLock = redisLock.getLock(StringConstants.EVENT_PROCESS_LOCK_KEY + deId);
        if(deProcessLock){
            joinPoint.proceed();
            processPendingEvents(deId);
            redisLock.deleteLock(StringConstants.EVENT_PROCESS_LOCK_KEY + deId);
            return;
        }

        addToPendingEvents(deId,eventPojo);
    }

    private void addToPendingEvents(String deId, EventPojo eventPojo) {
        eventPojoRedisTemplate.opsForList().rightPush(StringConstants.PENDING_EVENTS_KEY+deId,eventPojo);
    }

    private void processPendingEvents(String deId) {
        while (eventPojoRedisTemplate.opsForList().size(StringConstants.PENDING_EVENTS_KEY + deId) > 0){
            eventProcessor.processPendingEvents(eventPojoRedisTemplate.opsForList().leftPop(StringConstants.PENDING_EVENTS_KEY + deId));
        }
    }
}