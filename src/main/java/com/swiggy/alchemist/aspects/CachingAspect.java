package com.swiggy.alchemist.aspects;

import com.swiggy.alchemist.dao.incentiveConfig.Options;
import com.swiggy.alchemist.services.caching.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;



@Aspect
@Component
@Slf4j
public class CachingAspect {


    @Autowired
    private Options options;

    @Around("execution(@com.swiggy.alchemist.aspects.Redis * *(..))")
    public Object redisCache(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = joinPoint.getTarget().getClass().getMethod(signature.getName(), signature.getParameterTypes());
        CacheService cacheService = method.getAnnotation(Redis.class).cacheService().getCacheService();
        String cachePrefix = method.getAnnotation(Redis.class).cacheKeyPrefix();
        int cacheExpiry = method.getAnnotation(Redis.class).defaultExpiry();
        boolean isUpdate = method.getAnnotation(Redis.class).isUpdate();

        Object cacheValue = null;
        if(!isUpdate)
            cacheValue = cacheService.get(cachePrefix + parametersToString(method,joinPoint.getArgs()), signature.getMethod().getReturnType());
        log.info("cache value:{} key:{}",cacheValue,cachePrefix + parametersToString(method,joinPoint.getArgs()), signature.getMethod().getReturnType());
        if(cacheValue == null){
            cacheValue = joinPoint.proceed();
            if(cacheValue != null) {
                cacheService.set(cachePrefix + parametersToString(method, joinPoint.getArgs()), cacheValue, cacheExpiry);
            }
        }
        return cacheValue;
    }


    private String parametersToString(Method method, Object[] args) {
        Parameter[] parameters = method.getParameters();
        StringBuilder value = new StringBuilder("");

        int pos = 0;
        for (Parameter param : parameters) {
            if (param.getAnnotation(RedisKey.class) != null) {
                    if(args[pos]!=null) {
                        value.append(args[pos].toString().replace(" ","-").trim()).append("_");
                    }
            }
            pos++;
        }

        if (StringUtils.isEmpty(value.toString())) {
            for (Object p : args) {
                if(p != null) {
                    value.append(p.toString().replace(" ","-").trim()).append("_");
                }
            }
        }
        if(value.toString().endsWith("_")){
            return value.toString().substring(0, value.length() - 1);
        }
        return value.toString();
    }

}
