package com.swiggy.alchemist.aspects;


import com.swiggy.alchemist.services.caching.Cache;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by swiggy on 15/10/15.
 */

/**
 * If an object is passed as parameter then toString() method should be overridden otherwise the objects default toString() will be called.
 * Its not as issue for persistence but it will be hard to retrieve the key value with default toString().
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.ANNOTATION_TYPE })
public @interface Redis {

    String cacheKeyPrefix() default "";

    /**
     * Default expiry for the cache.
     * @return
     */
    int defaultExpiry();

    Cache cacheService() default Cache.DEFAULT;

    /**
     * WP_Option for expiry time.
     * @return
     */
    String wp_option() default "";

    /**
     * Used to specify if its an update call. So existing key-value pair will be updated in redis.
     * @return
     */
    boolean isUpdate() default false;
}