package com.swiggy.alchemist.controller;

import com.swiggy.alchemist.exceptions.AlchemistException;
import com.swiggy.alchemist.pojo.BaseResponse;
import com.swiggy.alchemist.services.deApp.DeAppBo;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by RAVISINGH on 18/10/16.
 */
@Controller
@RequestMapping("/deIncentive")
public class DeAppController {
    private static final Logger logger = LoggerFactory
            .getLogger(DeAppController.class);
    @Autowired
    DeAppBo deAppBo;

    @RequestMapping(
            value = {"/day/{day}","/day"},
            method = RequestMethod.GET
    )
    @ResponseBody
    BaseResponse getDayBonus(
            @RequestHeader(value="Authorization",required = false) final String auth,
            @PathVariable(value = "day") Optional<Integer> dayOptional)
            throws AlchemistException {
        Long deId = deAppBo.authenticateRequest(auth);
        int day=0;
        if (deId == null || deId.equals(0L)) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST,"NO de id");
        }
        if (dayOptional.isPresent() ){
            day= dayOptional.get()-1;
        }
        return new BaseResponse(0,"Success",deAppBo.getDailyData(day, deId));//-1 for app legacy

    }

    @RequestMapping(
            value = {"/week/{week}","/week"},
            method = RequestMethod.GET
    )
    @ResponseBody
    BaseResponse getWeekBonus(
            @RequestHeader(value="Authorization",required = false) final String auth,
            @PathVariable(value = "week") Optional<Integer> weekOptional)
            throws AlchemistException {
        Long deId = deAppBo.authenticateRequest(auth);
        int week =0;
        if (deId == null || deId.equals(0L)) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST,"NO de id");
        }
        if(weekOptional.isPresent()) {
            week = weekOptional.get()-1;
        }
        return new BaseResponse(0,"Success",deAppBo.getWeekData(week, deId));//-1 for app legacy

    }

    @RequestMapping(
            value = "/month",
            method = RequestMethod.GET
    )
    @ResponseBody
    BaseResponse getMonthBonus(
            @RequestHeader(value="Authorization",required = false) final String auth)
            throws AlchemistException {
        Long deId = deAppBo.authenticateRequest(auth);
        if (deId == null || deId.equals(0L)) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST,"NO de id");
        }
        return new BaseResponse(0,"Success",deAppBo.getMonthData(deId));

    }
}
