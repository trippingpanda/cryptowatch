package com.swiggy.alchemist.controller;

import com.swiggy.alchemist.antlr.AntlrHelper;
import com.swiggy.alchemist.dao.surge.SurgeDao;
import com.swiggy.alchemist.exceptions.AlchemistException;
import com.swiggy.alchemist.pojo.*;
import com.swiggy.alchemist.services.ruleParser.RuleParserBo;
import com.swiggy.alchemist.utility.SwiggyUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Timestamp;

/**
 * Created by RAVISINGH on 27/09/16.
 */
@Controller
@RequestMapping("/alchemist")
public class RuleController {
    private static final Logger logger = LoggerFactory
            .getLogger(RuleController.class);
    @Autowired
    RuleParserBo ruleParserBo;

    @Autowired
    SurgeDao surgeDao;

    @RequestMapping(
            value = "/addRule",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody
    BaseResponse addRule(
            @RequestBody(required = true) RuleCreateRequest ruleCreateRequest)
            throws AlchemistException {
        ruleParserBo.addRule(ruleCreateRequest.getRuleString(),ruleCreateRequest.getId(),ruleCreateRequest.getUser(), ruleCreateRequest.getActive());
        return  new BaseResponse(0,"Rule Created");
    }

    @RequestMapping(
            value = "/addSurge",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody
    BaseResponse addSurge(
            @RequestBody(required = true) SurgeCreateRequest surgeCreateRequest)
            throws AlchemistException {
        Timestamp startTime = SwiggyUtils.getTimeStamp(surgeCreateRequest.getStartTime());
        Timestamp endTime = SwiggyUtils.getTimeStamp(surgeCreateRequest.getEndTime());
        if (startTime.compareTo(endTime) >= 0) {
            return new BaseResponse(0, "start and end time not valid");
        }
        if (!surgeDao.isSurgeValid(surgeCreateRequest.getZoneId(), startTime, endTime))  {
            return  new BaseResponse(0, "Conflicting surge available");
        }
        surgeDao.addSurge(surgeCreateRequest.getZoneId(),startTime ,endTime
                , surgeCreateRequest.getMultiplier());
        return  new BaseResponse(0,"Surge Created");
    }

    @RequestMapping(
            value = "/archiveSurge",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody
    BaseResponse archiveSurge(
            @RequestBody(required = true) SurgeArchiveRequest surgeArchiveRequest)
            throws AlchemistException {

        surgeDao.markSurgeArchived(surgeArchiveRequest.getId(), surgeArchiveRequest.getUserId());
        return  new BaseResponse(0,"Surge Archived");
    }

    @RequestMapping(
            value = "/testExpression",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody
    BaseResponse testExpression(
            @RequestBody(required = true) TestPojo testPojo)
            throws AlchemistException {

        return  new BaseResponse(0, String.valueOf(AntlrHelper.evaluate(testPojo.getExpression(), new JSONObject())));
    }

    @RequestMapping(
            value = "/testEquation",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public @ResponseBody
    BaseResponse testEquation(
            @RequestBody(required = true) TestPojo testPojo)
            throws AlchemistException {

        return  new BaseResponse(0, String.valueOf(AntlrHelper.evaluateEquation(testPojo.getExpression(), new JSONObject())));
    }
}
