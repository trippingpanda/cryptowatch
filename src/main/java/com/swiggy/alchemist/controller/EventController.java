package com.swiggy.alchemist.controller;

import com.swiggy.alchemist.enums.EventNames;
import com.swiggy.alchemist.pojo.*;
import com.swiggy.alchemist.services.eventProcessors.IEventProcessor;
import com.swiggy.alchemist.utility.SwiggyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Slf4j
@Controller
@RequestMapping("/alchemist")
public class EventController {

    @Autowired
    private IEventProcessor eventProcessor;

    @RequestMapping(
            value = "/computeEvent",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    BaseResponse fireEvent(@RequestBody FireEventPojo fireEventPojo) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long time;
        try {
             time = simpleDateFormat.parse(fireEventPojo.getTime()).getTime();
        } catch (ParseException e) {
            return new BaseResponse(1,"error parsing time");
        }
        if(fireEventPojo.getEventName().equals(EventNames.END_OF_DAY.getName())){
            eventProcessor.processEvent(EventPojo.builder().eventName(EventNames.END_OF_DAY.getName()).eventData(EODPojo.builder().time(time).build()).build());
            return new BaseResponse(0,"Success eod event fired");
        }
        if(fireEventPojo.getEventName().equals(EventNames.END_OF_WEEK.getName())){
            eventProcessor.processEvent(EventPojo.builder().eventName(EventNames.END_OF_WEEK.getName()).eventData(EOWPojo.builder().time(time).build()).build());
            return new BaseResponse(0,"Success eow event fired");
        }

        if(fireEventPojo.getEventName().equals(EventNames.END_OF_MONTH.getName())){
            eventProcessor.processEvent(EventPojo.builder().eventName(EventNames.END_OF_MONTH.getName()).eventData(EOMPojo.builder().time(time).build()).build());
            return new BaseResponse(0,"Success eod event fired");
        }
        return new BaseResponse(0,"invalid data");
    }
}
