package com.swiggy.alchemist.controller;

/**
 * Created by RAVISINGH on 17/10/16.
 */

import com.swiggy.alchemist.dao.deliveryBoys.DeliveryBoysDao;
import com.swiggy.alchemist.exceptions.AlchemistException;
import com.swiggy.alchemist.pojo.BaseResponse;
import com.swiggy.alchemist.pojo.ReportsRequest;
import com.swiggy.alchemist.services.reports.AlchemistReportsBo;
import com.swiggy.alchemist.utility.SwiggyUtils;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.ByteArrayOutputStream;

/**
 * Created by RAVISINGH on 27/09/16.
 */
@Controller
@RequestMapping("/alchemist/reports")
public class ReportController {
    private static final Logger logger = LoggerFactory
            .getLogger(ReportController.class);
    @Autowired
    DeliveryBoysDao deliveryBoysDao;

    @Autowired
    AlchemistReportsBo alchemistReportsBo;


    @RequestMapping(
            value = "/city",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = "text/csv"
    )
    @ResponseBody byte[] getReportsCity(
            @RequestBody(required = true) ReportsRequest reportsRequest)
            throws AlchemistException {

        ByteArrayOutputStream os =  alchemistReportsBo.cityReports(reportsRequest.getCityId(),
                SwiggyUtils.getTimeStamp(reportsRequest.getFromDate()),
                SwiggyUtils.getTimeStamp(reportsRequest.getToDate()));
        if (null == os) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "Incorrect/Invalid date.");
        }
        return os.toByteArray();
    }
    @RequestMapping(
            value = "/zone",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = "text/csv"
    )
    @ResponseBody byte[] getReportsZone(
            @RequestBody(required = true) ReportsRequest reportsRequest)
            throws AlchemistException {

        ByteArrayOutputStream os =  alchemistReportsBo.zoneReports(reportsRequest.getZoneIds(),
                SwiggyUtils.getTimeStamp(reportsRequest.getFromDate()),
                SwiggyUtils.getTimeStamp(reportsRequest.getToDate()),String.valueOf(reportsRequest.getCityId()));
        if (null == os) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "Incorrect/Invalid date.");
        }
        return os.toByteArray();
    }

    @RequestMapping(
            value = "/test",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    BaseResponse test(
            @RequestBody(required = false) ReportsRequest reportsRequest)
            throws AlchemistException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date d1 = null;
        Date d2 = null;
        try {
            d1 = format.parse("2016-10-19 15:40:00");
            d2 = format.parse("2016-10-19 15:42:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new BaseResponse(0, String.valueOf(deliveryBoysDao.getLoginTime(216L,new Timestamp(d1.getTime()),new Timestamp(d2.getTime()))));
    }

}
