package com.swiggy.alchemist.queues.publish;

import com.swiggy.alchemist.pojo.DryRunPojo;
import com.swiggy.alchemist.pojo.FailedMessagePojo;
import com.swiggy.alchemist.utility.SwiggyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * Created by RAVISINGH on 22/11/16.
 */
@Component
@Slf4j
public class RabbitmqPublish {

    @Autowired
    @Qualifier("intraRabbitmqTemplate")
    AmqpTemplate amqpTemplate;

    @Value("${alchemist.dry.run.exchange}") private String dryRunExchange;

    @Value("${alchemist.failed.message.exchange}") private String failedMessageExchange;


    @Async
    public void publishDryRunPojo(DryRunPojo dryRunPojo) {
        amqpTemplate.convertAndSend(dryRunExchange,"", SwiggyUtils.jsonEncode(dryRunPojo));
    }

    @Async
    public void publishFailedMessagePojo(FailedMessagePojo failedMessagePojo) {
        amqpTemplate.convertAndSend(failedMessageExchange,"", SwiggyUtils.jsonEncode(failedMessagePojo));
    }
}
