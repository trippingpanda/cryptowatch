package com.swiggy.alchemist.queues.listener;

import com.swiggy.alchemist.enums.EventNames;
import com.swiggy.alchemist.pojo.*;
import com.swiggy.alchemist.queues.publish.RabbitmqPublish;
import com.swiggy.alchemist.scheduler.SchedulerImpl;
import com.swiggy.alchemist.services.eventProcessors.IEventProcessor;
import com.swiggy.alchemist.services.order.OrdersService;
import com.swiggy.alchemist.utility.EmailService;
import com.swiggy.alchemist.utility.SwiggyUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class QueueListener {

    @Autowired
    IEventProcessor eventProcessor;

    @Autowired
    EmailService emailService;

    @Autowired
    OrdersService ordersService;

    @Autowired
    RabbitmqPublish rabbitmqPublisher;

    @Autowired
    SchedulerImpl scheduler;

    @RabbitListener(queues = "swiggy.incentive_status_update")
    public void statusUpdateListener(Object message){
        String messageString = null;
        try {
            messageString = getMessageString(message);
            StatusUpdatePojo statusUpdatePojo = (StatusUpdatePojo) SwiggyUtils.jsonDecode(messageString, StatusUpdatePojo.class);
            log.info(SwiggyUtils.jsonEncode(statusUpdatePojo));
            if (ordersService.doesOrderExists(statusUpdatePojo.getOrderId()) && statusUpdatePojo.getDeliveryBoyId() != null) {
                EventPojo eventPojo;
                switch (statusUpdatePojo.getStatus()) {
                    case "rejected":
                    case "auto_rejected":
                        if(statusUpdatePojo.getOrderAckTime() != null) {
                            eventPojo = EventPojo.builder().eventData(statusUpdatePojo).eventName(EventNames.ORDER_REJECT.getName()).build();
                        } else {
                            log.info("Ignoring the reject status, order ack is null message :{}" ,messageString);
                            return;
                        }
                        break;
                    case "cancelled":
                        eventPojo = EventPojo.builder().eventData(statusUpdatePojo).eventName(EventNames.CANCELLED.getName()).build();
                        break;
                    default:
                        eventPojo = EventPojo.builder().eventData(statusUpdatePojo).eventName(EventNames.ORDER_STATUS_UPDATE.getName()).build();
                        break;
                }
                eventProcessor.processEvent(eventPojo,statusUpdatePojo.getDeliveryBoyId());
            } else {
                log.info("OrderId: {} does not exist ignoring status update",statusUpdatePojo.getOrderId());
            }
        }catch (Exception e){
            log.error("error {}",e);
            rabbitmqPublisher.publishFailedMessagePojo(FailedMessagePojo.builder().type(EventNames.ORDER_STATUS_UPDATE.getName()).failedMessage(message).build());
            emailService.sendEmail("Error for message: "+messageString+" In incentive status update queue Listener", ExceptionUtils.getStackTrace(e));
        }
    }


    @RabbitListener(queues = "swiggy.incentive_new_order")
    public void newOrderListener(Object message){
        String messageString = null;
        try {
            messageString = getMessageString(message);
            log.info("wtf byte new order");
            PortalOrderData portalOrder = (PortalOrderData) SwiggyUtils.jsonDecode(messageString, PortalOrderData.class);
            log.info(SwiggyUtils.jsonEncode(portalOrder));
            EventPojo eventPojo = EventPojo.builder().eventData(portalOrder).eventName(EventNames.NEW_ORDER.getName()).build();
            eventProcessor.processEvent(eventPojo);
        }catch (Exception e){
            log.error("error {}",e);
            rabbitmqPublisher.publishFailedMessagePojo(FailedMessagePojo.builder().type(EventNames.NEW_ORDER.getName()).failedMessage(message).build());
            emailService.sendEmail("Error for message: "+messageString +" in incentive new order listener",ExceptionUtils.getStackTrace(e));
        }
    }

    @RabbitListener(queues = "swiggy.incentive_de_rating")
    public void ratingListener(Object message){
        String messageString = null;
        try {
            messageString = getMessageString(message);
            log.info("wtf byte de rating");
            DeRatingPojo deRatingPojo = (DeRatingPojo) SwiggyUtils.jsonDecode(messageString, DeRatingPojo.class);
            log.info(SwiggyUtils.jsonEncode(deRatingPojo));
            EventPojo eventPojo = EventPojo.builder().eventData(deRatingPojo).eventName(EventNames.DE_RATING.getName()).build();
            eventProcessor.processEvent(eventPojo,Long.parseLong(deRatingPojo.getDeId()));
        }catch (Exception e){
            log.error("error {}",e);
            rabbitmqPublisher.publishFailedMessagePojo(FailedMessagePojo.builder().type(EventNames.DE_RATING.getName()).failedMessage(message).build());
            emailService.sendEmail("Error for message: "+ messageString +" in incentive rating listener",ExceptionUtils.getStackTrace(e));
        }
    }

    @RabbitListener(queues = "swiggy.incentive_cancel_order")
    public void cancelOrderListener(Object message){
        String messageString = null;
        try {
            messageString = getMessageString(message);
            log.info("wtf byte de cancel");
            CancelOrderPojo cancelOrderPojo = (CancelOrderPojo) SwiggyUtils.jsonDecode(messageString, CancelOrderPojo.class);
            log.info(SwiggyUtils.jsonEncode(cancelOrderPojo));
            if (ordersService.doesOrderExists(cancelOrderPojo.getOrderId())) {
                EventPojo eventPojo = EventPojo.builder().eventData(cancelOrderPojo).eventName(EventNames.CANCELLED.getName()).build();
                eventProcessor.processEvent(eventPojo);
            } else {
                log.info("OrderId: {} does not exist ignoring status update",cancelOrderPojo.getOrderId());
            }
        }catch (Exception e){
            log.error("error {}",e);
            rabbitmqPublisher.publishFailedMessagePojo(FailedMessagePojo.builder().type(EventNames.CANCELLED.getName()).failedMessage(message).build());
            emailService.sendEmail("Error for message : "+messageString + "in incentive cancel order listener",ExceptionUtils.getStackTrace(e));
        }
    }

    private String getMessageString(Object message) {
        String messageString = null;
        if (message instanceof String) {
            messageString = (String) message;
        } else if (message instanceof Message) {
            messageString = new String(((Message)message).getBody());
        } else {
            byte[] byte_message = (byte[]) message;
            messageString = new String(byte_message);
        }
        return messageString;
    }

    @RabbitListener(queues = "swiggy.incentive_de_logout",containerFactory = "intraRabbitListenerContainer")
    public void deLogoutListner(Object message){
        String messageString = null;
        try {
            messageString = getMessageString(message);
            log.info("wtf byte de logout");
            DeLogoutPojo deLogoutPojo = (DeLogoutPojo) SwiggyUtils.jsonDecode(messageString, DeLogoutPojo.class);
            log.info(SwiggyUtils.jsonEncode(deLogoutPojo));
            if(!deLogoutPojo.getEvent().equalsIgnoreCase("logged_out")){
                return;
            }
            EventPojo eventPojo = EventPojo.builder().eventData(deLogoutPojo).eventName(EventNames.LOGOUT.getName()).build();
            eventProcessor.processEvent(eventPojo,deLogoutPojo.getDeId());
        }catch (Exception e){
            log.error("error {}",e);
            rabbitmqPublisher.publishFailedMessagePojo(FailedMessagePojo.builder().type(EventNames.LOGOUT.getName()).failedMessage(message).build());
            emailService.sendEmail("Error for mesage : "+ messageString + "in incentive logout listener",ExceptionUtils.getStackTrace(e));
        }
    }

    @RabbitListener(queues = "swiggy.incentive_failed_messages",containerFactory = "intraRabbitListenerContainer")
    public void failedMessageListener(Object message){
        String messageString = null;
        try {
            messageString = getMessageString(message);
            FailedMessagePojo failedMessagePojo = (FailedMessagePojo) SwiggyUtils.jsonDecode(messageString,FailedMessagePojo.class);
            switch (EventNames.get(failedMessagePojo.getType())){
                case ORDER_STATUS_UPDATE :
                    statusUpdateListener(SwiggyUtils.jsonEncode(failedMessagePojo.getFailedMessage()));
                    return;
                case NEW_ORDER:
                    newOrderListener(SwiggyUtils.jsonEncode(failedMessagePojo.getFailedMessage()));
                    return;
                case DE_RATING:
                    ratingListener(SwiggyUtils.jsonEncode(failedMessagePojo.getFailedMessage()));
                    return;
                case CANCELLED:
                    cancelOrderListener(SwiggyUtils.jsonEncode(failedMessagePojo.getFailedMessage()));
                    return;
                case LOGOUT:
                    deLogoutListner(SwiggyUtils.jsonEncode(failedMessagePojo.getFailedMessage()));
                    return;
                case END_OF_DAY:
                    scheduler.runEndOfDay();
                    return;
                case END_OF_WEEK:
                    scheduler.runEndOfWeek();
                    return;
                case END_OF_MONTH:
                    scheduler.runEndOfMonth();
            }
        }catch (Exception e){
            log.error("error {}",e);
            emailService.sendEmail("Error for mesage : "+ messageString + "in incentive failed message listener",ExceptionUtils.getStackTrace(e));
        }
    }

}
