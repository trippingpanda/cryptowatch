package com.swiggy.alchemist;

import com.swiggy.alchemist.configs.AlchemistConfig;
import com.swiggy.alchemist.configs.DeliveryConfig;
import com.swiggy.alchemist.interceptors.RequestIdInterceptor;
import com.swiggy.alchemist.interceptors.RequestTimeInterceptor;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ComponentScan(basePackages = "com.swiggy.alchemist")
@SpringBootApplication
@EnableRabbit
@EnableTransactionManagement
@EnableAspectJAutoProxy
@EnableAutoConfiguration
@EnableScheduling
@Import({AlchemistConfig.class, DeliveryConfig.class})
public class AlchemistApplication extends WebMvcConfigurerAdapter
{
	@Autowired
	private Environment environment;

	@Autowired
	private RequestIdInterceptor requestIdInterceptor;

	@Autowired
	private RequestTimeInterceptor requestTimeInterceptor;

	/*
	 * Queue Configs
	 * @return
	 */

	@Bean
	public ConnectionFactory connectionFactory(){
		com.rabbitmq.client.ConnectionFactory rabbitmqConnectionFactory = new com.rabbitmq.client.ConnectionFactory();
		rabbitmqConnectionFactory.setHost(environment.getProperty("rabbitmq.address"));
		rabbitmqConnectionFactory.setUsername(environment.getProperty("rabbitmq.user"));
		rabbitmqConnectionFactory.setPassword(environment.getProperty("rabbitmq.password"));
		return new CachingConnectionFactory(rabbitmqConnectionFactory);
	}

	@Bean
	public SimpleRabbitListenerContainerFactory myRabbitListenerContainerFactory() {
		SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
		factory.setConnectionFactory(connectionFactory());
		factory.setConcurrentConsumers(2);
		factory.setMaxConcurrentConsumers(5);
		return factory;
	}

	@Bean(name = "intraConnectionFactory")
	public ConnectionFactory intraConnectionFactory(){
		com.rabbitmq.client.ConnectionFactory rabbitmqConnectionFactory = new com.rabbitmq.client.ConnectionFactory();
		rabbitmqConnectionFactory.setHost(environment.getProperty("rabbitmq.intra.address"));
		rabbitmqConnectionFactory.setUsername(environment.getProperty("rabbitmq.intra.user"));
		rabbitmqConnectionFactory.setPassword(environment.getProperty("rabbitmq.intra.password"));
		return new CachingConnectionFactory(rabbitmqConnectionFactory);
	}

	@Bean(name = "intraRabbitListenerContainer")
	public SimpleRabbitListenerContainerFactory intraRabbitListenerContainerFactory() {
		SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
		factory.setConnectionFactory(intraConnectionFactory());
		factory.setConcurrentConsumers(2);
		factory.setMaxConcurrentConsumers(5);
		return factory;
	}

	@Bean(name = "intraRabbitmqTemplate")
	public AmqpTemplate rabbitTemplate() {
		RabbitTemplate template = new RabbitTemplate(intraConnectionFactory());
		RetryTemplate retryTemplate = new RetryTemplate();
		ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
		retryTemplate.setBackOffPolicy(backOffPolicy);
		template.setRetryTemplate(retryTemplate);
		return template;
	}

	@Bean
	public ServletRegistrationBean dispatcherRegistration(DispatcherServlet dispatcherServlet) {
		ServletRegistrationBean registration = new ServletRegistrationBean(
				dispatcherServlet);
		registration.addUrlMappings("/");
		return registration;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(requestIdInterceptor).addPathPatterns("/**");
		registry.addInterceptor(requestTimeInterceptor).addPathPatterns("/**");
		super.addInterceptors(registry);
	}


	public static void main(String[] args)  throws Exception {
		SpringApplication.run(AlchemistApplication.class, args);
		/*
		File file = new File("/Users/RAVISINGH/Documents/Swiggy-Workspace/alchemist/src/main/antlr/test.txt");

		FileInputStream fis = new FileInputStream(file);
		byte[] data = new byte[(int) file.length()];
		fis.read(data);
		fis.close();
		String str = new String(data, "UTF-8");
		System.out.println(AntlrHelper.parseRuleDomainString(str));
		*/
	}


}
