package com.swiggy.alchemist.utility;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.alchemist.pojo.DeAppRulePojo;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SwiggyUtils {
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String jsonEncode(Object o)  {
        if (o == null)
            return "";
        try {
            return objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object jsonDecode(String s, Class c) {
        Object u = null;
        if(s!=null) {
            try {
                u = objectMapper.reader(c)
                        .readValue(s);
            } catch (Exception e) {
                //do someshit
            }
        }
        return u;
    }

    public static Object jsonDecode(byte[] s, Class c) {
        Object u = null;
        if(s!=null) {
            try {
                u = objectMapper.reader(c)
                        .readValue(s);
            } catch (Exception e) {
                //do someshit
            }
        }
        return u;
    }


    public static Integer getInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return 0;
        }
    }

    public static boolean isDouble(String input) {
        try {
            Double.parseDouble(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isDateValid(String dateString) {
            DateFormat df = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss", Locale.US );
            try {
                df.parse(dateString);
                return true;
            } catch ( ParseException exc ) {
            }

            return false;
    }
    public static Timestamp getTimeStamp(String dateString) {
        Timestamp timestamp = null;
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date parsedDate = dateFormat.parse(dateString);
            timestamp = new java.sql.Timestamp(parsedDate.getTime());
        }catch(Exception e){

        }

        return timestamp;
    }

    public static HashMap getHashMapFromJsonObject(String object){
        if(object == null) {
            return new HashMap();
        }
        try {
            return objectMapper.readValue(object,HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertRulePojoToString(DeAppRulePojo deAppRulePojo) {
        return deAppRulePojo.getRuleName() + ":" +deAppRulePojo.getBonus();
    }

    public static Timestamp getStartDayTime(int day) {
        Calendar date = new GregorianCalendar();
// reset hour, minutes, seconds and millis
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 5);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        date.add(Calendar.DAY_OF_MONTH, -1*(day));
        return new Timestamp(date.getTime().getTime());
    }
    public static Timestamp getEndDayTime(int day) {
        Calendar date = new GregorianCalendar();
// reset hour, minutes, seconds and millis
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 5);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        date.add(Calendar.DAY_OF_MONTH, 1 - 1*(day));
        return new Timestamp(date.getTime().getTime());
    }

    public static Timestamp[] getDaysOfWeek(int week) {
        Timestamp[] timestamps =  new Timestamp[8];
        Calendar date = new GregorianCalendar(), tempDate;
        date.add(Calendar.WEEK_OF_YEAR, -1*week);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 5);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        tempDate = (Calendar)date.clone();
        tempDate.add(Calendar.DAY_OF_MONTH, 1);
        timestamps[0] = new Timestamp(tempDate.getTime().getTime());
        timestamps[1] = new Timestamp(date.getTime().getTime());
        for (int i=2;i<8;i++) {
            tempDate = date;
            tempDate.add(Calendar.DAY_OF_MONTH, -1);
            timestamps[i] = new Timestamp(tempDate.getTime().getTime());
        }
        return timestamps;
    }
    public static Timestamp[] getTimesForMonth() {
        Timestamp[] timestamps =  new Timestamp[5];
        Calendar date = new GregorianCalendar(), tempDate;
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 5);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
        date.set(Calendar.DAY_OF_WEEK,date.getFirstDayOfWeek());
        tempDate = (Calendar)date.clone();
        tempDate.add(Calendar.WEEK_OF_YEAR, 1);
        timestamps[0] = new Timestamp(tempDate.getTime().getTime());
        timestamps[1] = new Timestamp(date.getTime().getTime());
        for (int i=2;i<5;i++) {
            tempDate = date;
            tempDate.add(Calendar.WEEK_OF_YEAR, -1);
            timestamps[i] = new Timestamp(tempDate.getTime().getTime());
        }
        return timestamps;
    }

}
