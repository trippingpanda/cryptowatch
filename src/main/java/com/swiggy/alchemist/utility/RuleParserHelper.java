package com.swiggy.alchemist.utility;

import com.swiggy.alchemist.constants.AlchemistConstants;
import com.swiggy.alchemist.constants.EntityConstants;
import com.swiggy.alchemist.exceptions.AlchemistException;
import com.swiggy.alchemist.services.evaluator.EvaluatorServiceImpl;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by RAVISINGH on 22/09/16.
 */
@Component
public class RuleParserHelper {
    @Autowired
    EvaluatorServiceImpl evaluatorService;

    public boolean validateRuleString(HashMap<String,String> keyValuePair) throws AlchemistException{

        for(String entity: EntityConstants.obligatoryEntities) {
            if(!keyValuePair.containsKey(entity)) {
                throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, entity + " not present");
            }
        }
        if(keyValuePair.get(EntityConstants.NAME).length() > AlchemistConstants.NAME_LENGTH_LIMIT) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "Name length not within limits");
        }

        if(keyValuePair.get(EntityConstants.DESCRIPTION).length() > AlchemistConstants.DESCRIPTION_LENGTH_LIMIT) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "description length not within limits");
        }

        if(keyValuePair.containsKey(EntityConstants.FROM) &&
                !SwiggyUtils.isDateValid(keyValuePair.get(EntityConstants.FROM))) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "From date not in proper format");
        }

        if(keyValuePair.containsKey(EntityConstants.TO) &&
                !SwiggyUtils.isDateValid(keyValuePair.get(EntityConstants.TO))) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "To date not in proper format");
        }
        if (keyValuePair.containsKey(EntityConstants.FROM) && keyValuePair.containsKey(EntityConstants.TO)) {
            Timestamp fromDate, toDate;
            fromDate = SwiggyUtils.getTimeStamp(keyValuePair.get(EntityConstants.FROM));
            toDate = SwiggyUtils.getTimeStamp(keyValuePair.get(EntityConstants.TO));
            if (!(fromDate.after(new Date()) && toDate.after(new Date()))) {
                throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "from or to date not after now");
            }
            if (fromDate.compareTo(toDate) >= 0) {
                throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "from not less than to ");
            }
            System.out.println(fromDate.getMinutes() + " " + toDate.getMinutes() + " " + fromDate.getSeconds() + " " +
                    toDate.getSeconds());
            if (fromDate.getMinutes() % 15 != 0 || toDate.getMinutes() % 15 != 0 || fromDate.getSeconds() != 0 ||
                    toDate.getSeconds() != 0) {
                throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "from and to time not multiple of 15 minutes");
            }
        }

        if(keyValuePair.containsKey(EntityConstants.STATUS_MESSAGE) &&
                (keyValuePair.get(EntityConstants.STATUS_MESSAGE).length() > AlchemistConstants.STATUS_MESSAGE_LIMIT)) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "status message not within limits");
        }

        if(keyValuePair.containsKey(EntityConstants.SCOPE) ) {
            String scope = keyValuePair.get(EntityConstants.SCOPE);
            if (!scope.equalsIgnoreCase("day") && !scope.equalsIgnoreCase("week") && !scope.equalsIgnoreCase("month")
                    && !scope.equalsIgnoreCase("year")) {
                throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "Scope is not valid");
            }
        }

//        if(keyValuePair.containsKey(EntityConstants.TIMESLOT) && !keyValuePair.containsKey(EntityConstants.SCOPE)) {
//            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "Timeslot mentioned without scope");
//        }

        if(keyValuePair.containsKey(EntityConstants.COMPUTE_ON) &&
                (!EntityConstants.computeOnFields.stream().filter(s -> s.equalsIgnoreCase(keyValuePair.get(EntityConstants.COMPUTE_ON)))
                        .findFirst().isPresent())) {
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "Compute on field not valid");
        }

        if(!evaluatorService.isValidExpression(keyValuePair.get(EntityConstants.CONDITION), true)){
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "Condition expression incorrect");
        }

        if(!evaluatorService.isValidExpression(keyValuePair.get(EntityConstants.BONUS), false)){
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST, "Bonus expression incorrect");
        }

        return true;
    }
}
