package com.swiggy.alchemist.utility;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class EmailService {

    @Autowired
    private Environment environment;


    private String fromEmail;
    private String toEmail;
    private String username;
    private String password;

    @PostConstruct
    public void initialize(){
        fromEmail = environment.getProperty("email_from");
        toEmail = environment.getProperty("email_to");
        username = environment.getProperty("email_username");
        password = environment.getProperty("email_password");
    }
    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

    @Async
    public void sendEmail(String subject, String message) {
        try {
            Email email = new SimpleEmail();
            email.setHostName("smtp.googlemail.com");
            email.setSmtpPort(465);
            email.setAuthenticator(new DefaultAuthenticator(username,password));
            email.setSSLOnConnect(true);
            email.setSubject(environment.getProperty("email.environment")+subject);
            email.setFrom(fromEmail);
            email.setMsg(message);
            email.addTo(toEmail.split(","));
            email.send();
        } catch (Exception e) {
            logger.error("Error while sending email subject, body" + subject + " "+ message);
        }
    }


}
