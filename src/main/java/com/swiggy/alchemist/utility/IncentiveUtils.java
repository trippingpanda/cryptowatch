package com.swiggy.alchemist.utility;

import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Slf4j
public class IncentiveUtils {

    public static String getDateForDailyPerformanceData(long time) {
        Date date = new Date(time) ;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-w-dd");
        String dateFormat = simpleDateFormat.format(new Date(time));
        if(isSunday(date)){
            String[] split = dateFormat.split("-");
            split[2] = String.valueOf(Integer.valueOf(split[2])-1);
            return String.join("-",split);
        }
        return dateFormat;
    }

    public static Integer getDayOfWeekFromDateString(String dateString) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-w-dd");
        try {
            LocalDateTime dateTime = LocalDateTime.ofInstant(format.parse(dateString).toInstant(), ZoneOffset.ofHoursMinutes(5,30));
            return dateTime.getDayOfWeek().getValue();
        } catch (ParseException e) {
            log.error("error parsing date time {} {}",dateString,e);
        }
        return null;
    }

    private static boolean isSunday(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E");
        return simpleDateFormat.format(date).equalsIgnoreCase("sun");
    }

    public static String getQuarterHourForPerfData(long time) {
        Date date = new Date(time) ;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-w-dd-HH");
        String dateFormat =  simpleDateFormat.format(new Date(time)) + "-" + getQuarterOfHour(time);
        if(isSunday(date)){
            String[] split = dateFormat.split("-");
            split[2] = String.valueOf(Integer.valueOf(split[2])-1);
            return String.join("-",split);
        }
        return dateFormat;
    }

    public static String getHourForPerfData(long time){
        Date date = new Date(time) ;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-w-dd-HH");
        String dateFormat =  simpleDateFormat.format(new Date(time));
        if(isSunday(date)){
            String[] split = dateFormat.split("-");
            split[2] = String.valueOf(Integer.valueOf(split[2])-1);
            return String.join("-",split);
        }
        return dateFormat;
    }

    private static String getQuarterOfHour(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm");
        int minute = Integer.valueOf(simpleDateFormat.format(new Date(time)));
        if(minute >= 0 && minute < 15)
            return "1";
        if(minute >= 15 && minute < 30)
            return "2";
        if(minute >= 30 && minute < 45)
            return "3";
        return "4";
    }

    public static String getWeekForWeeklyPerformanceData(long time) {
        Date date = new Date(time) ;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-w");
        String dateFormat  = simpleDateFormat.format(new Date(time));
        if(isSunday(date)){
            String[] split = dateFormat.split("-");
            split[2] = String.valueOf(Integer.valueOf(split[2])-1);
            return String.join("-",split);
        }
        return dateFormat;
    }

    public static String getMonthForMonthlyPerformanceData(long time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        return simpleDateFormat.format(new Date(time));
    }

    public static Timestamp getTimeStampFromTimeSlot(Integer hhMM, LocalDateTime localDateTime) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,localDateTime.getYear());
        cal.set(Calendar.DAY_OF_MONTH,localDateTime.getDayOfMonth());
        cal.set(Calendar.MONTH,localDateTime.getMonthValue()-1);
        cal.set(Calendar.HOUR_OF_DAY,hhMM / 100);
        cal.set(Calendar.MINUTE,hhMM % 100);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);
        return new java.sql.Timestamp(cal.getTime().getTime());
    }

    public static long getTimePassedInDays(Timestamp timestamp){
        try {
            long millisecondsPassed = Instant.now().toEpochMilli() - timestamp.getTime();
            return TimeUnit.MILLISECONDS.toDays(millisecondsPassed);
        }catch (Exception e){
            return 0;
        }

    }

    public static List<String> getQuartersForStartHour(Integer start,String dateForDailyPerfData) {
        int startHour = start/100;
        String hourlyPerfString = dateForDailyPerfData+"-"+(startHour>=10?startHour:"0"+startHour)+"-";
        int minute = start%100;
        if(minute == 15)
            return new ArrayList<>(Arrays.asList(hourlyPerfString + 2,hourlyPerfString + 3, hourlyPerfString +4));
        if(minute == 30)
            return new ArrayList<>(Arrays.asList(hourlyPerfString+3,hourlyPerfString+4));
        if(minute == 45)
            return new ArrayList<>(Arrays.asList(hourlyPerfString+4));
        return new ArrayList<>();
    }

    public static List<String> getQuartersForEndHour(Integer end, String dateForDailyPerfData) {
        int endHour = end/100;
        String hourlyPerfString = dateForDailyPerfData+"-"+(endHour>=10?endHour:"0"+endHour)+"-";
        int minute = end%100;
        if(minute == 15)
            return new ArrayList<>(Arrays.asList(hourlyPerfString + 1));
        if(minute == 30)
            return new ArrayList<>(Arrays.asList(hourlyPerfString + 1,hourlyPerfString+2));
        if(minute == 45)
            return new ArrayList<>(Arrays.asList(hourlyPerfString + 1,hourlyPerfString+2,hourlyPerfString+3));
        return new ArrayList<>();
    }
}
