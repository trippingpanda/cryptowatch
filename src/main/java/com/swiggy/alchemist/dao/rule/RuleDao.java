package com.swiggy.alchemist.dao.rule;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by RAVISINGH on 19/10/16.
 */
public interface RuleDao {

    List<Object[]> getDailyMapForDes(List<Long> deIds, Timestamp startTime, Timestamp endTime);

    List<Object[]> getWeeklyMapForDes(List<Long> deIds, Timestamp startTime, Timestamp endTime);

    List<Object[]> getMonthlyMapForDes(List<Long> deIds, Timestamp startTime, Timestamp endTime);

    List<Object[]> getDailyMapForToday(List<Long> deIds);

    List<Object[]> getWeeklyMapForToday(List<Long> deIds);

    List<Object[]> getMonthlyMapForToday(List<Long> deIds);

    Long getDailyBonus(Long deId, Timestamp startTime, Timestamp endTime);

    Long getWeeklyBonus(Long deId, Timestamp startTime, Timestamp endTime);

    Long getMonthlyBonus(Long deId, Timestamp startTime, Timestamp endTime);



}
