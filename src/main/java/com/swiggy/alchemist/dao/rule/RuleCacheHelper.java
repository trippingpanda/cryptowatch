package com.swiggy.alchemist.dao.rule;

import com.swiggy.alchemist.aspects.Redis;
import com.swiggy.alchemist.aspects.RedisKey;
import com.swiggy.alchemist.db.alchemist.repository.DeRuleMapDailyRepository;
import com.swiggy.alchemist.db.alchemist.repository.DeRuleMapMonthlyRepository;
import com.swiggy.alchemist.db.alchemist.repository.DeRuleMapWeeklyRepository;
import com.swiggy.alchemist.services.caching.Cache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;

/**
 * Created by RAVISINGH on 15/11/16.
 */
@Repository
@Transactional
@Slf4j
public class RuleCacheHelper {
    @Autowired
    DeRuleMapDailyRepository deRuleMapDailyRepository;

    @Autowired
    DeRuleMapWeeklyRepository deRuleMapWeeklyRepository;
    @Autowired
    DeRuleMapMonthlyRepository deRuleMapMonthlyRepository;

    @Value("${redis.cache.minute}") private static final int oneMinuteTimeout = 60;
    @Value("${redis.cache.twoHours}") private static final int twoHoursTimeout = 7200;
    @Value("${redis.cache.fourHours}") private static final int fourHoursTimeout =14400;

    @Redis(cacheKeyPrefix = "BONUS_SUM_DAILY_ARCHIVED_",cacheService = Cache.DEFAULT,defaultExpiry = fourHoursTimeout)
    public Long getDailyBonusSum(@RedisKey Long deId,@RedisKey Timestamp startTime,@RedisKey Timestamp endTime) {
        log.info("going to db for data");
        Long result = deRuleMapDailyRepository.bonusSum(deId, startTime, endTime);
        if (result == null) {
            result = 0L;
        }
        return result;
    }
    @Redis(cacheKeyPrefix = "BONUS_SUM_DAILY_UNARCHIVED_",cacheService = Cache.DEFAULT,defaultExpiry = oneMinuteTimeout)
    public Long getDailyBonusSum(@RedisKey Long deId) {
        log.info("going to db for data");
        Long result = deRuleMapDailyRepository.bonusSum(deId);
        if (result == null) {
            result = 0L;
        }
        return result;
    }

    @Redis(cacheKeyPrefix = "BONUS_SUM_WEEKLY_ARCHIVED_",cacheService = Cache.DEFAULT,defaultExpiry = fourHoursTimeout)
    public Long getWeeklyBonusSum(@RedisKey Long deId,@RedisKey Timestamp startTime,@RedisKey Timestamp endTime) {
        log.info("going to db for data");
        Long result = deRuleMapWeeklyRepository.bonusSum(deId, startTime, endTime);
        if (result == null) {
            result = 0L;
        }
        return result;
    }
    @Redis(cacheKeyPrefix = "BONUS_SUM_WEEKLY_UNARCHIVED_",cacheService = Cache.DEFAULT,defaultExpiry = twoHoursTimeout)
    public Long getWeeklyBonusSum(@RedisKey Long deId) {
        log.info("going to db for data");
        Long result = deRuleMapWeeklyRepository.bonusSum(deId);
        if (result == null) {
            result = 0L;
        }
        return result;
    }

    @Redis(cacheKeyPrefix = "BONUS_SUM_MONTHLY_ARCHIVED_",cacheService = Cache.DEFAULT,defaultExpiry = fourHoursTimeout)
    public Long getMonthlyBonusSum(Long deId, Timestamp startTime, Timestamp endTime) {
        log.info("going to db for data");
        Long result = deRuleMapMonthlyRepository.bonusSum(deId, startTime, endTime);
        if (result == null) {
            result = 0L;
        }
        return result;
    }
    @Redis(cacheKeyPrefix = "BONUS_SUM_MONTHLY_UNARCHIVED_",cacheService = Cache.DEFAULT,defaultExpiry = twoHoursTimeout)
    public Long getMonthlyBonusSum(Long deId) {
        log.info("going to db for data");
        Long result = deRuleMapMonthlyRepository.bonusSum(deId);
        if (result == null) {
            result = 0L;
        }
        return result;
    }
}
