package com.swiggy.alchemist.dao.rule;

import com.swiggy.alchemist.aspects.Redis;
import com.swiggy.alchemist.aspects.RedisKey;
import com.swiggy.alchemist.db.alchemist.repository.DeRuleMapDailyRepository;
import com.swiggy.alchemist.db.alchemist.repository.DeRuleMapMonthlyRepository;
import com.swiggy.alchemist.db.alchemist.repository.DeRuleMapWeeklyRepository;
import com.swiggy.alchemist.services.caching.Cache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

/**
 * Created by RAVISINGH on 19/10/16.
 */
@Repository
@Transactional
@Slf4j
public class RuleDaoImpl implements RuleDao {

    @Value("${redis.cache.minute}") private static final int oneMinuteTimeout = 60;
    @Value("${redis.cache.twoHours}") private static final int twoHoursTimeout = 7200;
    @Value("${redis.cache.fourHours}") private static final int fourHoursTimeout =14400;

    @Autowired
    DeRuleMapDailyRepository deRuleMapDailyRepository;

    @Autowired
    DeRuleMapWeeklyRepository deRuleMapWeeklyRepository;
    @Autowired
    DeRuleMapMonthlyRepository deRuleMapMonthlyRepository;

    @Autowired
    RuleCacheHelper ruleCacheHelper;

    @Override
    @Redis(cacheKeyPrefix = "DAILY_MAP_ARCHIVED_",cacheService = Cache.DEFAULT,defaultExpiry = fourHoursTimeout)
    public List<Object[]> getDailyMapForDes(@RedisKey List<Long> deIds,@RedisKey Timestamp startTime,@RedisKey Timestamp endTime){
        log.info("going to db for data daily map archived");
        return deRuleMapDailyRepository.getDailyMapForDes(deIds,startTime,endTime);
    }

    @Override
    @Redis(cacheKeyPrefix = "WEEKLY_MAP_ARCHIVED_",cacheService = Cache.DEFAULT,defaultExpiry = fourHoursTimeout)
    public List<Object[]> getWeeklyMapForDes(@RedisKey List<Long> deIds,@RedisKey Timestamp startTime,@RedisKey Timestamp endTime){
        log.info("going to db for data weekly map archived");
        return deRuleMapWeeklyRepository.getWeeklyMapForDes(deIds,startTime,endTime);
    }

    @Override
    @Redis(cacheKeyPrefix = "MONTHLY_MAP_ARCHIVED_",cacheService = Cache.DEFAULT,defaultExpiry = fourHoursTimeout)
    public List<Object[]> getMonthlyMapForDes(@RedisKey List<Long> deIds,@RedisKey Timestamp startTime,@RedisKey Timestamp endTime){
        log.info("going to db for data monthly map archived");
        return deRuleMapMonthlyRepository.getMonthlyMapForDes(deIds,startTime,endTime);
    }

    @Override
    @Redis(cacheKeyPrefix = "DAILY_MAP_UNARCHIVED_",cacheService = Cache.DEFAULT,defaultExpiry = oneMinuteTimeout)
    public List<Object[]> getDailyMapForToday(@RedisKey List<Long> deIds) {
        log.info("going to db for data daily map unarchived");
        return deRuleMapDailyRepository.getDailyMapForDesToday(deIds);
    }

    @Override
    @Redis(cacheKeyPrefix = "WEEKLY_MAP_UNARCHIVED_",cacheService = Cache.DEFAULT,defaultExpiry = twoHoursTimeout)
    public List<Object[]> getWeeklyMapForToday(@RedisKey List<Long> deIds){
        log.info("going to db for data weekly map unarchived");
        return deRuleMapWeeklyRepository.getWeeklyMapForDesToday(deIds);
    }

    @Override
    @Redis(cacheKeyPrefix = "MONTHLY_MAP_UNARCHIVED_",cacheService = Cache.DEFAULT,defaultExpiry = twoHoursTimeout)
    public List<Object[]> getMonthlyMapForToday( @RedisKey List<Long> deIds){
        log.info("going to db for data monthly map unarchived");
        return deRuleMapMonthlyRepository.getMonthlyMapForDesToday(deIds);
    }

    public Long getDailyBonus(Long deId, Timestamp startTime, Timestamp endTime) {
        log.info("Daily bonus started");
        Long totalBonus = ruleCacheHelper.getDailyBonusSum(deId, startTime, endTime);
        if (endTime.compareTo(new Timestamp(Calendar.getInstance().getTime().getTime())) > 0) {
            if (totalBonus != null) {
                log.info(totalBonus+ " daily bonus " +deId);
                Long dailSum =  ruleCacheHelper.getDailyBonusSum(deId);
                if(dailSum!= null) {
                    totalBonus += dailSum;
                }
            }else {
                totalBonus = ruleCacheHelper.getDailyBonusSum(deId);
            }
        }
        if (totalBonus == null) {
            return  0L;
        }
        log.info("Daily bonus ended");
        return totalBonus;
    }

    public Long getWeeklyBonus(Long deId, Timestamp startTime, Timestamp endTime) {
        log.info("Weekly bonus started");
        Long totalBonus = ruleCacheHelper.getWeeklyBonusSum(deId, startTime, endTime);
        if (endTime.compareTo(new Timestamp(Calendar.getInstance().getTime().getTime())) > 0) {
            if (totalBonus != null) {
                Long dailSum =  ruleCacheHelper.getWeeklyBonusSum(deId);
                if(dailSum!= null) {
                    totalBonus += dailSum;
                }
            }else {
                totalBonus = ruleCacheHelper.getWeeklyBonusSum(deId);
            }
        }
        if (totalBonus == null) {
            return  0L;
        }
        log.info("Weekly bonus ended");
        return totalBonus;
    }

    public Long getMonthlyBonus(Long deId,Timestamp startTime,Timestamp endTime) {
        log.info("Monthly bonus started");
        Long totalBonus = ruleCacheHelper.getMonthlyBonusSum(deId, startTime, endTime);
        if (endTime.compareTo(new Timestamp(Calendar.getInstance().getTime().getTime())) > 0) {
            if (totalBonus != null) {
                Long dailSum =  ruleCacheHelper.getMonthlyBonusSum(deId);
                if(dailSum!= null) {
                    totalBonus += dailSum;
                }
            }else {
                totalBonus = ruleCacheHelper.getMonthlyBonusSum(deId);
            }
        }
        if (totalBonus == null) {
            return  0L;
        }
        log.info("Monthly bonus ended");
        return totalBonus;
    }
}
