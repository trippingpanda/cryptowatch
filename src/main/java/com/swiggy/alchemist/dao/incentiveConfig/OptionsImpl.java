package com.swiggy.alchemist.dao.incentiveConfig;

import com.swiggy.alchemist.services.caching.CacheService;
import com.swiggy.alchemist.services.caching.OptionCache;
import com.swiggy.alchemist.services.caching.OptionMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;



@Component
public class OptionsImpl implements Options {

    @Autowired
    CacheService<HashMap<String,String>> optionTemplate;
    @Autowired
    OptionCache optionCache;

    @Override
    public String getOption(String key, String defaultObject) {
        OptionMap option = optionCache.getEntry(key);
        String val = option.getOption(key);
        if(val == null)
            return defaultObject;
        else return val;
    }

    @Override
    public Integer getOptionAsInt(String s, int defaultObject) {
        try {
            return Integer.parseInt(getOption(s, String.valueOf(defaultObject)));
        }catch (NumberFormatException e){
            return defaultObject;
        }
    }

    @Override
    public Long getOptionAsLong(String s, long defaultObject) {
        try {
            return Long.parseLong(getOption(s, String.valueOf(defaultObject)));
        } catch (NumberFormatException e) {
            return defaultObject;
        }
    }

    @Override
    public Float getOptionAsFloat(String s, float defaultObject) {
        try {
            return Float.parseFloat(getOption(s, String.valueOf(defaultObject)));
        } catch (NumberFormatException e) {
            return defaultObject;
        }
    }

    @Override
    public boolean getOptionAsBoolean(String s, boolean defaultObject) {
        return Boolean.parseBoolean(getOption(s,String.valueOf(defaultObject)));
    }
}
