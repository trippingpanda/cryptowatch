package com.swiggy.alchemist.dao.incentiveConfig;

/**
 * Created by atulagrawal1 on 10/08/15.
 */
public interface Options {

    String getOption(String s,String defaultObject);

    Integer getOptionAsInt(String s, int defaultObject);

    Long getOptionAsLong(String s,long defaultObject);

    Float getOptionAsFloat(String s, float defaultObject);

    boolean getOptionAsBoolean(String s, boolean defaultObject);
}
