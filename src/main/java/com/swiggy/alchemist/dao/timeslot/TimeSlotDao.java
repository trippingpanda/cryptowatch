package com.swiggy.alchemist.dao.timeslot;

import com.swiggy.alchemist.db.alchemist.entities.DeTimeslotsEntity;
import com.swiggy.alchemist.pojo.TimeslotData;

import java.util.List;

/**
 * Created by RAVISINGH on 28/09/16.
 */
public interface TimeSlotDao {
    Long addTimeSlot(TimeslotData timeslotData);

    List<Long> getValidTimeSlots(long time);
    public void addTimeSlotMap(Long ruleId, List<Long> timeSlotIds);

    List<Long> getTimeSlotsForRule(long ruleId);

    DeTimeslotsEntity getTimeSlotById(Long id);
}
