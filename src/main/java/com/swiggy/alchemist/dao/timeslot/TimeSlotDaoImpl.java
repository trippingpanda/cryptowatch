package com.swiggy.alchemist.dao.timeslot;

import com.swiggy.alchemist.db.alchemist.entities.DeTimeslotsEntity;
import com.swiggy.alchemist.db.alchemist.entities.TimeSlotRuleMapEntity;
import com.swiggy.alchemist.pojo.TimeslotData;
import com.swiggy.alchemist.db.alchemist.repository.TimeSlotRespository;
import com.swiggy.alchemist.db.alchemist.repository.TimeSlotRuleMapRespository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by RAVISINGH on 28/09/16.
 */
@Repository
@Transactional
@Slf4j
public class TimeSlotDaoImpl implements TimeSlotDao{
    @Autowired
    TimeSlotRespository timeSlotRespository;
    @Autowired
    TimeSlotRuleMapRespository timeSlotRuleMapRespository;

    public Long addTimeSlot(TimeslotData timeslotData) {
        DeTimeslotsEntity deTimeslotsEntity;
        List<String> times = Arrays.asList(timeslotData.getTimeString().split("-"));
        int startTime = Integer.parseInt(times.get(0));
        int endTime = Integer.parseInt(times.get(1));
        String weekdays = timeslotData.getDaysString();
        List<DeTimeslotsEntity> deTimeslotsEntities = timeSlotRespository.
                findByStartTimeEqualsAndEndTimeEqualsAndWeekdaysEquals(startTime,endTime,weekdays);
        if(deTimeslotsEntities.size()== 0) {
            deTimeslotsEntity = new DeTimeslotsEntity();
            deTimeslotsEntity.setEndTime(endTime);
            deTimeslotsEntity.setStartTime(startTime);
            deTimeslotsEntity.setWeekdays(weekdays);
            timeSlotRespository.save(deTimeslotsEntity);
        }else {
            deTimeslotsEntity = deTimeslotsEntities.get(0);
        }
        return deTimeslotsEntity.getId();
    }

    public void addTimeSlotMap(Long ruleId, List<Long> timeSlotIds) {
        timeSlotRuleMapRespository.deleteByRuleId(ruleId);
        log.info("deleted mapping ..........."+ ruleId);
        for (Long timeSlotId : timeSlotIds) {
            TimeSlotRuleMapEntity timeSlotRuleMapEntity = new TimeSlotRuleMapEntity();
            timeSlotRuleMapEntity.setRuleId(ruleId);
            timeSlotRuleMapEntity.setTimeSlotId(timeSlotId);
            try {
                timeSlotRuleMapRespository.save(timeSlotRuleMapEntity);
            } catch (ConstraintViolationException e) {
                log.info("Error while inserting in DB");
            }
        }
    }

    @Override
    public List<Long> getTimeSlotsForRule(long ruleId) {
        return timeSlotRuleMapRespository.findTimeSlotIdByRuleId(ruleId);
    }

    @Override
    public DeTimeslotsEntity getTimeSlotById(Long id) {
        return timeSlotRespository.findOne(id);
    }

    @Override
    public List<Long> getValidTimeSlots(long time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HHmm");
        String format = dateFormat.format(new Date(time));
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault());
        int value = localDateTime.getDayOfWeek().getValue(); // returns 1 for monday.
        return timeSlotRespository.findByTimeSlot(Integer.valueOf(format),String.valueOf(value));
    }
}
