package com.swiggy.alchemist.dao.dePayments;

import com.swiggy.alchemist.db.alchemist.entities.DeRuleMapDailyEntity;
import com.swiggy.alchemist.db.alchemist.entities.DeRuleMapMonthlyEntity;
import com.swiggy.alchemist.db.alchemist.entities.DeRuleMapWeeklyEntity;
import com.swiggy.alchemist.db.alchemist.entities.IncentiveRulesEntity;
import com.swiggy.alchemist.db.alchemist.repository.DeRuleMapDailyRepository;
import com.swiggy.alchemist.db.alchemist.repository.DeRuleMapMonthlyRepository;
import com.swiggy.alchemist.db.alchemist.repository.DeRuleMapWeeklyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.time.Instant;

@Component
@Transactional
@Slf4j
public class DePaymentsDaoImpl implements DePaymentsDao {

    @Autowired
    private DeRuleMapDailyRepository deRuleMapDailyRepository;

    @Autowired
    private DeRuleMapWeeklyRepository deRuleMapWeeklyRepository;

    @Autowired
    private DeRuleMapMonthlyRepository deRuleMapMonthlyRepository;

    @Override
    public void archiveDailyBonusPayments() {
        deRuleMapDailyRepository.archivePayments(Timestamp.from(Instant.now()));
    }

    @Override
    public void archiveWeeklyBonusPayments() {
        deRuleMapWeeklyRepository.archivePayments(Timestamp.from(Instant.now()));
    }

    @Override
    public void archiveMonthlyBonusPayments() {
        deRuleMapMonthlyRepository.archivePayments(Timestamp.from(Instant.now()));
    }

    @Override
    public void updateDailyIncentiveBonusPayment(Long deId, IncentiveRulesEntity incentiveRule, Double bonusAmount,
                                                 String date, boolean isCompleted, String orderId) {
        DeRuleMapDailyEntity deRuleMapDailyEntity = new DeRuleMapDailyEntity();
        deRuleMapDailyEntity.setDeId(deId);
        deRuleMapDailyEntity.setBonus(bonusAmount);
        deRuleMapDailyEntity.setCompleted(isCompleted ? (byte)1:0);
        deRuleMapDailyEntity.setCount(1);
        deRuleMapDailyEntity.setRuleId(incentiveRule.getId());
        deRuleMapDailyEntity.setCreatedOn(date);
        deRuleMapDailyEntity.setOrders(orderId);
        deRuleMapDailyEntity.setCreatedAt(Timestamp.from(Instant.now()));
        deRuleMapDailyEntity.setUpdatedAt(Timestamp.from(Instant.now()));
        deRuleMapDailyRepository.save(deRuleMapDailyEntity);
        log.info("Created an daily bonus entry for de {} for rule {} with bonus {} created at {}",deId,incentiveRule.getId(),bonusAmount,date);
    }

    @Override
    public void updateWeeklyIncentiveBonusPayment(Long deId, IncentiveRulesEntity incentiveRule, Double bonusAmount, String week) {
        DeRuleMapWeeklyEntity deRuleMapWeeklyEntity = new DeRuleMapWeeklyEntity();
        deRuleMapWeeklyEntity.setRuleId(incentiveRule.getId());
        deRuleMapWeeklyEntity.setCount(1);
        deRuleMapWeeklyEntity.setCompleted((byte)1);
        deRuleMapWeeklyEntity.setBonus(bonusAmount);
        deRuleMapWeeklyEntity.setCreatedOn(week);
        deRuleMapWeeklyEntity.setDeId(deId);
        deRuleMapWeeklyEntity.setCreatedAt(Timestamp.from(Instant.now()));
        deRuleMapWeeklyEntity.setUpdatedAt(Timestamp.from(Instant.now()));
        deRuleMapWeeklyRepository.save(deRuleMapWeeklyEntity);
        log.info("Created an weekly bonus entry for de {} for rule {} with bonus {} created at {}",deId,incentiveRule.getId(),bonusAmount,week);

    }

    @Override
    public void updateMonthlyIncentiveBonusPayment(Long deId, IncentiveRulesEntity incentiveRule, Double bonusAmount, String month) {
        DeRuleMapMonthlyEntity deRuleMapMonthlyEntity = new DeRuleMapMonthlyEntity();
        deRuleMapMonthlyEntity.setBonus(bonusAmount);
        deRuleMapMonthlyEntity.setCompleted((byte)1);
        deRuleMapMonthlyEntity.setCreatedOn(month);
        deRuleMapMonthlyEntity.setDeId(deId);
        deRuleMapMonthlyEntity.setRuleId(incentiveRule.getId());
        deRuleMapMonthlyEntity.setCount(1);
        deRuleMapMonthlyEntity.setCreatedAt(Timestamp.from(Instant.now()));
        deRuleMapMonthlyEntity.setUpdatedAt(Timestamp.from(Instant.now()));
        deRuleMapMonthlyRepository.save(deRuleMapMonthlyEntity);
        log.info("Created an monthly bonus entry for de {} for rule {} with bonus {} created at {}",deId,incentiveRule.getId(),bonusAmount,month);

    }

    @Override
    public void updateRecurringBonusPayment(DeRuleMapDailyEntity deRuleMapDailyEntity, Double bonusAmount , String orderId) {
        deRuleMapDailyEntity.setBonus(deRuleMapDailyEntity.getBonus()+bonusAmount);
        deRuleMapDailyEntity.setCount(deRuleMapDailyEntity.getCount()+1);
        if (StringUtils.isEmpty(deRuleMapDailyEntity.getOrders())) {
            deRuleMapDailyEntity.setOrders(orderId);
        } else {
            deRuleMapDailyEntity.setOrders(deRuleMapDailyEntity.getOrders() +"," + orderId);
        }
        deRuleMapDailyEntity.setUpdatedAt(Timestamp.from(Instant.now()));
        deRuleMapDailyRepository.save(deRuleMapDailyEntity);
        log.info("Updated daily bonus entry for de {} for rule {} with bonus {} created at {}",deRuleMapDailyEntity.getDeId(),deRuleMapDailyEntity.getRuleId(),bonusAmount,deRuleMapDailyEntity.getCreatedOn());
    }

    @Override
    public int getRuleCountInWeekForMonthScope(String ruleName, String monthString, long deId) {
        return deRuleMapWeeklyRepository.getComputeCountInMonth(ruleName,monthString,deId);
    }

    @Override
    public int getRuleCountInDaysForMonthScope(String ruleName, String monthString, long deId) {
        return deRuleMapDailyRepository.getComputeCountInDuration(ruleName,monthString,deId);
    }

    @Override
    public int getRuleCountInDaysForWeekScope(String ruleName, String weekString, long deId) {
        log.info("rule Name , week string : {} getting rule count in days for week",ruleName,weekString);
        return deRuleMapDailyRepository.getComputeCountInDuration(ruleName,weekString,deId);
    }

    @Override
    public int getRuleCountInDay(String ruleName, String dateForDailyPerformanceData, long deId) {
        Integer count = deRuleMapDailyRepository.getComputeCountForSameDay(ruleName,dateForDailyPerformanceData,deId);
        if (count == null) {
            return  0;
        }
        return count;
    }


}
