package com.swiggy.alchemist.dao.dePayments;

import com.swiggy.alchemist.db.alchemist.entities.DeRuleMapDailyEntity;
import com.swiggy.alchemist.db.alchemist.entities.IncentiveRulesEntity;

public interface DePaymentsDao {
    void archiveDailyBonusPayments();

    void archiveWeeklyBonusPayments();

    void archiveMonthlyBonusPayments();

    void updateDailyIncentiveBonusPayment(Long id, IncentiveRulesEntity incentiveRule, Double bonusAmount, String date, boolean isCompleted, String orderId);

    void updateWeeklyIncentiveBonusPayment(Long id, IncentiveRulesEntity incentiveRule, Double bonusAmount, String week);

    void updateMonthlyIncentiveBonusPayment(Long id, IncentiveRulesEntity incentiveRule, Double bonusAmount, String month);

    void updateRecurringBonusPayment(DeRuleMapDailyEntity deRuleMapDailyEntity, Double bonusAmount, String orderId);

    int getRuleCountInWeekForMonthScope(String ruleName, String monthForMonthlyPerformanceData, long deId);

    int getRuleCountInDaysForMonthScope(String ruleName, String monthForMonthlyPerformanceData, long deId);

    int getRuleCountInDaysForWeekScope(String ruleName, String weekForWeeklyPerformanceData, long deId);

    int getRuleCountInDay(String ruleName, String dateForDailyPerformanceData, long deId);

}
