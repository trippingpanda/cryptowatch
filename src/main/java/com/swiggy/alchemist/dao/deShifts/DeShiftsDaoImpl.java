package com.swiggy.alchemist.dao.deShifts;

import com.swiggy.alchemist.db.delivery.repository.DeShiftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by RAVISINGH on 18/10/16.
 */
@Component
@Transactional("deliveryTransactionManager")
public class DeShiftsDaoImpl implements DeShiftsDao {

    @Autowired
    DeShiftRepository deShiftRepository;
    @Override
    public List<Long> getIdByName(List<String> names) {
       return deShiftRepository.findByNames(names);
    }
}
