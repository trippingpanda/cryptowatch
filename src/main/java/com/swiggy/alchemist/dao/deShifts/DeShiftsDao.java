package com.swiggy.alchemist.dao.deShifts;

import java.util.List;

/**
 * Created by RAVISINGH on 18/10/16.
 */
public interface DeShiftsDao {

    List<Long> getIdByName(List<String> names);
}
