package com.swiggy.alchemist.dao.parser;

import com.swiggy.alchemist.exceptions.AlchemistException;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by RAVISINGH on 22/09/16.
 */
public interface RuleParserDao {

    public long addRule(String ruleName, String description, String bonus, String expression, Timestamp from,
                        Timestamp to, String statusExpression, String statusMessage, String ruleString, String computeOn,
                        String scope, Long id, String user, Boolean isActive) throws AlchemistException;

    public void addIncentiveGroupMap(Long ruleId, List<String> incentiveGroupsStr);

    public void addZoneMap(Long ruleId, List<String> incentiveGroupsStr);

    public void addZoneMapWithIds(Long ruleId, List<Long> zones);


    public List<Long> getIncentiveGroupsByName(List<String> incentiveGroupsStr);
}
