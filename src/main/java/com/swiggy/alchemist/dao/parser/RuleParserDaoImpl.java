package com.swiggy.alchemist.dao.parser;

import com.swiggy.alchemist.dao.deShifts.DeShiftsDao;
import com.swiggy.alchemist.dao.zone.ZoneDao;
import com.swiggy.alchemist.db.alchemist.entities.IncentiveGroupRuleMapEntity;
import com.swiggy.alchemist.db.alchemist.entities.IncentiveRulesEntity;
import com.swiggy.alchemist.db.alchemist.entities.RuleZoneMapEntity;
import com.swiggy.alchemist.db.alchemist.repository.IncentiveGroupRespository;
import com.swiggy.alchemist.db.alchemist.repository.IncentiveGroupRuleMapRespository;
import com.swiggy.alchemist.db.alchemist.repository.IncentiveRulesRepository;
import com.swiggy.alchemist.db.alchemist.repository.RuleZoneMapRepository;
import com.swiggy.alchemist.exceptions.AlchemistException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

/**
 * Created by RAVISINGH on 22/09/16.
 */
@Repository
@Transactional
@Slf4j
public class RuleParserDaoImpl implements RuleParserDao {

    @Autowired
    IncentiveRulesRepository incentiveRulesRepository;
    @Autowired
    IncentiveGroupRuleMapRespository incentiveGroupRuleMapRespository;
    @Autowired
    IncentiveGroupRespository incentiveGroupRespository;
    @Autowired
    ZoneDao zoneDao;
    @Autowired
    RuleZoneMapRepository ruleZoneMapRepository;

    @Autowired
    DeShiftsDao deShiftsDao;



    @Transactional
    public long addRule(String ruleName, String description, String bonus, String expression, Timestamp from,
                        Timestamp to, String statusExpression, String statusMessage, String ruleString, String computeOn,
                        String scope, Long id, String user, Boolean isActive)
            throws AlchemistException {


        IncentiveRulesEntity incentiveRule = incentiveRulesRepository.findByRuleName(ruleName);

        if(incentiveRule == null ) {
            if(id != null) {
                incentiveRule = incentiveRulesRepository.findById(id);
            } else {
                incentiveRule = new IncentiveRulesEntity();
            }
        } else if(id == null){
            throw new AlchemistException(HttpStatus.SC_BAD_REQUEST,"Rule with name already exists");
           // incentiveRule = incentiveRules.get(0);
        }

        incentiveRule.setRuleName(ruleName);
        incentiveRule.setDescription(description);
        incentiveRule.setBonus(bonus);
        incentiveRule.setExpression(expression);
        incentiveRule.setValidityStartTime(from);
        incentiveRule.setValidityEndTime(to);
        incentiveRule.setStatusExpression(statusExpression);
        incentiveRule.setStatusMessage(statusMessage);
        incentiveRule.setRuleText(ruleString);
        incentiveRule.setComputeOn(computeOn);
        incentiveRule.setScope(scope);
        incentiveRule.setActive(isActive);
        incentiveRule.setUpdatedBy(user);
        incentiveRule.setUpdatedAt(Timestamp.from(Instant.now()));
        if(id == null ){
            incentiveRule.setCreatedBy(user);
            incentiveRule.setCreatedAt(Timestamp.from(Instant.now()));
        }
        incentiveRulesRepository.save(incentiveRule);
        return incentiveRule.getId();
    }

    private void deleteExistingMapping(Long ruleId) {

        log.info("deleted mapping for incentive group "+ ruleId);
        incentiveGroupRuleMapRespository.deleteByRuleId(ruleId);
    }

    private void deleteExistingZoneMapping(Long ruleId) {
        ruleZoneMapRepository.deleteByRuleId(ruleId);
    }

    public void addIncentiveGroupMap(Long ruleId, List<String> incentiveGroupsStr) {
        List<Long> incentiveGroups = deShiftsDao.getIdByName(incentiveGroupsStr);
        if (incentiveGroups != null && incentiveGroups.size() > 0) {
            incentiveGroupRuleMapRespository.deleteByRuleId(ruleId);
            for(Long incentiveGroup: incentiveGroups) {
                log.info("adding TG nth time");
                IncentiveGroupRuleMapEntity incentiveGroupRuleMapEntity  = new IncentiveGroupRuleMapEntity();
                incentiveGroupRuleMapEntity.setIncentiveGroupId(incentiveGroup);
                incentiveGroupRuleMapEntity.setRuleId(ruleId);
                incentiveGroupRuleMapRespository.save(incentiveGroupRuleMapEntity);
            }
        }
    }


    public void addZoneMap(Long ruleId, List<String> incentiveGroupsStr) {
        List<Long> zones = zoneDao.getZonesByName(incentiveGroupsStr);
        addZoneMapWithIds(ruleId, zones);
    }

    public void addZoneMapWithIds(Long ruleId, List<Long> zones) {
        if (zones != null && zones.size() > 0) {
            deleteExistingZoneMapping(ruleId);
            for(Long zone: zones) {
                RuleZoneMapEntity ruleZoneMapEntity  = new RuleZoneMapEntity();
                ruleZoneMapEntity.setRuleId(ruleId);
                ruleZoneMapEntity.setZoneId(zone);
                ruleZoneMapRepository.save(ruleZoneMapEntity);
            }
        }
    }




    public List<Long> getIncentiveGroupsByName(List<String> incentiveGroupsStr) {
        return incentiveGroupRespository.findByNames(incentiveGroupsStr);
    }


}
