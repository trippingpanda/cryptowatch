package com.swiggy.alchemist.dao.surge;

import java.sql.Timestamp;

/**
 * Created by RAVISINGH on 25/10/16.
 */
public interface SurgeDao {

    void addSurge(Long zoneId, Timestamp startTime, Timestamp endTime, Float multiplier);

    Float getSurge(Long zoneId);

    boolean isSurgeValid(Long zoneId, Timestamp startTime, Timestamp endTime);

    void markSurgeArchived(Long id, String userId);
}
