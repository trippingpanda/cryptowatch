package com.swiggy.alchemist.dao.surge;

import com.swiggy.alchemist.aspects.Redis;
import com.swiggy.alchemist.db.alchemist.entities.SurgeEntity;
import com.swiggy.alchemist.db.alchemist.repository.SurgeRepository;
import com.swiggy.alchemist.services.caching.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by RAVISINGH on 25/10/16.
 */
@Repository
@Transactional
public class SurgeDaoImpl implements SurgeDao {
    @Autowired
    SurgeRepository surgeRepository;

    @Override
    public void addSurge(Long zoneId, Timestamp startTime, Timestamp endTime, Float multiplier) {
        SurgeEntity surgeEntity = new SurgeEntity();
        surgeEntity.setEndTime(endTime);
        surgeEntity.setStartTime(startTime);
        surgeEntity.setMultiplier(multiplier);
        surgeEntity.setZoneId(zoneId);
        surgeEntity.setArchived(false);
        surgeRepository.save(surgeEntity);
    }
    @Override
    @Redis(cacheKeyPrefix = "AL_SURGE_ZONE_",cacheService = Cache.DEFAULT,defaultExpiry = 30)
    public Float getSurge(Long zoneId) {
        List<Float> result = surgeRepository.getMultiplierForZone(zoneId);
        if(result != null &&  result.size() > 0 ) {
            return result.get(0);
        }
        return 0F;
    }

    @Override
    public boolean isSurgeValid(Long zoneId, Timestamp startTime, Timestamp endTime) {
        List<Float> result = surgeRepository.getMultiplierForTime(zoneId, startTime, endTime);
        if(result != null && result.size() > 0) {
            return  false;
        }
        return true;
    }

    @Override
    public void markSurgeArchived(Long id, String userId) {
        surgeRepository.markSurgeArchived(id);
    }
}
