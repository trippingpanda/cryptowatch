package com.swiggy.alchemist.dao.performance;


import com.swiggy.alchemist.db.alchemist.entities.*;

import java.util.HashMap;
import java.util.List;

public interface DePerformanceDao {

    DePerformanceDailyEntity getDailyPerformanceData(long deId, long time);

    DePerformanceWeeklyEntity getWeeklyPerformanceData(long deId, long assignedTime);

    DePerformanceMonthlyEntity getMonthlyPerformanceData(long deId, long assignedTime);

    DePerformanceQuarterEntity get15MinPerformanceData(long id, long assignedTime);

    DePerformanceHourlyEntity getHourlyPerformanceData(Long deId, long assignedTime);

    void saveDailyPerformanceData(DePerformanceDailyEntity dePerformanceDailyEntity);

    void saveWeeklyPerformanceData(DePerformanceWeeklyEntity dePerformanceWeeklyEntity);

    void saveMonthlyPerformanceData(DePerformanceMonthlyEntity dePerformanceMonthlyEntity);

    void save15MinPerformanceData(DePerformanceQuarterEntity quaterHourPerformanceData);

    void saveHourlyPerformanceData(DePerformanceHourlyEntity hourPerformanceData);

    HashMap getWeeklyPerformanceData(Long deId, String weekForWeeklyPerformanceData);

    HashMap getDailyPerformanceData(Long deId, String dateForDailyPerformanceData, List<Long> timeSlotsForRule);

    HashMap getMonthlyPerformanceData(Long deId,String monthForMonthlyPerformanceData);

}
