package com.swiggy.alchemist.dao.performance;

import com.swiggy.alchemist.dao.timeslot.TimeSlotDao;
import com.swiggy.alchemist.db.alchemist.entities.*;
import com.swiggy.alchemist.db.alchemist.repository.*;
import com.swiggy.alchemist.pojo.AggregationPojo;
import com.swiggy.alchemist.services.aggregation.AggregationHelper;
import com.swiggy.alchemist.utility.IncentiveUtils;
import com.swiggy.alchemist.utility.SwiggyUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.swiggy.alchemist.utility.IncentiveUtils.*;

/**
 *
 */
@Service
@Slf4j
public class DePerformanceDaoImpl implements DePerformanceDao {

    @Autowired
    private DePerformanceQuaterlyRepository quarterlyRepository;

    @Autowired
    private DePerformanceDailyRepository dailyRepository;

    @Autowired
    private DePerformanceWeeklyRepository weeklyRepository;

    @Autowired
    private DePerformanceMonthlyRepository monthlyRepository;

    @Autowired
    private DePerformanceHourlyRepository hourlyRepository;

    @Autowired
    private AggregationHelper aggregationHelper;

    @Autowired
    TimeSlotDao timeSlotDao;

    @Override
    public DePerformanceQuarterEntity get15MinPerformanceData(long id, long time) {
        DePerformanceQuarterEntityPK dePerformanceQuarterEntityPK = new DePerformanceQuarterEntityPK();
        dePerformanceQuarterEntityPK.setDeId(id);
        dePerformanceQuarterEntityPK.setQuarterId(getQuarterHourForPerfData(time));
        DePerformanceQuarterEntity dePerformanceQuarterEntity = quarterlyRepository.findOne(dePerformanceQuarterEntityPK);
        if(dePerformanceQuarterEntity == null){
            dePerformanceQuarterEntity = new DePerformanceQuarterEntity();
            dePerformanceQuarterEntity.setDeId(id);
            dePerformanceQuarterEntity.setQuarterId(getQuarterHourForPerfData(time));
            dePerformanceQuarterEntity.setCreatedAt(Timestamp.from(Instant.now()));
        }

        log.info(dePerformanceQuarterEntity.getQuarterId());
        return dePerformanceQuarterEntity;
    }

    @Override
    public DePerformanceHourlyEntity getHourlyPerformanceData(Long deId, long time) {
        DePerformanceHourlyEntityPK dePerformanceHourlyEntityPK = new DePerformanceHourlyEntityPK();
        dePerformanceHourlyEntityPK.setDeId(deId);
        dePerformanceHourlyEntityPK.setHour(getHourForPerfData(time));
        DePerformanceHourlyEntity dePerformanceHourlyEntity = hourlyRepository.findOne(dePerformanceHourlyEntityPK);
        if(dePerformanceHourlyEntity == null){
            dePerformanceHourlyEntity = new DePerformanceHourlyEntity();
            dePerformanceHourlyEntity.setDeId(deId);
            dePerformanceHourlyEntity.sethour(getHourForPerfData(time));
            dePerformanceHourlyEntity.setCreatedAt(Timestamp.from(Instant.now()));
        }

        log.info(dePerformanceHourlyEntity.gethour());
        return dePerformanceHourlyEntity;
    }

    @Override
    public DePerformanceDailyEntity getDailyPerformanceData(long deId, long time) {
        DePerformanceDailyEntityPK dePerformanceDailyEntityPK = new DePerformanceDailyEntityPK();
        dePerformanceDailyEntityPK.setDeId(deId);
        dePerformanceDailyEntityPK.setDate(getDateForDailyPerformanceData(time));
        DePerformanceDailyEntity dePerformanceDailyEntity = dailyRepository.findOne(dePerformanceDailyEntityPK);
        if(dePerformanceDailyEntity == null){
            dePerformanceDailyEntity = new DePerformanceDailyEntity();
            dePerformanceDailyEntity.setDate(getDateForDailyPerformanceData(time));
            dePerformanceDailyEntity.setDeId(deId);
            dePerformanceDailyEntity.setCreatedAt(Timestamp.from(Instant.now()));
        }
        log.info(dePerformanceDailyEntity.getDate());
        return dePerformanceDailyEntity;
    }



    @Override
    public DePerformanceWeeklyEntity getWeeklyPerformanceData(long deId, long time) {
        DePerformanceWeeklyEntityPK dePerformanceWeeklyEntityPK = new DePerformanceWeeklyEntityPK();
        dePerformanceWeeklyEntityPK.setDeId(deId);
        dePerformanceWeeklyEntityPK.setWeek(getWeekForWeeklyPerformanceData(time));
        DePerformanceWeeklyEntity dePerformanceWeeklyEntity = weeklyRepository.findOne(dePerformanceWeeklyEntityPK);
        if(dePerformanceWeeklyEntity == null){
            dePerformanceWeeklyEntity = new DePerformanceWeeklyEntity();
            dePerformanceWeeklyEntity.setDeId(deId);
            dePerformanceWeeklyEntity.setWeek(getWeekForWeeklyPerformanceData(time));
            dePerformanceWeeklyEntity.setCreatedAt(Timestamp.from(Instant.now()));
        }
        log.info(dePerformanceWeeklyEntity.getWeek());
        return dePerformanceWeeklyEntity;
    }



    @Override
    public DePerformanceMonthlyEntity getMonthlyPerformanceData(long deId, long time) {
        DePerformanceMonthlyEntityPK dePerformanceMonthlyEntityPK = new DePerformanceMonthlyEntityPK();
        dePerformanceMonthlyEntityPK.setDeId(deId);
        dePerformanceMonthlyEntityPK.setMonth(getMonthForMonthlyPerformanceData(time));
        DePerformanceMonthlyEntity dePerformanceMonthlyEntity = monthlyRepository.findOne(dePerformanceMonthlyEntityPK);
        if(dePerformanceMonthlyEntity == null){
            dePerformanceMonthlyEntity = new DePerformanceMonthlyEntity();
            dePerformanceMonthlyEntity.setDeId(deId);
            dePerformanceMonthlyEntity.setMonth(getMonthForMonthlyPerformanceData(time));
            dePerformanceMonthlyEntity.setCreatedAt(Timestamp.from(Instant.now()));
        }
        log.info(dePerformanceMonthlyEntity.getMonth());
        return dePerformanceMonthlyEntity;
    }


    @Override
    public void save15MinPerformanceData(DePerformanceQuarterEntity quaterHourPerformanceData) {
        quarterlyRepository.save(quaterHourPerformanceData);
    }

    @Override
    public void saveHourlyPerformanceData(DePerformanceHourlyEntity hourPerformanceData) {
        hourlyRepository.save(hourPerformanceData);
    }

    @Override
    public HashMap getWeeklyPerformanceData(Long deId, String weekForWeeklyPerformanceData) {
        DePerformanceWeeklyEntityPK dePerformanceWeeklyEntityPK = new DePerformanceWeeklyEntityPK();
        dePerformanceWeeklyEntityPK.setDeId(deId);
        dePerformanceWeeklyEntityPK.setWeek(weekForWeeklyPerformanceData);
        DePerformanceWeeklyEntity dePerformanceWeeklyEntity = weeklyRepository.findOne(dePerformanceWeeklyEntityPK);
        if(dePerformanceWeeklyEntity != null && dePerformanceWeeklyEntity.getPerfBlob() != null) {
            return SwiggyUtils.getHashMapFromJsonObject(dePerformanceWeeklyEntity.getPerfBlob());
        } else {
            return new HashMap();
        }
    }

    @Override
    public HashMap getDailyPerformanceData(Long deId, String dateForDailyPerformanceData, List<Long> timeSlotsForRule) {
        if(CollectionUtils.isEmpty(timeSlotsForRule)) {
            DePerformanceDailyEntityPK dePerformanceDailyEntityPK = new DePerformanceDailyEntityPK();
            dePerformanceDailyEntityPK.setDeId(deId);
            dePerformanceDailyEntityPK.setDate(dateForDailyPerformanceData);
            DePerformanceDailyEntity dePerformanceDailyEntity = dailyRepository.findOne(dePerformanceDailyEntityPK);
            if (dePerformanceDailyEntity != null && dePerformanceDailyEntity.getPerfBlob() !=  null) {
                return SwiggyUtils.getHashMapFromJsonObject(dePerformanceDailyEntity.getPerfBlob());
            } else {
                return new HashMap();
            }
        }else {
            return getDailyAggregationForRuleTimeSlot(deId, timeSlotsForRule,dateForDailyPerformanceData);
        }
    }

    private HashMap getDailyAggregationForRuleTimeSlot(Long deId, List<Long> timeSlotsForRule, String dateForDailyPerformanceData) {
        List<AggregationPojo> aggregationPojoList = new ArrayList<>();
        log.info("for deId {}: running aggregations over timeslots : {}",deId,timeSlotsForRule);
        timeSlotsForRule.forEach(x -> {
            DeTimeslotsEntity timeSlot = timeSlotDao.getTimeSlotById(x);
            int date = IncentiveUtils.getDayOfWeekFromDateString(dateForDailyPerformanceData);
            if(timeSlot.getWeekdays().contains(String.valueOf(date))) {
                aggregationPojoList.addAll(getHourlyAggregations(deId, timeSlot.getStartTime(), timeSlot.getEndTime(), dateForDailyPerformanceData));
                aggregationPojoList.addAll(getQuaterHourAggregations(deId, timeSlot.getStartTime(), timeSlot.getEndTime(), dateForDailyPerformanceData));
            }
        });

        AggregationPojo aggregationPojo = AggregationPojo.builder().avgArrivalDelay(aggregationHelper.getAvgArrivalDelay(aggregationPojoList))
                .avgConfirmationDelay(aggregationHelper.getAvgConfirmationDelay(aggregationPojoList))
                .avgDeliveredDelay(aggregationHelper.getAvgDeliveryDelay(aggregationPojoList))
                .avgFirstMile(aggregationHelper.getAvgFirstMile(aggregationPojoList))
                .avgLastMile(aggregationHelper.getAvglastMile(aggregationPojoList))
                .avgRating(aggregationHelper.getAvgRating(aggregationPojoList))
                .avgPickupDelay(aggregationHelper.getAvgPickupDelay(aggregationPojoList))
                .avgReachedDelay(aggregationHelper.getAvgReachedDelay(aggregationPojoList))
                .avgSla(aggregationHelper.getAvgSla(aggregationPojoList))
                .trips_count(aggregationHelper.getTotalTrips(aggregationPojoList)).build();
        log.info(SwiggyUtils.jsonEncode(aggregationPojo));
        return SwiggyUtils.getHashMapFromJsonObject(SwiggyUtils.jsonEncode(aggregationPojo));
    }

    /**
     *Times in hhmm format . if 0815-1235, gives aggregations for 0815-0900 and 1200 - 1235
     * @param deId
     * @param start
     * @param end
     * @param dateForDailyPerformanceData
     * @return
     */
    private List<AggregationPojo> getQuaterHourAggregations(Long deId, Integer start, Integer end, String dateForDailyPerformanceData) {
        List<AggregationPojo> aggregationPojos = new ArrayList<>();
        List<String> stringForQuarterlyPerfData = IncentiveUtils.getQuartersForStartHour(start, dateForDailyPerformanceData);
        stringForQuarterlyPerfData.addAll(IncentiveUtils.getQuartersForEndHour(end, dateForDailyPerformanceData));
        List<DePerformanceQuarterEntityPK> dePerformanceQuarterEntityPKs = stringForQuarterlyPerfData.stream().map(x -> {
            DePerformanceQuarterEntityPK dePerformanceQuarterEntityPK = new DePerformanceQuarterEntityPK();
            dePerformanceQuarterEntityPK.setDeId(deId);
            dePerformanceQuarterEntityPK.setQuarterId(x);
            return dePerformanceQuarterEntityPK;
        }).collect(Collectors.toList());

        Iterable<DePerformanceQuarterEntity> dePerformanceQuarterEntities = quarterlyRepository.findAll(dePerformanceQuarterEntityPKs);
        if(dePerformanceQuarterEntities == null){
            return aggregationPojos;
        }
        dePerformanceQuarterEntities.forEach(x -> {
            if(x != null && !StringUtils.isEmpty(x.getPerfBlob())){
                aggregationPojos.add((AggregationPojo) SwiggyUtils.jsonDecode(x.getPerfBlob(),AggregationPojo.class));
            }
        });
        return aggregationPojos;
    }

    /**
     * Times in hhmm format. if 0815 to 1235. Gives hourly aggregations of hours from 9-12 inclusive.
     * @param start
     * @param end
     * @param dateForDailyPerformanceData
     */
    private List<AggregationPojo> getHourlyAggregations(Long deId,Integer start, Integer end, String dateForDailyPerformanceData) {
        List<AggregationPojo> aggregationPojos = new ArrayList<>();
        int startHour = start/100;
        if(start%100 != 0) { // for cases like 0815
            startHour += 1;
        }
        int endHour = (end/100) - 1;
        log.info("for deId {}: running aggregations over start time {} , end time : {} and date {}",deId,start,end,dateForDailyPerformanceData);
        while(startHour <= endHour) {
            DePerformanceHourlyEntityPK dePerformanceHourlyEntityPK = new DePerformanceHourlyEntityPK();
            dePerformanceHourlyEntityPK.setDeId(deId);
            dePerformanceHourlyEntityPK.setHour(dateForDailyPerformanceData+"-"+(startHour>=10?startHour:"0"+startHour));
            log.info("getting aggregation for {}",dateForDailyPerformanceData+"-"+(startHour>=10?startHour:"0"+startHour));
            DePerformanceHourlyEntity dePerformanceHourlyEntity = hourlyRepository.findOne(dePerformanceHourlyEntityPK);
            if(dePerformanceHourlyEntity != null && dePerformanceHourlyEntity.getPerfBlob()!= null){
                log.info("adding hourly perf pojo: {}",dePerformanceHourlyEntity.getPerfBlob());
                aggregationPojos.add((AggregationPojo) SwiggyUtils.jsonDecode(dePerformanceHourlyEntity.getPerfBlob(),AggregationPojo.class));
            }
            startHour++;
        }
        return aggregationPojos;
    }


    @Override
    public HashMap getMonthlyPerformanceData(Long deId, String monthForMonthlyPerformanceData) {
        log.info("month "+ monthForMonthlyPerformanceData);
        DePerformanceMonthlyEntityPK dePerformanceMonthlyEntityPK = new DePerformanceMonthlyEntityPK();
        dePerformanceMonthlyEntityPK.setDeId(deId);
        dePerformanceMonthlyEntityPK.setMonth(monthForMonthlyPerformanceData);
        DePerformanceMonthlyEntity dePerformanceMonthlyEntity = monthlyRepository.findOne(dePerformanceMonthlyEntityPK);
        if(dePerformanceMonthlyEntity != null && dePerformanceMonthlyEntity.getPerfBlob() != null) {
            return SwiggyUtils.getHashMapFromJsonObject(dePerformanceMonthlyEntity.getPerfBlob());
        } else {
            return  new HashMap();
        }
    }


    @Override
    public void saveDailyPerformanceData(DePerformanceDailyEntity dePerformanceDailyEntity) {
        dailyRepository.save(dePerformanceDailyEntity);
    }

    @Override
    public void saveWeeklyPerformanceData(DePerformanceWeeklyEntity weeklyPerformanceEntity) {
        weeklyRepository.save(weeklyPerformanceEntity);
    }

    @Override
    public void saveMonthlyPerformanceData(DePerformanceMonthlyEntity dePerformanceMonthlyEntity) {
        monthlyRepository.save(dePerformanceMonthlyEntity);
    }

}
