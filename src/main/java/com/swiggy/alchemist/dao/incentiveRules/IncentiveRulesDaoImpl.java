package com.swiggy.alchemist.dao.incentiveRules;

import com.swiggy.alchemist.db.alchemist.entities.IncentiveRulesEntity;
import com.swiggy.alchemist.db.alchemist.repository.IncentiveRulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class IncentiveRulesDaoImpl implements IncentiveRulesDao {

    @Autowired
    private IncentiveRulesRepository incentiveRulesRepository;

    List<String> statusUpdateComputeOn  = Arrays.asList("delivered","pickedup","assigned","confirmed","reached");

    @Override
    public List<IncentiveRulesEntity> getRulesValidOnDelivered(List<Long> validTimeSlots, List<Long> incentiveGroupId, long zoneId) {
        if(CollectionUtils.isEmpty(incentiveGroupId))
            return new ArrayList<>();
        List<IncentiveRulesEntity> validRules = incentiveRulesRepository.findRecurrentRules(incentiveGroupId,zoneId,statusUpdateComputeOn);
        List<IncentiveRulesEntity> rulesValidOnDelivered = incentiveRulesRepository.findValidRules(validTimeSlots, incentiveGroupId,statusUpdateComputeOn,zoneId);
        validRules.addAll(rulesValidOnDelivered);
        return validRules;
    }

    @Override
    public IncentiveRulesEntity getRuleByName(String ruleName) {
        return incentiveRulesRepository.findByRuleName(ruleName);
    }

    @Override
    public List<IncentiveRulesEntity> getRulesValidOnRejected(List<Long> validTimeSlots, List<Long> incentiveGroupId, long zoneId) {
        if(CollectionUtils.isEmpty(incentiveGroupId))
            return new ArrayList<>();
        List<IncentiveRulesEntity> validRules = incentiveRulesRepository.findRecurrentRules(incentiveGroupId,zoneId,Arrays.asList("rejected"));
        List<IncentiveRulesEntity> rulesValidOnRejected =  incentiveRulesRepository.findValidRules(validTimeSlots,incentiveGroupId,"rejected",zoneId);
        validRules.addAll(rulesValidOnRejected);
        return validRules;
    }

    @Override
    public List<IncentiveRulesEntity> getRulesValidOnPickedUp(List<Long> validTimeSlots, List<Long> incentiveGroupId, long zoneId) {
        if(CollectionUtils.isEmpty(incentiveGroupId))
            return new ArrayList<>();
        List<IncentiveRulesEntity> validRules = incentiveRulesRepository.findRecurrentRules(incentiveGroupId,zoneId,Arrays.asList("pickedup","assigned","confirmed","reached"));
        List<IncentiveRulesEntity> rulesValidOnPickedup =  incentiveRulesRepository.findValidRules(validTimeSlots,incentiveGroupId,"pickedup",zoneId);
        validRules.addAll(rulesValidOnPickedup);
        return validRules;
    }

    @Override
    public List<IncentiveRulesEntity> getRulesValidOnLogout(List<Long> incentiveGroupIds, long zoneId) {
        if(CollectionUtils.isEmpty(incentiveGroupIds))
            return new ArrayList<>();
        return incentiveRulesRepository.findRulesForLogout(incentiveGroupIds,zoneId);
    }

    @Override
    public List<IncentiveRulesEntity> getRulesValidOnEOD(List<Long> validTimeSlots, List<Long> incentiveGroupId, long zoneId) {
        if(CollectionUtils.isEmpty(incentiveGroupId))
            return new ArrayList<>();
        return incentiveRulesRepository.findValidRules(validTimeSlots,incentiveGroupId,"EOD",zoneId);
    }

    @Override
    public List<IncentiveRulesEntity> getRulesValidOnEOM(List<Long> validTimeSlots, List<Long> incentiveGroupId, long zoneId) {
        if(CollectionUtils.isEmpty(incentiveGroupId))
            return new ArrayList<>();
        return incentiveRulesRepository.findValidRules(validTimeSlots,incentiveGroupId,"EOM",zoneId);
    }

    @Override
    public List<IncentiveRulesEntity> getRulesValidOnEOW(List<Long> validTimeSlots, List<Long> incentiveGroupId, long zoneId) {
        if(CollectionUtils.isEmpty(incentiveGroupId))
            return new ArrayList<>();
        return incentiveRulesRepository.findValidRules(validTimeSlots,incentiveGroupId,"EOW",zoneId);
    }

    @Override
    public List<IncentiveRulesEntity> getRulesValidOnEOD(List<Long> incentiveGroupId, long zoneId) {
        if(CollectionUtils.isEmpty(incentiveGroupId))
            return new ArrayList<>();
        return incentiveRulesRepository.findRulesForEndOfTime(incentiveGroupId,zoneId,"eod");
    }

    @Override
    public List<IncentiveRulesEntity> getRulesValidOnEOW(List<Long> incentiveGroupId, long zoneId) {
        if(CollectionUtils.isEmpty(incentiveGroupId))
            return new ArrayList<>();
        return incentiveRulesRepository.findRulesForEndOfTime(incentiveGroupId,zoneId,"eow");
    }

    @Override
    public List<IncentiveRulesEntity> getRulesValidOnEOM(List<Long> incentiveGroupId, long zoneId) {
        if(CollectionUtils.isEmpty(incentiveGroupId))
            return new ArrayList<>();
        return incentiveRulesRepository.findRulesForEndOfTime(incentiveGroupId,zoneId,"eom");
    }
}
