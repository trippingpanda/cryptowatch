package com.swiggy.alchemist.dao.incentiveRules;

import com.swiggy.alchemist.db.alchemist.entities.IncentiveRulesEntity;

import java.util.List;

public interface IncentiveRulesDao {
    List<IncentiveRulesEntity> getRulesValidOnDelivered(List<Long> validTimeSlots, List<Long> incentiveGroupId, long zoneId);

    IncentiveRulesEntity getRuleByName(String ruleName);

    List<IncentiveRulesEntity> getRulesValidOnRejected(List<Long> validTimeSlots, List<Long> incentiveGroupId, long zoneId);

    List<IncentiveRulesEntity> getRulesValidOnPickedUp(List<Long> validTimeSlots, List<Long> incentiveGroupId, long zoneId);

    List<IncentiveRulesEntity> getRulesValidOnLogout(List<Long> incentiveGroupIds, long zoneId);

    List<IncentiveRulesEntity> getRulesValidOnEOD(List<Long> validTimeSlots, List<Long> incentiveGroupId, long zoneId);
    List<IncentiveRulesEntity> getRulesValidOnEOM(List<Long> validTimeSlots, List<Long> incentiveGroupId, long zoneId);
    List<IncentiveRulesEntity> getRulesValidOnEOW(List<Long> validTimeSlots, List<Long> incentiveGroupId, long zoneId);

    List<IncentiveRulesEntity> getRulesValidOnEOD(List<Long> incentiveGroupId, long zoneId);

    List<IncentiveRulesEntity> getRulesValidOnEOW(List<Long> incentiveGroupId, long zoneId);

    List<IncentiveRulesEntity> getRulesValidOnEOM(List<Long> incentiveGroupId, long zoneId);
}
