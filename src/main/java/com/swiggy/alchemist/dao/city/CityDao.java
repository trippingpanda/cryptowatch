package com.swiggy.alchemist.dao.city;

import java.util.List;

/**
 * Created by RAVISINGH on 17/11/16.
 */
public interface CityDao {
    List<Long> getCitiesByName(List<String> zones);
}
