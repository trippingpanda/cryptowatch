package com.swiggy.alchemist.dao.city;

import com.swiggy.alchemist.db.delivery.repository.CityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by RAVISINGH on 17/11/16.
 */
@Slf4j
@Component
@Transactional("deliveryTransactionManager")
public class CityDaoImpl implements CityDao {

    @Autowired
    CityRepository cityRepository;

    public List<Long> getCitiesByName(List<String> zones) {
        return cityRepository.findByNames(zones);
    }
}
