package com.swiggy.alchemist.dao.zone;

import com.swiggy.alchemist.db.delivery.entities.ZoneEntity;

import java.util.List;

/**
 * Created by RAVISINGH on 05/10/16.
 */
public interface ZoneDao {
    ZoneEntity getZone(Long id);

     List<Long> getZoneByCity(Long cityId);

     List<Long> getZonesByName(List<String> zones);

     boolean isRainModeOn(Long zoneId);
}
