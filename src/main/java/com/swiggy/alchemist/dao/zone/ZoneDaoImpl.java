package com.swiggy.alchemist.dao.zone;

import com.swiggy.alchemist.aspects.Redis;
import com.swiggy.alchemist.db.delivery.entities.ZoneEntity;
import com.swiggy.alchemist.db.delivery.repository.ZoneRespository;
import com.swiggy.alchemist.services.caching.Cache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by RAVISINGH on 05/10/16.
 */
@Slf4j
@Component
@Transactional("deliveryTransactionManager")
public class ZoneDaoImpl implements ZoneDao {

    @Autowired
    ZoneRespository zoneRespository;
    @Override
    @Redis(cacheKeyPrefix = "AL_ZONE_",cacheService = Cache.DEFAULT,defaultExpiry = 30)
    public ZoneEntity getZone(Long id) {
        log.info("zone "+ zoneRespository.findById(1).getName());
        return zoneRespository.findById(id);
    }

    @Override
    public List<Long> getZoneByCity(Long cityId) {
        return zoneRespository.findByCityId(cityId);
    }

    public List<Long> getZonesByName(List<String> zones) {
        return zoneRespository.findByNames(zones);
    }

    @Override
    @Redis(cacheKeyPrefix = "AL_RM_ZONE_",cacheService = Cache.DEFAULT,defaultExpiry = 30)
    public boolean isRainModeOn(Long zoneId) {
        Boolean rainMode = zoneRespository.getRainMode(zoneId);
        if (rainMode != null) {
            return  rainMode.booleanValue();
        }
        return false;
    }
}
