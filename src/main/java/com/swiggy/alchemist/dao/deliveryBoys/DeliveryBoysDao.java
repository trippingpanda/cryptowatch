package com.swiggy.alchemist.dao.deliveryBoys;


import java.sql.Timestamp;
import java.util.List;

public interface DeliveryBoysDao {
    long getZoneForDe(Long deliveryBoyId);

    List<Long> getDeOfZone(Long zone);

    List<Long> getIncentiveGroupForDe(Long deliveryBoyId);

    long getLoginTime(Long deId, Timestamp startTime, Timestamp endTime);

    String getDeTenureInDays(Long deId);

    List<Long> getDeOfZone(List<Long> zones);

    List<Long> getAllDeliveryBoys();

}
