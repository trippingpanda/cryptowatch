package com.swiggy.alchemist.dao.deliveryBoys;

import com.swiggy.alchemist.aspects.Redis;
import com.swiggy.alchemist.db.delivery.entities.CandidateInfo;
import com.swiggy.alchemist.db.delivery.entities.DeAttendanceLog;
import com.swiggy.alchemist.db.delivery.entities.DeliveryBoys;
import com.swiggy.alchemist.db.delivery.repository.CandidateInfoRepository;
import com.swiggy.alchemist.db.delivery.repository.DeAttendanceLogRepository;
import com.swiggy.alchemist.db.delivery.repository.DeIncentiveGroupRepository;
import com.swiggy.alchemist.db.delivery.repository.DeliveryBoysRepository;
import com.swiggy.alchemist.services.caching.Cache;
import com.swiggy.alchemist.utility.IncentiveUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
@Transactional("deliveryTransactionManager")
@Slf4j
public class DeliveryBoysDaoImpl implements DeliveryBoysDao {

    @Autowired
    private DeliveryBoysRepository deliveryBoysRepository;
    @Autowired
    private DeAttendanceLogRepository deAttendanceLogRepository;
    @Autowired
    private DeIncentiveGroupRepository deIncentiveGroupRepository;
    @Autowired
    private CandidateInfoRepository candidateInfoRepository;

    private static final String DEFAULT_DE_TENURE = "30";


    @Override
    @Redis(cacheKeyPrefix = "DE_ZONE_",cacheService = Cache.DEFAULT,defaultExpiry = 300)
    public long getZoneForDe(Long deliveryBoyId) {
        return deliveryBoysRepository.findZoneIdById(deliveryBoyId);
    }

    @Override
    public List<Long> getIncentiveGroupForDe(Long deliveryBoyId) {
        return deIncentiveGroupRepository.findIncentiveGroupIdByDeId(deliveryBoyId);
    }

    public long getLoginTime(Long deId, Timestamp startTime, Timestamp endTime) {
        Timestamp currenTimestamp = new Timestamp(new Date().getTime());
        if (endTime.compareTo(currenTimestamp) > 0) {
            endTime = currenTimestamp;
        }
        log.info("startTime : {} endTime: {}",startTime, endTime);
        List<DeAttendanceLog> deAttendanceLogList = deAttendanceLogRepository.getDeAttendanceLogs(deId,startTime,endTime);
        if(deAttendanceLogList != null && deAttendanceLogList.size() > 0) {
            return getLoggedInTime(deAttendanceLogList, startTime, endTime);
        }
        deAttendanceLogList = deAttendanceLogRepository.getLastDeAttendanceLogs(deId,startTime,new PageRequest(0,1));
        if(deAttendanceLogList != null && deAttendanceLogList.size() > 0) {
            String event = deAttendanceLogList.get(0).getEvent();
            if (event.equalsIgnoreCase("logged_in") || event.equalsIgnoreCase("start_duty")) {
                long diff = endTime.getTime() - startTime.getTime();
                return diff / (60 * 1000);
            }
        }
        return 0;
    }

    @Override
    @Redis(cacheKeyPrefix = "DE_TENURE_",cacheService = Cache.STRING,defaultExpiry = 300)
    public String getDeTenureInDays(Long deId) {
        log.info("getting value from db");
        CandidateInfo candidateInfo = candidateInfoRepository.findByDeId(deId);
        if(candidateInfo == null || candidateInfo.getRecord_timestamp() == null || candidateInfo.getRecord_timestamp().toString().isEmpty()){
            return DEFAULT_DE_TENURE;
        }

        long timePassedInDays = IncentiveUtils.getTimePassedInDays(candidateInfo.getRecord_timestamp());
        if(timePassedInDays == 0)
            return DEFAULT_DE_TENURE;
        return String.valueOf(timePassedInDays);
    }

    private long getLoggedInTime(List<DeAttendanceLog> deAttendanceLogs, Timestamp startTime, Timestamp endTime) {
        long result = 0;
        Date loginTime = startTime;
        for (DeAttendanceLog deAttendanceLog : deAttendanceLogs) {
            String event = deAttendanceLog.getEvent();
            if (event.equalsIgnoreCase("logged_in") || event.equalsIgnoreCase("start_duty")) {
                loginTime = deAttendanceLog.getTime();
            } else if (event.equalsIgnoreCase("logged_out") || event.equalsIgnoreCase("stop_duty")) {
                if (loginTime != null) {
                    long diff = deAttendanceLog.getTime().getTime() - loginTime.getTime();
                    result += diff / (60 * 1000); //in minutes
                    loginTime = null;
                }
            }
        }
        if (loginTime != null) {
            long diff = endTime.getTime() - loginTime.getTime();
            result += diff / (60 * 1000); //in minutes
        }
        return result;
    }

    public List<Long> getDeOfZone(List<Long> zones) {
        return deliveryBoysRepository.getDeListByZones(zones);
    }

    public List<Long> getDeOfZone(Long zone) {

        return deliveryBoysRepository.getDeListByZone(zone);
    }

    @Override
    public List<Long> getAllDeliveryBoys() {

        return deliveryBoysRepository.findValidDes();
    }

}
