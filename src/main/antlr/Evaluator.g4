grammar Evaluator;

@header {
package com.swiggy.alchemist.antlr;
}

cummulative
    : cummulative AND equation #AndCondition
    | cummulative OR equation  #OrCondition
    | LPAR cummulative RPAR    #CummulativeWithParen
    | equation                 #ToEquation
    ;

equation
    : expression EQ expression #EqualTo
    | expression GT expression #GreaterThan
    | expression LT expression #LessThan
    | LPAR equation RPAR       #EquationWithParen
    ;
expression
    : MAX expression expression #MaxExpression
    | MIN expression expression #MinExpression
    | plusOrMinus # Calculate
    ;

plusOrMinus
    : plusOrMinus PLUS multOrDiv  # Plus
    | plusOrMinus MINUS multOrDiv # Minus
    | multOrDiv                   # ToMultOrDiv
    ;

multOrDiv
    : multOrDiv MULT pow # Multiplication
    | multOrDiv DIV pow  # Division
    | pow                # ToPow
    ;

pow
    : unaryMinus (POW pow)? # Power
    ;

unaryMinus
    : MINUS unaryMinus # ChangeSign
    | atom             # ToAtom
    ;

atom
    : DOUBLE                # Double
    | INT                   # Int
    | ID                    # Variable
    | LPAR plusOrMinus RPAR # Braces
    ;

condition
   : EQ
   | GT
   | LT
   ;


    INT    : [0-9]+;
    DOUBLE : [0-9]+'.'[0-9]+;
    POW    : '^';
    NL     : '\n';
    WS     : [ \t\r]+ -> skip;
    AND    : 'AND' | 'and';
    OR     : 'OR' | 'or';
    MAX    : 'MAX' | 'max';
    MIN    : 'MIN' | 'min';
    ID     : [a-zA-Z_][a-zA-Z_0-9]*;
    EQ    : '=';
    GT    : '>';
    LT    : '<';
    PLUS  : '+';
    EQUAL : '=';
    MINUS : '-';
    MULT  : '*';
    DIV   : '/';
    LPAR  : '(';
    RPAR  : ')';

