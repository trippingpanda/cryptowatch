grammar RuleGrammar;

@header {
package com.swiggy.alchemist.antlr;
}

incentiveRule:  'RULE' pair+
    ;

pair : KEY (TEXT)+;

KEY: NAME |  DESCRIPTION | BONUS | DESCRIPTION | STATUS_MESSAGE | STATUS_EXP | SCOPE |
     FOR | CONDITION | TIMESLOT | FROM | TO | DEFINE | COMPUTE_ON;

TEXT : [a-zA-Z0-9(){}+-\&\*/%:<>"_.]+ ;


NUMBER
    :  ('+'|'-')? DIGIT+ '.' DIGIT*
    |  ('+'|'-')? DIGIT+  ;


fragment
DIGIT:  '0'..'9';

fragment NAME: 'NAME';

fragment RULE : 'RULE';

fragment BONUS: 'BONUS';

fragment DESCRIPTION: 'DESCRIPTION';

fragment STATUS_EXP: 'STATUS EXP';

fragment STATUS_MESSAGE: 'STATUS MESSAGE';

fragment SCOPE: 'SCOPE';

fragment FOR: 'FOR';

fragment CONDITION: 'CONDITION';

fragment TIMESLOT: 'TIMESLOT';

fragment FROM: 'FROM';

fragment TO: 'TO';

fragment DEFINE: 'DEFINE';

fragment COMPUTE_ON: 'COMPUTE ON';


WS  :   [\t \n\r]+ -> skip;