grammar ForGrammar;

@header {
package com.swiggy.alchemist.antlr;
}


ruleDomain: pair (OR pair)*;
pair : KEY EQ TEXT+ ;
KEY : INCENTIVE_GROUP | ZONE_ID | CITY_ID;

fragment INCENTIVE_GROUP: 'incentive_group'| 'INCENTIVE_GROUP';
fragment ZONE_ID: 'zone' | 'ZONE';
fragment CITY_ID: 'city' | 'CITY';
EQ : '=';
OR : 'OR'|'or' | 'AND' | 'and';
TEXT : [a-zA-Z0-9(){}+-\&\*/%:<>"_]+ ;
WS  :   [\t \n\r ]+ -> skip;
